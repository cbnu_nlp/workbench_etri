package dbconnector;

import com.mongodb.client.MongoCollection;
import org.bson.Document;

public class KorDic {
    private static KorDic instance;

    public static KorDic getInstance(){
        if(instance == null){
            instance = new KorDic();
        }
        return instance;
    }

    DBConnector mongoDBConnector = new DBConnector();

    private MongoCollection<Document> korStandardDic;

    private KorDic(){
        mongoDBConnector.connectorServer();
        korStandardDic = mongoDBConnector.getCollection("korDicDB", "korDicData");
    }

    public MongoCollection<Document> getKorStandardDic(){
        return korStandardDic;
    }
}
//kordicDB / data_2010