package dbconnector;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import java.util.Arrays;

public class DBConnector {
    static private String dbUsername = "root";
    static private char[] dbPassword = "nlp44342".toCharArray();
    static private String dbName = "admin";

    static private String serverAddress = "203.255.81.71";

    private MongoClient mongoClient;

    public void connectorServer(){
        MongoCredential credential = MongoCredential.createCredential(dbUsername, dbName, dbPassword);
        mongoClient = new MongoClient(new ServerAddress(serverAddress), Arrays.asList(credential));
    }

    public void discoonectorServer(){
        mongoClient.close();
    }

    private MongoDatabase getDB(String dbName){
        return mongoClient.getDatabase(dbName);
    }

    public MongoCollection getCollection(String dbName, String collectionName){
        MongoDatabase mongoDatabase = getDB(dbName);
        if(mongoDatabase == null){
            return null;
        }else{
            return mongoDatabase.getCollection(collectionName);
        }
    }
}
