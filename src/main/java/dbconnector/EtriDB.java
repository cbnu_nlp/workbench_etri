package dbconnector;

import com.mongodb.client.MongoCollection;
import org.bson.Document;

public class EtriDB {
    private static  EtriDB instance;
    private static String userName;

    public static  EtriDB getInstance(String name){
        userName = name;
        if(instance == null) {
            instance = new EtriDB();
        }
        return instance;
    }

    DBConnector dbConnector = new DBConnector();

    private MongoCollection<Document> etriCollection;
    private MongoCollection<Document> etriQA;
    private MongoCollection<Document> etriSave;

    public MongoCollection<Document> getetriCollection(){
        return etriCollection;
    }
    public MongoCollection<Document> getEtriQA(){
        return etriQA;
    }
    public MongoCollection<Document> getEtriSave(){
        return etriSave;
    }

    //디비이름
    //morph : etri
    //wsd : etri_wsd
    //ne : etri_ne
    String dbName = "etri_patent_morp";
    public void getEtriCollection(){
        dbConnector.connectorServer();
        if (userName.equals("민숙")) {
            etriCollection = dbConnector.getCollection(dbName, "Group3");
        }else if (userName.equals("희연")) {
            etriCollection = dbConnector.getCollection(dbName, "Group1");
        }else if (userName.equals("덕진")) {
            etriCollection = dbConnector.getCollection(dbName, "Group4");
        }else if (userName.equals("영훈")) {
            etriCollection = dbConnector.getCollection(dbName, "Group2");
        }else if (userName.equals("천솔")) {
            etriCollection = dbConnector.getCollection(dbName, "Group2");
        }else if (userName.equals("지원")) {
            etriCollection = dbConnector.getCollection(dbName, "Group3");
        }else if (userName.equals("시은")) {
            etriCollection = dbConnector.getCollection(dbName, "Group4");
        }else if (userName.equals("현진")) {
            etriCollection = dbConnector.getCollection(dbName, "Group1");
        }else if(userName.equals("실험용")){
            etriCollection = dbConnector.getCollection(dbName, "7-실험용");
        }

        etriQA = dbConnector.getCollection(dbName, "5-Question");
        etriSave = dbConnector.getCollection(dbName, "6-진행상황저장");
    }

    private EtriDB(){
        getEtriCollection();
    }
}
