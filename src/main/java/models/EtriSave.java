package models;

import org.bson.Document;

public class EtriSave {
    private String name;
    private String file;
    private int sentenceIndex;
    private int morpIndex;
    private int time;

    public EtriSave(Document document){
        name = document.get("name") != null ? String.valueOf(document.get("name")) : null;
        file = document.get("file") != null ? String.valueOf(document.get("file")) : null;
        sentenceIndex = document.get("sentenceIndex") != null ? (int) document.get("sentenceIndex") : null;
        morpIndex = document.get("morpIndex") != null ? (int) document.get("morpIndex") : null;
        time = document.get("time") != null ? (int) document.get("time") : null;
    }

    public int getTime() {
        return time;
    }

    public void setTime(final int time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getFile() {
        return file;
    }

    public void setFile(final String file) {
        this.file = file;
    }

    public int getSentenceIndex() {
        return sentenceIndex;
    }

    public void setSentenceIndex(final int sentenceIndex) {
        this.sentenceIndex = sentenceIndex;
    }

    public int getMorpIndex() {
        return morpIndex;
    }

    public void setMorpIndex(final int morpIndex) {
        this.morpIndex = morpIndex;
    }
}
