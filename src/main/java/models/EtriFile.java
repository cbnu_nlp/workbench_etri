package models;

import models.entity.Entity0;
import models.metainfo.Metainfo;
import models.sentence.Sentence;
import models.title.Title;
import org.bson.BsonInt32;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;

public class EtriFile {
    private ObjectId _id;
    private String doc_id;
    private String dct;
    private String category;
    private int category_weight;

    private Title title;
    private Metainfo metainfo;
    private List<Sentence> sentenceList;
    private List<Entity0> entityList;

    public EtriFile(Document document){
        _id = document.get("_id") != null ? (ObjectId) document.get("_id") : null;
        doc_id = document.get("doc_id") != null ? (String) document.get("doc_id") : null;
        dct = document.get("DCT") != null ? (String) document.get("DCT") : null;
        category = document.get("category") != null ? (String) document.get("category") : null;
        category_weight = (int) (document.get("category_weight") != null ? document.get("category_weight") : null);

        title = new Title((Document)document.get("title"));
//        metainfo = new Metainfo((Document)document.get("metaInfo"));

        sentenceList = new ArrayList<>();

        for (Document document1 : (List<Document>) document.get("sentence"))
            sentenceList.add(new Sentence(document1));
//        for(Document document1 : (List<Document>)document.get("entity"))
//            entityList.add(new Entity0(document1));
    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(final ObjectId _id) {
        this._id = _id;
    }

    public String getDoc_id() {
        return doc_id;
    }

    public void setDoc_id(final String doc_id) {
        this.doc_id = doc_id;
    }

    public String getDct() {
        return dct;
    }

    public void setDct(final String dct) {
        this.dct = dct;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(final String category) {
        this.category = category;
    }

    public int getCategory_weight() {
        return category_weight;
    }

    public void setCategory_weight(final int category_weight) {
        this.category_weight = category_weight;
    }

    public Title getTitle() {
        return title;
    }

    public void setTitle(final Title title) {
        this.title = title;
    }

    public Metainfo getMetainfo() {
        return metainfo;
    }

    public void setMetainfo(final Metainfo metainfo) {
        this.metainfo = metainfo;
    }

    public List<Sentence> getSentenceList() {
        return sentenceList;
    }

    public void setSentenceList(final List<Sentence> sentenceList) {
        this.sentenceList = sentenceList;
    }

    public List<Entity0> getEntityList() {
        return entityList;
    }

    public void setEntityList(final List<Entity0> entityList) {
        this.entityList = entityList;
    }
}
