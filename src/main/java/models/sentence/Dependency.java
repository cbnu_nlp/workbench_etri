package models.sentence;

import models.sentence.dependency.Mod;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

public class Dependency {
    private int id;
    private String text;
    private String head;
    private String label;
    private List<Mod> modList = new ArrayList<>();
    private Object weight;

    private int modCount = 0;

    public Dependency(Document document){
        id = document.get("id") != null ? (int) document.get("id") : null;
        text = document.get("text") != null ? document.get("text").toString() : null;
        head = document.get("head") != null ? document.get("head").toString() : null;
        label = document.get("label") != null ? (String) document.get("label") : null;
        for(Integer document1 : (List<Integer>)document.get("mod")) {
            modList.add(new Mod(document1));
        }
        try {
            weight = document.get("weight") != null ? (Double) document.get("weight") : null;
        }catch (ClassCastException e){
            weight = document.get("weight") != null ? (int) document.get("weight") : null;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(final String text) {
        this.text = text;
    }

    public String getHead() {
        return head;
    }

    public void setHead(final String head) {
        this.head = head;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(final String label) {
        this.label = label;
    }

    public List<Mod> getModList() {
        return modList;
    }

    public void setModList(final List<Mod> modList) {
        this.modList = modList;
    }

    public Object getWeight() {
        return weight;
    }

    public void setWeight(final Object weight) {
        this.weight = weight;
    }
}
