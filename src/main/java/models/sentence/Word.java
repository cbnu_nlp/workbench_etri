package models.sentence;

import org.bson.Document;

public class Word {
    private int id;
    private String text;
    private String type;
    private int begin;
    private int end;

    public Word(Document document){
        id = document.get("id") != null ? (int) document.get("id") : null;
        text = document.get("text") != null ? document.get("text").toString() : null;
        type = document.get("type") != null ? document.get("type").toString() : null;
        begin = document.get("begin") != null ? (int) document.get("begin") : null;
        end = document.get("end") != null ? (int) document.get("end") : null;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(final String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public int getBegin() {
        return begin;
    }

    public void setBegin(final int begin) {
        this.begin = begin;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(final int end) {
        this.end = end;
    }
}
