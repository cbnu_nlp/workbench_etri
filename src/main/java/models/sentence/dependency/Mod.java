package models.sentence.dependency;

import org.bson.Document;

public class Mod {
    private int mods;

    public Mod(int document){
        mods = document;
    }

    public int getMods() {
        return mods;
    }

    public void setMods(final int mods) {
        this.mods = mods;
    }
}
