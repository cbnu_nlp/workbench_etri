package models.sentence;

import models.sentence.phrase_dependency.Element;
import models.sentence.phrase_dependency.Sub_phrase;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

public class Phrase_dependency {
    private int id;
    private String label;
    private String text;
    private int begin;
    private int end;
    private int key_begin;
    private int head_phrase;
    private List<Sub_phrase> sub_phraseList = new ArrayList<>();
    private Object weight;
    private List<Element> elementList = new ArrayList<>();

    public Phrase_dependency(Document document){
        id = document.get("id") != null ? (int) document.get("id") : null;
        label = document.get("label") != null ? (String) document.get("label") : null;
        text = document.get("text") != null ? (String) document.get("text") : null;
        begin = document.get("begin") != null ? (int) document.get("begin") : null;
        end = document.get("end") != null ? (int) document.get("end") : null;
        key_begin = document.get("key_begin") != null ? (int) document.get("key_begin") : null;
        head_phrase = document.get("head_phrase") != null ? (int) document.get("head_phrase") : null;

        for(Integer document1 : (List<Integer>)document.get("sub_phrase"))
            sub_phraseList.add(new Sub_phrase(document1));

        try {
            weight = document.get("weight") != null ? (Double) document.get("weight") : null;
        }catch (ClassCastException e){
            weight = document.get("weight") != null ? (int) document.get("weight") : null;
        }

        for(Document document1 : (List<Document>)document.get("element"))
            elementList.add(new Element(document1));
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(final String label) {
        this.label = label;
    }

    public String getText() {
        return text;
    }

    public void setText(final String text) {
        this.text = text;
    }

    public int getBegin() {
        return begin;
    }

    public void setBegin(final int begin) {
        this.begin = begin;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(final int end) {
        this.end = end;
    }

    public int getKey_begin() {
        return key_begin;
    }

    public void setKey_begin(final int key_begin) {
        this.key_begin = key_begin;
    }

    public int getHead_phrase() {
        return head_phrase;
    }

    public void setHead_phrase(final int head_phrase) {
        this.head_phrase = head_phrase;
    }

    public List<Sub_phrase> getSub_phraseList() {
        return sub_phraseList;
    }

    public void setSub_phraseList(final List<Sub_phrase> sub_phraseList) {
        this.sub_phraseList = sub_phraseList;
    }

    public Object getWeight() {
        return weight;
    }

    public void setWeight(final Object weight) {
        this.weight = weight;
    }

    public List<Element> getElementList() {
        return elementList;
    }

    public void setElementList(final List<Element> elementList) {
        this.elementList = elementList;
    }
}
