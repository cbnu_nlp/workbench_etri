package models.sentence;

import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

public class Sentence {
    private int id;
    private String reserve_str;
    private String text;

    private List<Morp> morpList;
    private List<Morp_eval> morp_evalList;
    private List<WSD> wsdList;
    private List<Word> wordList;
    private List<NE> neList;
    private List<Chunk> chunkList;
    private List<Dependency> dependencyList;
    private List<Phrase_dependency> phrase_dependencyList;
    private List<SRL> srlList;
    private List<Relation0> relation0List;
    private List<SA> saList;
    private List<ZA> zaList;

    public Sentence(Document document){
        id = document.get("id") != null ? (int) document.get("id") : null;
        reserve_str = document.get("reserve_str") != null ? (String) document.get("reserve_str") : null;
        text = document.get("text") != null ? (String) document.get("text") : null;

        morpList = new ArrayList<>();
        wsdList = new ArrayList<>();
        neList = new ArrayList<>();
        wordList = new ArrayList<>();
        morp_evalList = new ArrayList<>();
        dependencyList = new ArrayList<>();
        chunkList = new ArrayList<>();
        srlList = new ArrayList<>();
        relation0List = new ArrayList<>();
        saList = new ArrayList<>();
        zaList = new ArrayList<>();

        try{
            for(Document document1 : (List<Document>)document.get("morp"))
                morpList.add(new Morp(document1));
        }catch(NullPointerException e){
            System.out.println("NullPointerException : morp");
        }

        try{
            for(Document document1 : (List<Document>)document.get("WSD"))
                wsdList.add(new WSD(document1));
        }catch(NullPointerException e){
            System.out.println("NullPointerException : WSD");
        }

        try {
            for (Document document1 : (List<Document>) document.get("NE"))
                neList.add(new NE(document1));
        }catch(NullPointerException e){
            System.out.println("NullPointerException : NE");
        }

        try {
            for (Document document1 : (List<Document>) document.get("word"))
                wordList.add(new Word(document1));
        }catch (NullPointerException e){
            System.out.println("NullPointerException : word");
        }

        try {
            for (Document document1 : (List<Document>) document.get("morp_eval"))
                morp_evalList.add(new Morp_eval(document1));
        }catch (NullPointerException e ){
            System.out.println("NullPointerException : morp_eval");
        }

        try{
            for(Document document1 : (List<Document>)document.get("dependency"))
                dependencyList.add(new Dependency(document1));
        }catch (NullPointerException e ){
            System.out.println("NullPointerException : dependency");
        }

        try{
            for(Document document1 : (List<Document>)document.get("chunk"))
                chunkList.add(new Chunk(document1));
        }catch (NullPointerException e ){
            System.out.println("NullPointerException : chunk");
        }

        try{
            for(Document document1 : (List<Document>)document.get("SRL"))
                srlList.add(new SRL(document1));
        }catch (NullPointerException e ){
            System.out.println("NullPointerException : SRL");
        }

        try{
            for(Document document1 : (List<Document>)document.get("relation"))
                relation0List.add(new Relation0(document1));
        }catch (NullPointerException e ){
            System.out.println("NullPointerException : relation");
        }

        try{
            for(Document document1 : (List<Document>)document.get("SA"))
                saList.add(new SA(document1));
        }catch (NullPointerException e ){
            System.out.println("NullPointerException : SA");
        }

        try{
            for(Document document1 : (List<Document>)document.get("ZA"))
                zaList.add(new ZA(document1));
        }catch (NullPointerException e ){
            System.out.println("NullPointerException : ZA");
        }

        phrase_dependencyList = new ArrayList<>();
        try {
            for (Document document1 : (List<Document>) document.get("phrase_dependency"))
                phrase_dependencyList.add(new Phrase_dependency(document1));
        }catch (NullPointerException e){
            System.out.println("NullPointerException : phrase_dependency");
        }
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getReserve_str() {
        return reserve_str;
    }

    public void setReserve_str(final String reserve_str) {
        this.reserve_str = reserve_str;
    }

    public String getText() {
        return text;
    }

    public void setText(final String text) {
        this.text = text;
    }

    public List<Morp> getMorpList() {
        return morpList;
    }

    public void setMorpList(final List<Morp> morpList) {
        this.morpList = morpList;
    }

    public List<Morp_eval> getMorp_evalList() {
        return morp_evalList;
    }

    public void setMorp_evalList(final List<Morp_eval> morp_evalList) {
        this.morp_evalList = morp_evalList;
    }

    public List<WSD> getWsdList() {
        return wsdList;
    }

    public void setWsdList(final List<WSD> wsdList) {
        this.wsdList = wsdList;
    }

    public List<Word> getWordList() {
        return wordList;
    }

    public void setWordList(final List<Word> wordList) {
        this.wordList = wordList;
    }

    public List<NE> getNeList() {
        return neList;
    }

    public void setNeList(final List<NE> neList) {
        this.neList = neList;
    }

    public List<Chunk> getChunkList() {
        return chunkList;
    }

    public void setChunkList(final List<Chunk> chunkList) {
        this.chunkList = chunkList;
    }

    public List<Dependency> getDependencyList() {
        return dependencyList;
    }

    public void setDependencyList(final List<Dependency> dependencyList) {
        this.dependencyList = dependencyList;
    }

    public List<Phrase_dependency> getPhrase_dependencyList() {
        return phrase_dependencyList;
    }

    public void setPhrase_dependencyList(final List<Phrase_dependency> phrase_dependencyList) {
        this.phrase_dependencyList = phrase_dependencyList;
    }

    public List<SRL> getSrlList() {
        return srlList;
    }

    public void setSrlList(final List<SRL> srlList) {
        this.srlList = srlList;
    }

    public List<Relation0> getRelation0List() {
        return relation0List;
    }

    public void setRelation0List(final List<Relation0> relation0List) {
        this.relation0List = relation0List;
    }

    public List<SA> getSaList() {
        return saList;
    }

    public void setSaList(final List<SA> saList) {
        this.saList = saList;
    }

    public List<ZA> getZaList() {
        return zaList;
    }

    public void setZaList(final List<ZA> zaList) {
        this.zaList = zaList;
    }
}
