package models.sentence;

import models.sentence.srl.Argument;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

public class SRL {
    private String verb;
    private int sense;
    private int word_id;
    private Object weight;
    private List<Argument> argumentList = new ArrayList<>();

    public SRL(Document document) {
        verb = document.get("verb") != null ? document.get("verb").toString() : null;
        sense = document.get("sense") != null ? (int) document.get("sense") : null;
        word_id = document.get("word_id") != null ? (int) document.get("word_id") : null;
        try {
            weight = document.get("weight") != null ? (Double) document.get("weight") : null;
        }catch (ClassCastException e){
            weight = document.get("weight") != null ? (int) document.get("weight") : null;
        }
        for(Document document1 : (List<Document>)document.get("argument"))
            argumentList.add(new Argument(document1));
    }

    public String getVerb() {
        return verb;
    }

    public void setVerb(final String verb) {
        this.verb = verb;
    }

    public int getSense() {
        return sense;
    }

    public void setSense(final int sense) {
        this.sense = sense;
    }

    public int getWord_id() {
        return word_id;
    }

    public void setWord_id(final int word_id) {
        this.word_id = word_id;
    }

    public Object getWeight() {
        return weight;
    }

    public void setWeight(final Object weight) {
        this.weight = weight;
    }

    public List<Argument> getArgumentList() {
        return argumentList;
    }

    public void setArgumentList(final List<Argument> argumentList) {
        this.argumentList = argumentList;
    }
}
