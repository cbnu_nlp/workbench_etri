package models.sentence;

import org.bson.Document;

public class WSD {
    private int id;
    private String text;
    private String type;
    private String scode;
    private Object weight;
    private int position;
    private int begin;
    private int end;

    public WSD(Document document){
        id = (int) (document.get("id") != null ? document.get("id") : null);
        text = document.get("text") != null ? document.get("text").toString() : null;
        type = document.get("type") != null ? document.get("type").toString() : null;
        scode = document.get("scode") != null ? document.get("scode").toString() : null;
        try {
            weight = (Double) ((document.get("weight") != null ? document.get("weight") : null));
        }catch (ClassCastException e){
            weight = Double.valueOf((int) (document.get("weight") != null ? document.get("weight") : null));
        }
        position = (int) (document.get("position") != null ? document.get("position") : null);
        begin = (int) (document.get("begin") != null ? document.get("begin") : null);
        end = (int) (document.get("end") != null ? document.get("end") : null);
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(final String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public String getScode() {
        return scode;
    }

    public void setScode(final String scode) {
        this.scode = scode;
    }

    public Object getWeight() {
        return weight;
    }

    public void setWeight(final Object weight) {
        this.weight = weight;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(final int position) {
        this.position = position;
    }

    public int getBegin() {
        return begin;
    }

    public void setBegin(final int begin) {
        this.begin = begin;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(final int end) {
        this.end = end;
    }
}
