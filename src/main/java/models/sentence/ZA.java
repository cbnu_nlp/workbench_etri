package models.sentence;

import org.bson.Document;

public class ZA {
    private int id;
    private int verb_wid;
    private int ant_sid;
    private int ant_wid;
    private String type;
    private int istitle;
    private Object weight;

    public ZA(Document document) {
        id = document.get("id") != null ? (int) document.get("id") : null;
        verb_wid = document.get("verb_wid") != null ? (int) document.get("verb_wid") : null;
        ant_sid = document.get("ant_sid") != null ? (int) document.get("ant_sid") : null;
        ant_wid = document.get("ant_wid") != null ? (int) document.get("ant_wid") : null;
        type = document.get("type") != null ? document.get("type").toString() : null;
        istitle = document.get("istitle") != null ? (int) document.get("istitle") : null;
        try {
            weight = document.get("weight") != null ? (Double) document.get("weight") : null;
        }catch (ClassCastException e){
            weight = document.get("weight") != null ? (int) document.get("weight") : null;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public int getVerb_wid() {
        return verb_wid;
    }

    public void setVerb_wid(final int verb_wid) {
        this.verb_wid = verb_wid;
    }

    public int getAnt_sid() {
        return ant_sid;
    }

    public void setAnt_sid(final int ant_sid) {
        this.ant_sid = ant_sid;
    }

    public int getAnt_wid() {
        return ant_wid;
    }

    public void setAnt_wid(final int ant_wid) {
        this.ant_wid = ant_wid;
    }

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public int getIstitle() {
        return istitle;
    }

    public void setIstitle(final int istitle) {
        this.istitle = istitle;
    }

    public Object getWeight() {
        return weight;
    }

    public void setWeight(final Object weight) {
        this.weight = weight;
    }
}
