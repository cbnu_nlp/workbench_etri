package models.sentence.phrase_dependency;

import org.bson.Document;

public class Element {
    private String text;
    private String label;
    private int begin;
    private int end;
    private String ne_type;

    public Element(Document document){
        text = document.get("text") != null ? document.get("text").toString() : null;
        label = document.get("label") != null ? document.get("label").toString() : null;
        begin = document.get("begin") != null ? (int) document.get("begin") : null;
        end = document.get("end") != null ? (int) document.get("end") : null;
        ne_type = document.get("ne_type") != null ? (String) document.get("ne_type") : null;
    }

    public String getText() {
        return text;
    }

    public void setText(final String text) {
        this.text = text;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(final String label) {
        this.label = label;
    }

    public int getBegin() {
        return begin;
    }

    public void setBegin(final int begin) {
        this.begin = begin;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(final int end) {
        this.end = end;
    }

    public String getNe_type() {
        return ne_type;
    }

    public void setNe_type(final String ne_type) {
        this.ne_type = ne_type;
    }
}
