package models.sentence.phrase_dependency;

import org.bson.Document;

public class Sub_phrase {
    private int sub_phrases;

    public Sub_phrase(int document){
        sub_phrases = document;
    }

    public int getSub_phrases() {
        return sub_phrases;
    }

    public void setSub_phrases(final int sub_phrases) {
        this.sub_phrases = sub_phrases;
    }
}
