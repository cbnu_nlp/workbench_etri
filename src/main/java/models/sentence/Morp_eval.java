package models.sentence;

import org.bson.Document;

public class Morp_eval {
    private int id;
    private String result;
    private String target;
    private int word_id;
    private int m_begin;
    private int m_end;

    public Morp_eval(Document document){
        id = document.get("id") != null ? (int) document.get("id") : null;
        result = document.get("result") != null ? document.get("result").toString() : null;
        target = document.get("target") != null ? document.get("target").toString() : null;
        word_id = document.get("word_id") != null ? (int) document.get("word_id") : null;
        m_begin = document.get("m_begin") != null ? (int) document.get("m_begin") : null;
        m_end = document.get("m_end") != null ? (int) document.get("m_end") : null;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getResult() {
        return result;
    }

    public void setResult(final String result) {
        this.result = result;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(final String target) {
        this.target = target;
    }

    public int getWord_id() {
        return word_id;
    }

    public void setWord_id(final int word_id) {
        this.word_id = word_id;
    }

    public int getM_begin() {
        return m_begin;
    }

    public void setM_begin(final int m_begin) {
        this.m_begin = m_begin;
    }

    public int getM_end() {
        return m_end;
    }

    public void setM_end(final int m_end) {
        this.m_end = m_end;
    }
}
