package models.sentence;

import org.bson.Document;

public class Morp {
    private int id;
    private String lemma;
    private String type;
    private int position;
    private Object weight;

    private String remove;      //알짜+힘 -> 알짜힘
    private String division;    //알짜힘 -> 알짜+힘

    private String wrong;

    public Morp(Document document){
        id = document.get("id") != null ? (int) document.get("id") : null;
        lemma = document.get("lemma") != null ? document.get("lemma").toString() : null;
        type = document.get("type") != null? document.get("type").toString() : null;
        position = document.get("position") != null ? (int) document.get("position") : null;
        try {
            weight = document.get("weight") != null ? (Double) document.get("weight") : null;
        }catch (ClassCastException e){
            weight = document.get("weight") != null ? (int) document.get("weight") : null;
        }

//        remove = document.get("remove") != null ? String.valueOf(document.get("remove")) : null;
//        division = document.get("division") != null ? String.valueOf(document.get("division")) : null;
//        wrong = document.get("wrong") != null ? String.valueOf(document.get("wrong")) : null;
    }

    public String getWrong() {
        return wrong;
    }

    public void setWrong(final String wrong) {
        this.wrong = wrong;
    }

    public String getRemove() {
        return remove;
    }

    public void setRemove(final String remove) {
        this.remove = remove;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(final String division) {
        this.division = division;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getLemma() {
        return lemma;
    }

    public void setLemma(final String lemma) {
        this.lemma = lemma;
    }

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(final int position) {
        this.position = position;
    }

    public Object getWeight() {
        return weight;
    }

    public void setWeight(final Object weight) {
        this.weight = weight;
    }
}
