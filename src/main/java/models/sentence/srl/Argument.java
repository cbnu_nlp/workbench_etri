package models.sentence.srl;

import org.bson.Document;

public class Argument {
    private String type;
    private int word_id;
    private String text;
    private Object weight;

    public Argument(Document document){
        type = document.get("type") != null ? document.get("type").toString() : null;
        word_id = document.get("word_id") != null ? (int) document.get("word_id") : null;
        text = document.get("text") != null ? document.get("text").toString() : null;
        try{
            weight = document.get("weight") != null ? (Double) document.get("weight") : null;
        }catch (ClassCastException e){
            weight = document.get("weight") != null ? (int) document.get("weight") : null;
        }
    }

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public int getWord_id() {
        return word_id;
    }

    public void setWord_id(final int word_id) {
        this.word_id = word_id;
    }

    public String getText() {
        return text;
    }

    public void setText(final String text) {
        this.text = text;
    }

    public Object getWeight() {
        return weight;
    }

    public void setWeight(final Object weight) {
        this.weight = weight;
    }
}
