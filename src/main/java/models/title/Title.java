package models.title;

import org.bson.Document;

public class Title {
    private String text = "";
    private String NE = "";

    public Title(Document document){
        text = document.get("text") != null ? document.get("text").toString() : null;
        NE = document.get("NE") != null ? document.get("NE").toString() : null;
    }

    public String getText() {
        return text;
    }

    public void setText(final String text) {
        this.text = text;
    }

    public String getNE() {
        return NE;
    }

    public void setNE(final String NE) {
        this.NE = NE;
    }
}
