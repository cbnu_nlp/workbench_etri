package search_kor_dic;

import javafx.beans.property.SimpleStringProperty;

public class SearchKorData {
    private SimpleStringProperty tableHomograph = new SimpleStringProperty("");
    private SimpleStringProperty tableMean = new SimpleStringProperty("");
    private SimpleStringProperty tableEx = new SimpleStringProperty("");
    private SimpleStringProperty tablePos = new SimpleStringProperty("");

    public SearchKorData(){
        this("", "", "", "");
    }

    public SearchKorData(String tableHomograph, String tableMean, String tableEx, String tablePos){
        setTableHomograph(tableHomograph);
        setTableMean(tableMean);
        setTableEx(tableEx);
        setTablePos(tablePos);
    }

    public String getTableHomograph() {
        return tableHomograph.get();
    }

    public SimpleStringProperty tableHomographProperty() {
        return tableHomograph;
    }

    public void setTableHomograph(final String tableHomograph) {
        this.tableHomograph.set(tableHomograph);
    }

    public String getTableMean() {
        return tableMean.get();
    }

    public SimpleStringProperty tableMeanProperty() {
        return tableMean;
    }

    public void setTableMean(final String tableMean) {
        this.tableMean.set(tableMean);
    }

    public String getTableEx() {
        return tableEx.get();
    }

    public SimpleStringProperty tableExProperty() {
        return tableEx;
    }

    public void setTableEx(final String tableEx) {
        this.tableEx.set(tableEx);
    }

    public String getTablePos() {
        return tablePos.get();
    }

    public SimpleStringProperty tablePosProperty() {
        return tablePos;
    }

    public void setTablePos(final String tablePos) {
        this.tablePos.set(tablePos);
    }
}
