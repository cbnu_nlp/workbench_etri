import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application{
    public void start(final Stage stage)throws Exception{
        Parent parent = FXMLLoader.load(getClass().getResource("/fxml/morph.fxml"));
        Scene scene = new Scene(parent);
        stage.setScene(scene);
        stage.setTitle("Etri_Workbench");
        stage.show();
    }

    static public void main(String[] args) throws InterruptedException{
        launch(args);
    }
}
