package controller.page;

import com.mongodb.BasicDBObject;
import com.mongodb.client.model.Projections;
import dbconnector.EtriDB;
import dbconnector.KorDic;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import models.EtriFile;
import models.EtriSave;
import models.sentence.Chunk;
import models.sentence.Dependency;
import models.sentence.Morp;
import models.sentence.Morp_eval;
import models.sentence.NE;
import models.sentence.Phrase_dependency;
import models.sentence.Relation0;
import models.sentence.SA;
import models.sentence.SRL;
import models.sentence.Sentence;
import models.sentence.WSD;
import models.sentence.Word;
import models.sentence.ZA;
import models.title.Title;
import org.bson.Document;
import search_kor_dic.SearchKorData;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import static com.mongodb.client.model.Filters.eq;

public class WsdPage implements Initializable{

    @FXML private ListView<String> list_file;
    @FXML private ListView<String> list_id;
    @FXML private ListView<String> list_wsd;
    @FXML private ListView<String> list_viewMorp;

    EtriDB etriDB;
    EtriSave etriSave;
    long startTime = System.currentTimeMillis();
    long endTime;
    int tempTime;
    //디비 접속 버튼
    String qaName = "지정되지 않은 사용자";  //qa할때 저장할 이름
    public void ConnectDBButton(MouseEvent mouseEvent) {
        final Button button = (Button) mouseEvent.getSource();
        final String buttonText = button.getText();

        List<String> tempList = new ArrayList<>();
        if (buttonText.equals("민숙") && InfoQA.getText().equals("3숙")) {
            etriDB = EtriDB.getInstance(buttonText);
            qaName = "민숙";
        } else if (buttonText.equals("희연") && InfoQA.getText().equals("1연")) {
            etriDB = EtriDB.getInstance(buttonText);
            qaName = "희연";
        } else if (buttonText.equals("덕진") && InfoQA.getText().equals("4덕진")) {
            etriDB = EtriDB.getInstance(buttonText);
            qaName = "덕진";
        } else if (buttonText.equals("영훈") && InfoQA.getText().equals("훈2")) {
            etriDB = EtriDB.getInstance(buttonText);
            qaName = "영훈";
        }else if (buttonText.equals("천솔") && InfoQA.getText().equals("천2")) {
            etriDB = EtriDB.getInstance(buttonText);
            qaName = "천솔";
        } else if (buttonText.equals("지원") && InfoQA.getText().equals("3원")) {
            etriDB = EtriDB.getInstance(buttonText);
            qaName = "지원";
        } else if (buttonText.equals("시은") && InfoQA.getText().equals("4시은")) {
            etriDB = EtriDB.getInstance(buttonText);
            qaName = "시은";
        } else if (buttonText.equals("현진") && InfoQA.getText().equals("1진")) {
            etriDB = EtriDB.getInstance(buttonText);
            qaName = "현진";
        } else if(buttonText.equals("실험용") && InfoQA.getText().equals("nlp456")){
            etriDB = EtriDB.getInstance(buttonText);
            qaName = "실험용";
        }
        for (Document document : etriDB.getetriCollection().find().projection(Projections.include("title"))) {
            tempList.add(new Title((Document) document.get("title")).getText());
        }
        list_file.setItems(FXCollections.observableArrayList(tempList));

        String loadText = "";
        for(Document document: etriDB.getEtriSave().find(eq("name", qaName))) {
            loadText = "마지막으로 태깅한 위치 : " + document.get("name") + " " + document.get("file")
                       + " " + document.get("sentenceIndex") + " " + document.get("morpIndex");
            tempTime = (int) document.get("time");
        }
        InfoQA.setText(loadText);
        workTime.setText(String.valueOf(tempTime));

        startTime = System.currentTimeMillis();
    }

    //파일 리스트 불러오기 -> ConnectDBButton으로 대체
    List<String> korWordList;
    List<String> korHomographList;
    List<String> korMeanList;
    List<String> korPosList;
    List<String> korExList;
    public void initialize(final URL url, final ResourceBundle resourceBundle){
        //표준국어대사전 단어 저장
        korWordList = new ArrayList<>();
        korHomographList = new ArrayList<>();
        korMeanList = new ArrayList<>();
        korPosList = new ArrayList<>();
        korExList = new ArrayList<>();
        for (Document document : korDic.getKorStandardDic().find().projection(Projections.include())) {
            korWordList.add(document.get("word").toString());
            korHomographList.add(document.get("homograph").toString());
            korMeanList.add(document.get("mean").toString());
            korPosList.add(document.get("pos").toString());
            korExList.add(document.get("ex").toString());
        }
    }

    EtriFile getEtriFile;
    //파일 리스트 내부 문장 리스트 불러오기
    String fileTitle;
    public void clickFile(MouseEvent mouseEvent) {
        fileTitle = list_file.getSelectionModel().getSelectedItem();

        getEtriFile = new EtriFile(etriDB.getetriCollection().find(eq("title.text", fileTitle)).iterator().next());

        List<String> tempIDList = new ArrayList<>();
        for(int i=0; i<getEtriFile.getSentenceList().size(); i++)
            tempIDList.add(String.valueOf(getEtriFile.getSentenceList().get(i).getId()));
        list_id.setItems(FXCollections.observableArrayList(tempIDList));

        initSentence();
        initLemmaType();
        initListMorp();
    }

    List<Sentence> getSentence;
    List<WSD> getWsd;
    List<Morp> getMorp;
    @FXML private Text sentenceText;
    //파일 리스트 내부 문장 내부 형태소 분석 리스트 불러오기
    private int sentenceIndex;
    public void clickId(MouseEvent mouseEvent) {
        sentenceIndex = list_id.getSelectionModel().getSelectedIndex();
        sentenceEvent(sentenceIndex);
    }
    private void sentenceEvent(int index){
        getSentence = getEtriFile.getSentenceList();
        getWsd = getSentence.get(index).getWsdList();

        String sentence = getSentence.get(index).getText();
        List<String> tempWsdList = new ArrayList<>();
        for(int i=0; i<getWsd.size(); i++) {
            tempWsdList.add(getWsd.get(i).getId() + " " + getWsd.get(i).getText());
        }
        list_wsd.setItems(FXCollections.observableArrayList(tempWsdList));

        sentenceText.setText(sentence);


        getMorp = getSentence.get(index).getMorpList();
        List<String> tempMorpList = new ArrayList<>();
        for(int i=0; i<getMorp.size(); i++){
            tempMorpList.add(getMorp.get(i).getId() + " " + getMorp.get(i).getLemma());
        }
        list_viewMorp.setItems(FXCollections.observableArrayList(tempMorpList));

        initLemmaType();
    }

    @FXML private TextField lemmaText;
    @FXML private TextField typeText;
    @FXML private Text weightText;  //1이상 값들이 있음
    @FXML private Text scodeText;
    @FXML private TextField positionText;
    @FXML private TextField beginText;
    @FXML private TextField endText;
    //파일 리스트 내부 문장 내부 형태소 분석 결과 불러오기
    private int tagIndex;
    public void clickWSD(MouseEvent mouseEvent){
        tagIndex = list_wsd.getSelectionModel().getSelectedIndex();
        wsdEvent(tagIndex);
    }

    public void pressWSD(KeyEvent keyEvent) {
        if(keyEvent.getCode() == KeyCode.UP){
            if(tagIndex > 0) {
                tagIndex--;
                wsdEvent(tagIndex);
            }
        }
        if(keyEvent.getCode() == KeyCode.DOWN){
            if(tagIndex+1 < getWsd.size()) {
                tagIndex++;
                wsdEvent(tagIndex);
            }
        }
    }

    private void wsdEvent(int index){
        lemmaText.setText(getWsd.get(index).getText());
        typeText.setText(getWsd.get(index).getType());
        weightText.setText(String.valueOf(getWsd.get(index).getWeight()));
        scodeText.setText(getWsd.get(index).getScode());
        positionText.setText(String.valueOf(getWsd.get(index).getPosition()));
        beginText.setText(String.valueOf(getWsd.get(index).getBegin()));
        endText.setText(String.valueOf(getWsd.get(index).getEnd()));


        //표준국어대사전 검색
        String tempWord = getWsd.get(index).getText();
        data = tableKorView.getItems();
        data.removeAll(tableKorView.getItems());
        for(int i = 0; i < korWordList.size(); i++){
            if(tempWord.equals(korWordList.get(i))){
                data.add(new SearchKorData(korHomographList.get(i), korMeanList.get(i), korExList.get(i), korPosList.get(i)));
            }
        }
    }


    private void initSentence(){
        sentenceText.setText("");
    }
    private void initLemmaType(){
        lemmaText.setText("");
        typeText.setText("");
        weightText.setText("");
        scodeText.setText("");
    }
    private void initListMorp(){
        list_wsd.setItems(FXCollections.observableArrayList(""));
    }


    //표준국어대사전 검색
    @FXML private TextField findKorWord;
    @FXML private TableView<SearchKorData> tableKorView = new TableView<>();
    KorDic korDic = KorDic.getInstance();
    ObservableList<SearchKorData> data = tableKorView.getItems();
    private void korView(String word){
        data = tableKorView.getItems();
        data.removeAll(tableKorView.getItems());
        for(int i=0; i<korWordList.size(); i++){
            if(korWordList.get(i).equals(word))
                data.add(new SearchKorData(korHomographList.get(i), korMeanList.get(i), korExList.get(i), korPosList.get(i)));
        }
    }

    public void korWordFind(KeyEvent keyEvent) {
        if(keyEvent.getCode() == KeyCode.ENTER){
            String inputText = findKorWord.getText();
            korView(inputText);
            findKorWord.clear();
        }
    }

    //나머지 업데이트용
    List<Morp> updateMorp;
    List<NE> updateNe;
    List<Word> updateWord;
    List<ZA> updateZa;
    List<SRL> updateSrl;
    List<Relation0> updateRelation;
    List<Dependency> updateDependency;
    List<SA> updateSa;
    List<Morp_eval> updateMorpEval;
    List<Chunk> updateChunk;
    List<Phrase_dependency> updatePhraseDependency;
    Document[] arrayMorp;
    Document[] arrayNe;
    Document[] arrayWord;
    Document[] arrayZa;
    Document[] arraySrl;
    Document[] arrayRelation;
    Document[] arrayDependency;
    Document[] arraySa;
    Document[] arrayMorpEval;
    Document[] arrayChunk;
    Document[] arrayPhraseDependency;
    private void AnotherUpdate(List<Sentence> tempSentence, int index){
        updateMorp = tempSentence.get(index).getMorpList();
        updateNe = tempSentence.get(index).getNeList();
        updateWord = tempSentence.get(index).getWordList();
        updateZa = tempSentence.get(index).getZaList();
        updateSrl = tempSentence.get(index).getSrlList();
        updateRelation = tempSentence.get(index).getRelation0List();
        updateDependency = tempSentence.get(index).getDependencyList();
        updateSa = tempSentence.get(index).getSaList();
        updateMorpEval = tempSentence.get(index).getMorp_evalList();
        updateChunk = tempSentence.get(index).getChunkList();
        updatePhraseDependency = tempSentence.get(index).getPhrase_dependencyList();

        arrayMorp = new Document[updateMorp.size()];
        arrayNe = new Document[updateNe.size()];
        arrayWord = new Document[updateWord.size()];
        arrayZa = new Document[updateZa.size()];
        arraySrl = new Document[updateSrl.size()];
        arrayRelation = new Document[updateRelation.size()];
        arrayDependency = new Document[updateDependency.size()];
        arraySa = new Document[updateSa.size()];
        arrayMorpEval = new Document[updateMorpEval.size()];
        arrayChunk = new Document[updateChunk.size()];
        arrayPhraseDependency = new Document[updatePhraseDependency.size()];

        for(int i=0; i<updateMorp.size(); i++){
            arrayMorp[i] = new Document("id", updateMorp.get(i).getId()).append("lemma", updateMorp.get(i).getLemma())
                                .append("type", updateMorp.get(i).getType()).append("position", updateMorp.get(i).getPosition())
                                                                        .append("weight", updateMorp.get(i).getWeight());
        }
        for(int i=0; i<updateNe.size(); i++){
            arrayNe[i] = new Document("id", updateNe.get(i).getId()).append("text", updateNe.get(i).getText())
                                .append("type", updateNe.get(i).getType()).append("weight", updateNe.get(i).getWeight())
                                .append("begin", updateNe.get(i).getBegin()).append("end", updateNe.get(i).getEnd())
                                                                    .append("common_noun", updateNe.get(i).getCommon_noun());
        }
        for(int i=0; i<updateWord.size(); i++){
            arrayWord[i] = new Document("id", updateWord.get(i).getId()).append("text", updateWord.get(i).getText())
                                .append("begin", updateWord.get(i).getBegin()).append("end", updateWord.get(i).getEnd())
                                                                        .append("type", updateWord.get(i).getType());
        }
        for(int i=0; i<updateWord.size(); i++){
            arrayWord[i] = new Document("id", updateWord.get(i).getId()).append("text", updateWord.get(i).getText())
                                                                        .append("type", updateWord.get(i).getType())
                                                                        .append("begin", updateWord.get(i).getBegin())
                                                                        .append("end", updateWord.get(i).getEnd());
        }
        for(int i=0; i<updateZa.size(); i++){
            arrayZa[i] = new Document("id", updateZa.get(i).getId()).append("verb_wid", updateZa.get(i).getVerb_wid())
                                                                    .append("ant_sid", updateZa.get(i).getAnt_sid())
                                                                    .append("ant_wid", updateZa.get(i).getAnt_wid())
                                                                    .append("type", updateZa.get(i).getType())
                                                                    .append("istitle", updateZa.get(i).getIstitle())
                                                                    .append("weight", updateZa.get(i).getWeight());
        }
        for(int i=0; i<updateSrl.size(); i++){
            Document[] arrayArgument = new Document[updateSrl.get(i).getArgumentList().size()];
            for(int j=0; j<arrayArgument.length; j++){
                arrayArgument[j] = new Document("type", updateSrl.get(i).getArgumentList().get(j).getType())
                                   .append("word_id", updateSrl.get(i).getArgumentList().get(j).getWord_id())
                                   .append("text", updateSrl.get(i).getArgumentList().get(j).getText())
                                   .append("weight", updateSrl.get(i).getArgumentList().get(j).getWeight());
            }
            arraySrl[i] = new Document("verb", updateSrl.get(i).getVerb()).append("sense", updateSrl.get(i).getSense())
                                                                          .append("word_id", updateSrl.get(i).getWord_id())
                                                                          .append("weight", updateSrl.get(i).getWeight())
                                                                          .append("argument", Arrays.asList(arrayArgument));
        }
        for(int i=0; i<updateRelation.size(); i++){
            arrayRelation[i] = new Document();
        }
        for(int i=0; i<updateDependency.size(); i++){
            List<Integer> modList = new ArrayList<>();
            for(int j=0; j<updateDependency.get(i).getModList().size(); j++)
                modList.add(updateDependency.get(i).getModList().get(j).getMods());
            arrayDependency[i] = new Document("id", updateDependency.get(i).getId()).append("text", updateDependency.get(i).getText())
                                                                                    .append("head", updateDependency.get(i).getHead())
                                                                                    .append("label", updateDependency.get(i).getLabel())
                                                                                    .append("mod", modList)
                                                                                    .append("weight", updateDependency.get(i).getWeight());
        }
        for(int i=0; i<updateSa.size(); i++){
            arraySa[i] = new Document();
        }
        for(int i=0; i<updateMorpEval.size(); i++){
            arrayMorpEval[i] = new Document("id", updateMorpEval.get(i).getId()).append("result", updateMorpEval.get(i).getResult())
                                                                                .append("target", updateMorpEval.get(i).getTarget())
                                                                                .append("word_id", updateMorpEval.get(i).getWord_id())
                                                                                .append("m_begin", updateMorpEval.get(i).getM_begin())
                                                                                .append("m_end", updateMorpEval.get(i).getM_end());
        }
        for(int i=0; i<updateChunk.size(); i++){
            arrayChunk[i] = new Document();
            //            arrayChunk[i] = new Document("id", updateChunk.get(i).getId()).append("type", updateChunk.get(i).getType())
            //                                                                          .append("begin", updateChunk.get(i).getBegin())
            //                                                                          .append("end", updateChunk.get(i).getEnd())
            //                                                                          .append("text", updateChunk.get(i).getText());
        }
        for(int i=0; i<updatePhraseDependency.size(); i++){
            List<Integer> subPhraseList = new ArrayList<>();
            for(int j=0; j<updatePhraseDependency.get(i).getSub_phraseList().size(); j++)
                subPhraseList.add(updatePhraseDependency.get(i).getSub_phraseList().get(j).getSub_phrases());
            Document[] arrayElement = new Document[updatePhraseDependency.get(i).getElementList().size()];
            for(int j=0; j<arrayElement.length; j++){
                arrayElement[j] = new Document("text", updatePhraseDependency.get(i).getElementList().get(j).getText())
                                  .append("label", updatePhraseDependency.get(i).getElementList().get(j).getLabel())
                                  .append("begin", updatePhraseDependency.get(i).getElementList().get(j).getBegin())
                                  .append("end", updatePhraseDependency.get(i).getElementList().get(j).getEnd())
                                  .append("ne_type", updatePhraseDependency.get(i).getElementList().get(j).getNe_type());
            }
            arrayPhraseDependency[i] = new Document("id", updatePhraseDependency.get(i).getId())
                                       .append("label", updatePhraseDependency.get(i).getLabel())
                                       .append("text", updatePhraseDependency.get(i).getText())
                                       .append("begin", updatePhraseDependency.get(i).getBegin())
                                       .append("end", updatePhraseDependency.get(i).getEnd())
                                       .append("key_begin", updatePhraseDependency.get(i).getKey_begin())
                                       .append("head_phrase", updatePhraseDependency.get(i).getHead_phrase())
                                       .append("sub_phrase", subPhraseList)
                                       .append("weight", updatePhraseDependency.get(i).getWeight())
                                       .append("element", Arrays.asList(arrayElement));
        }
    }

    //표준국어대사전 상세정보 + 업데이트
    @FXML private TextField InfoQA;
    @FXML private Text korMeanText;
    @FXML private Text korExText;
    private int korTagIndex;
    public void korWordClick(MouseEvent mouseEvent) {
        korTagIndex = tableKorView.getSelectionModel().getFocusedIndex();

        if(mouseEvent.getButton() == MouseButton.PRIMARY){
            korMeanText.setText(data.get(korTagIndex).getTableMean());
            korExText.setText(data.get(korTagIndex).getTableEx());
        }


        //업데이트
        if(mouseEvent.getClickCount() == 2) {
            endTime = System.currentTimeMillis() - startTime - pauseTime;

            Document saveDocument = new Document("name", qaName).append("file", fileTitle)
                                                                .append("sentenceIndex", sentenceIndex).append("morpIndex", tagIndex)
                                                                .append("time", (tempTime + (int) (endTime / 1000)));
            BasicDBObject saveName = new BasicDBObject().append("name", qaName);
            BasicDBObject save = new BasicDBObject("$set", saveDocument);
            etriDB.getEtriSave().updateOne(saveName, save);


            //버튼이 아니라 TableView에 있는 scode를 넣어야됨
            String tempScode = data.get(korTagIndex).getTableHomograph();
            scodeText.setText(tempScode);


            if (list_wsd.getSelectionModel().getSelectedIndex() == -1) {
                //팝업창 경고 뛰워주면 좋을듯
                InfoQA.setText("어절을 선택하고 누르세요.");
            } else {
                InfoQA.clear();
                List<Sentence> updateSentence = getEtriFile.getSentenceList();
                List<WSD> updateWsd;

                Document[] arraySentence = new Document[updateSentence.size()];
                Document[] arrayWsd;

                //텍스트 백업
                String backupText = String.valueOf(list_file.getSelectionModel().getSelectedItems()).replace("[", "").replace("]", "")
                                    + " " + sentenceIndex + " " + tagIndex + " " + tempScode;
                try {
                    String filePath = "C:/Users/Public/Legal_WSD.txt";
                    try {
                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                       Charset.forName("UTF-8"));
                        PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                        printWriter.println(backupText);
                        printWriter.close();
                    } catch (FileNotFoundException e) {
                        File file = new File(filePath);
                        FileWriter fileWriter = new FileWriter(file, true);
                        fileWriter.close();

                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                       Charset.forName("UTF-8"));
                        PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                        printWriter.println(backupText);
                        printWriter.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //텍스트 백업

                for (int i = 0; i < updateSentence.size(); i++) {
                    updateWsd = updateSentence.get(i).getWsdList();
                    arrayWsd = new Document[updateWsd.size()];

                    for (int j = 0; j < updateWsd.size(); j++) {
                        if (i == sentenceIndex && j == tagIndex) {
                            arrayWsd[j] = new Document("text", updateWsd.get(j).getText()).append("type", updateWsd.get(j).getType())
                                                                                          .append("id", updateWsd.get(j).getId())
                                                                                          .append("weight", 1.0)
                                                                                          .append("position",
                                                                                                  updateWsd.get(j).getPosition())
                                                                                          .append("scode", tempScode)
                                                                                          .append("begin", updateWsd.get(j).getBegin())
                                                                                          .append("end", updateWsd.get(j).getEnd());
                            //업데이트된거 바로(?) 볼 수 있게
                            updateWsd.get(j).setScode(tempScode);

                        } else {
                            arrayWsd[j] = new Document("text", updateWsd.get(j).getText()).append("type", updateWsd.get(j).getType())
                                                                                          .append("id", updateWsd.get(j).getId())
                                                                                          .append("weight", updateWsd.get(j).getWeight())
                                                                                          .append("position",
                                                                                                  updateWsd.get(j).getPosition())
                                                                                          .append("scode", updateWsd.get(j).getScode())
                                                                                          .append("begin", updateWsd.get(j).getBegin())
                                                                                          .append("end", updateWsd.get(j).getEnd());
                        }
                    }
                    AnotherUpdate(updateSentence, i);

                    arraySentence[i] = new Document("id", updateSentence.get(i).getId())
                                       .append("reserve_str", updateSentence.get(i).getReserve_str())
                                       .append("text", updateSentence.get(i).getText())
                                       .append("morp", Arrays.asList(arrayMorp))
                                       .append("morp_eval", Arrays.asList(arrayMorpEval))
                                       .append("WSD", Arrays.asList(arrayWsd))
                                       .append("word", Arrays.asList(arrayWord))
                                       .append("NE", Arrays.asList(arrayNe))
                                       .append("chunk", Arrays.asList(arrayChunk))
                                       .append("dependency",Arrays.asList(arrayDependency))
                                       .append("phrase_dependency", Arrays.asList(arrayPhraseDependency))
                                       .append("SRL", Arrays.asList(arraySrl))
                                       .append("relation", Arrays.asList(arrayRelation))
                                       .append("SA", Arrays.asList(arraySa))
                                       .append("ZA", Arrays.asList(arrayZa));
                }

                Document documentSentence = new Document("sentence", Arrays.asList(arraySentence));

                BasicDBObject searchUpdateTitle = new BasicDBObject().append("title.text", getEtriFile.getTitle().getText());
                BasicDBObject update = new BasicDBObject("$set", documentSentence);

                etriDB.getetriCollection().updateOne(searchUpdateTitle, update);
            }
        }
    }


    public void NoScode(MouseEvent mouseEvent) {
        endTime = System.currentTimeMillis() - startTime - pauseTime;

        Document saveDocument = new Document("name", qaName).append("file", fileTitle)
                                                            .append("sentenceIndex", sentenceIndex).append("morpIndex", tagIndex)
                                                            .append("time", (tempTime + (int) (endTime / 1000)));
        BasicDBObject saveName = new BasicDBObject().append("name", qaName);
        BasicDBObject save = new BasicDBObject("$set", saveDocument);
        etriDB.getEtriSave().updateOne(saveName, save);


        //버튼이 아니라 TableView에 있는 scode를 넣어야됨
        String tempScode = "88";
        scodeText.setText(tempScode);


        if (list_wsd.getSelectionModel().getSelectedIndex() == -1) {
            //팝업창 경고 뛰워주면 좋을듯
            InfoQA.setText("어절을 선택하고 누르세요.");
        } else {
            InfoQA.clear();
            List<Sentence> updateSentence = getEtriFile.getSentenceList();
            List<WSD> updateWsd;

            Document[] arraySentence = new Document[updateSentence.size()];
            Document[] arrayWsd;

            //텍스트 백업
            String backupText = String.valueOf(list_file.getSelectionModel().getSelectedItems()).replace("[", "").replace("]", "")
                                + " " + sentenceIndex + " " + tagIndex + " " + tempScode;
            try {
                String filePath = "C:/Users/Public/Legal_WSD.txt";
                try {
                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                   Charset.forName("UTF-8"));
                    PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                    printWriter.println(backupText);
                    printWriter.close();
                } catch (FileNotFoundException e) {
                    File file = new File(filePath);
                    FileWriter fileWriter = new FileWriter(file, true);
                    fileWriter.close();

                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                   Charset.forName("UTF-8"));
                    PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                    printWriter.println(backupText);
                    printWriter.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            //텍스트 백업

            for (int i = 0; i < updateSentence.size(); i++) {
                updateWsd = updateSentence.get(i).getWsdList();
                arrayWsd = new Document[updateWsd.size()];

                for (int j = 0; j < updateWsd.size(); j++) {
                    if (i == sentenceIndex && j == tagIndex) {
                        arrayWsd[j] = new Document("text", updateWsd.get(j).getText()).append("type", updateWsd.get(j).getType())
                                                                                      .append("id", updateWsd.get(j).getId())
                                                                                      .append("weight", 1.0)
                                                                                      .append("position",
                                                                                              updateWsd.get(j).getPosition())
                                                                                      .append("scode", tempScode)
                                                                                      .append("begin", updateWsd.get(j).getBegin())
                                                                                      .append("end", updateWsd.get(j).getEnd());
                        //업데이트된거 바로(?) 볼 수 있게
                        updateWsd.get(j).setScode(tempScode);

                        //                            String sentence = updateSentence.get(i).getText() + "\n\n";
                        //                            for (int k = 0; k < updateWsd.size(); k++) {
                        //                                sentence += updateWsd.get(k).getText() + "/" + updateWsd.get(k).getType() + " ";
                        //                            }
                        //                            sentenceText.setText(sentence);
                    } else {
                        arrayWsd[j] = new Document("text", updateWsd.get(j).getText()).append("type", updateWsd.get(j).getType())
                                                                                      .append("id", updateWsd.get(j).getId())
                                                                                      .append("weight", updateWsd.get(j).getWeight())
                                                                                      .append("position",
                                                                                              updateWsd.get(j).getPosition())
                                                                                      .append("scode", updateWsd.get(j).getScode())
                                                                                      .append("begin", updateWsd.get(j).getBegin())
                                                                                      .append("end", updateWsd.get(j).getEnd());
                    }
                }
                AnotherUpdate(updateSentence, i);

                arraySentence[i] = new Document("id", updateSentence.get(i).getId())
                                   .append("reserve_str", updateSentence.get(i).getReserve_str())
                                   .append("text", updateSentence.get(i).getText())
                                   .append("morp", Arrays.asList(arrayMorp))
                                   .append("morp_eval", Arrays.asList(arrayMorpEval))
                                   .append("WSD", Arrays.asList(arrayWsd))
                                   .append("word", Arrays.asList(arrayWord))
                                   .append("NE", Arrays.asList(arrayNe))
                                   .append("chunk", Arrays.asList(arrayChunk))
                                   .append("dependency",Arrays.asList(arrayDependency))
                                   .append("phrase_dependency", Arrays.asList(arrayPhraseDependency))
                                   .append("SRL", Arrays.asList(arraySrl))
                                   .append("relation", Arrays.asList(arrayRelation))
                                   .append("SA", Arrays.asList(arraySa))
                                   .append("ZA", Arrays.asList(arrayZa));
            }

            Document documentSentence = new Document("sentence", Arrays.asList(arraySentence));

            BasicDBObject searchUpdateTitle = new BasicDBObject().append("title.text", getEtriFile.getTitle().getText());
            BasicDBObject update = new BasicDBObject("$set", documentSentence);

            etriDB.getetriCollection().updateOne(searchUpdateTitle, update);
        }
    }

    public void NomeanScode(MouseEvent mouseEvent) {
        endTime = System.currentTimeMillis() - startTime - pauseTime;

        Document saveDocument = new Document("name", qaName).append("file", fileTitle)
                                                            .append("sentenceIndex", sentenceIndex).append("morpIndex", tagIndex)
                                                            .append("time", (tempTime + (int) (endTime / 1000)));
        BasicDBObject saveName = new BasicDBObject().append("name", qaName);
        BasicDBObject save = new BasicDBObject("$set", saveDocument);
        etriDB.getEtriSave().updateOne(saveName, save);


        //버튼이 아니라 TableView에 있는 scode를 넣어야됨
        String tempScode = "99";
        scodeText.setText(tempScode);


        if (list_wsd.getSelectionModel().getSelectedIndex() == -1) {
            //팝업창 경고 뛰워주면 좋을듯
            InfoQA.setText("어절을 선택하고 누르세요.");
        } else {
            InfoQA.clear();
            List<Sentence> updateSentence = getEtriFile.getSentenceList();
            List<WSD> updateWsd;

            Document[] arraySentence = new Document[updateSentence.size()];
            Document[] arrayWsd;

            //텍스트 백업
            String backupText = String.valueOf(list_file.getSelectionModel().getSelectedItems()).replace("[", "").replace("]", "")
                                + " " + sentenceIndex + " " + tagIndex + " " + tempScode;
            try {
                String filePath = "C:/Users/Public/Legal_WSD.txt";
                try {
                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                   Charset.forName("UTF-8"));
                    PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                    printWriter.println(backupText);
                    printWriter.close();
                } catch (FileNotFoundException e) {
                    File file = new File(filePath);
                    FileWriter fileWriter = new FileWriter(file, true);
                    fileWriter.close();

                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                   Charset.forName("UTF-8"));
                    PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                    printWriter.println(backupText);
                    printWriter.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            //텍스트 백업

            for (int i = 0; i < updateSentence.size(); i++) {
                updateWsd = updateSentence.get(i).getWsdList();
                arrayWsd = new Document[updateWsd.size()];

                for (int j = 0; j < updateWsd.size(); j++) {
                    if (i == sentenceIndex && j == tagIndex) {
                        arrayWsd[j] = new Document("text", updateWsd.get(j).getText()).append("type", updateWsd.get(j).getType())
                                                                                      .append("id", updateWsd.get(j).getId())
                                                                                      .append("weight", 1.0)
                                                                                      .append("position",
                                                                                              updateWsd.get(j).getPosition())
                                                                                      .append("scode", tempScode)
                                                                                      .append("begin", updateWsd.get(j).getBegin())
                                                                                      .append("end", updateWsd.get(j).getEnd());
                        //업데이트된거 바로(?) 볼 수 있게
                        updateWsd.get(j).setScode(tempScode);

                        //                            String sentence = updateSentence.get(i).getText() + "\n\n";
                        //                            for (int k = 0; k < updateWsd.size(); k++) {
                        //                                sentence += updateWsd.get(k).getText() + "/" + updateWsd.get(k).getType() + " ";
                        //                            }
                        //                            sentenceText.setText(sentence);
                    } else {
                        arrayWsd[j] = new Document("text", updateWsd.get(j).getText()).append("type", updateWsd.get(j).getType())
                                                                                      .append("id", updateWsd.get(j).getId())
                                                                                      .append("weight", updateWsd.get(j).getWeight())
                                                                                      .append("position",
                                                                                              updateWsd.get(j).getPosition())
                                                                                      .append("scode", updateWsd.get(j).getScode())
                                                                                      .append("begin", updateWsd.get(j).getBegin())
                                                                                      .append("end", updateWsd.get(j).getEnd());
                    }
                }
                AnotherUpdate(updateSentence, i);

                arraySentence[i] = new Document("id", updateSentence.get(i).getId())
                                   .append("reserve_str", updateSentence.get(i).getReserve_str())
                                   .append("text", updateSentence.get(i).getText())
                                   .append("morp", Arrays.asList(arrayMorp))
                                   .append("morp_eval", Arrays.asList(arrayMorpEval))
                                   .append("WSD", Arrays.asList(arrayWsd))
                                   .append("word", Arrays.asList(arrayWord))
                                   .append("NE", Arrays.asList(arrayNe))
                                   .append("chunk", Arrays.asList(arrayChunk))
                                   .append("dependency",Arrays.asList(arrayDependency))
                                   .append("phrase_dependency", Arrays.asList(arrayPhraseDependency))
                                   .append("SRL", Arrays.asList(arraySrl))
                                   .append("relation", Arrays.asList(arrayRelation))
                                   .append("SA", Arrays.asList(arraySa))
                                   .append("ZA", Arrays.asList(arrayZa));
            }

            Document documentSentence = new Document("sentence", Arrays.asList(arraySentence));

            BasicDBObject searchUpdateTitle = new BasicDBObject().append("title.text", getEtriFile.getTitle().getText());
            BasicDBObject update = new BasicDBObject("$set", documentSentence);

            etriDB.getetriCollection().updateOne(searchUpdateTitle, update);
        }
    }

    public void ZeroCode(MouseEvent mouseEvent) {
        endTime = System.currentTimeMillis() - startTime - pauseTime;

        Document saveDocument = new Document("name", qaName).append("file", fileTitle)
                                                            .append("sentenceIndex", sentenceIndex).append("morpIndex", tagIndex)
                                                            .append("time", (tempTime + (int) (endTime / 1000)));
        BasicDBObject saveName = new BasicDBObject().append("name", qaName);
        BasicDBObject save = new BasicDBObject("$set", saveDocument);
        etriDB.getEtriSave().updateOne(saveName, save);


        //버튼이 아니라 TableView에 있는 scode를 넣어야됨
        String tempScode = "00";
        scodeText.setText(tempScode);


        if (list_wsd.getSelectionModel().getSelectedIndex() == -1) {
            //팝업창 경고 뛰워주면 좋을듯
            InfoQA.setText("어절을 선택하고 누르세요.");
        } else {
            InfoQA.clear();
            List<Sentence> updateSentence = getEtriFile.getSentenceList();
            List<WSD> updateWsd;

            Document[] arraySentence = new Document[updateSentence.size()];
            Document[] arrayWsd;

            //텍스트 백업
            String backupText = String.valueOf(list_file.getSelectionModel().getSelectedItems()).replace("[", "").replace("]", "")
                                + " " + sentenceIndex + " " + tagIndex + " " + tempScode;
            try {
                String filePath = "C:/Users/Public/Legal_WSD.txt";
                try {
                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                   Charset.forName("UTF-8"));
                    PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                    printWriter.println(backupText);
                    printWriter.close();
                } catch (FileNotFoundException e) {
                    File file = new File(filePath);
                    FileWriter fileWriter = new FileWriter(file, true);
                    fileWriter.close();

                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                   Charset.forName("UTF-8"));
                    PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                    printWriter.println(backupText);
                    printWriter.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            //텍스트 백업

            for (int i = 0; i < updateSentence.size(); i++) {
                updateWsd = updateSentence.get(i).getWsdList();
                arrayWsd = new Document[updateWsd.size()];

                for (int j = 0; j < updateWsd.size(); j++) {
                    if (i == sentenceIndex && j == tagIndex) {
                        arrayWsd[j] = new Document("text", updateWsd.get(j).getText()).append("type", updateWsd.get(j).getType())
                                                                                      .append("id", updateWsd.get(j).getId())
                                                                                      .append("weight", 1.0)
                                                                                      .append("position",
                                                                                              updateWsd.get(j).getPosition())
                                                                                      .append("scode", tempScode)
                                                                                      .append("begin", updateWsd.get(j).getBegin())
                                                                                      .append("end", updateWsd.get(j).getEnd());
                        //업데이트된거 바로(?) 볼 수 있게
                        updateWsd.get(j).setScode(tempScode);

                        //                            String sentence = updateSentence.get(i).getText() + "\n\n";
                        //                            for (int k = 0; k < updateWsd.size(); k++) {
                        //                                sentence += updateWsd.get(k).getText() + "/" + updateWsd.get(k).getType() + " ";
                        //                            }
                        //                            sentenceText.setText(sentence);
                    } else {
                        arrayWsd[j] = new Document("text", updateWsd.get(j).getText()).append("type", updateWsd.get(j).getType())
                                                                                      .append("id", updateWsd.get(j).getId())
                                                                                      .append("weight", updateWsd.get(j).getWeight())
                                                                                      .append("position",
                                                                                              updateWsd.get(j).getPosition())
                                                                                      .append("scode", updateWsd.get(j).getScode())
                                                                                      .append("begin", updateWsd.get(j).getBegin())
                                                                                      .append("end", updateWsd.get(j).getEnd());
                    }
                }
                AnotherUpdate(updateSentence, i);

                arraySentence[i] = new Document("id", updateSentence.get(i).getId())
                                   .append("reserve_str", updateSentence.get(i).getReserve_str())
                                   .append("text", updateSentence.get(i).getText())
                                   .append("morp", Arrays.asList(arrayMorp))
                                   .append("morp_eval", Arrays.asList(arrayMorpEval))
                                   .append("WSD", Arrays.asList(arrayWsd))
                                   .append("word", Arrays.asList(arrayWord))
                                   .append("NE", Arrays.asList(arrayNe))
                                   .append("chunk", Arrays.asList(arrayChunk))
                                   .append("dependency",Arrays.asList(arrayDependency))
                                   .append("phrase_dependency", Arrays.asList(arrayPhraseDependency))
                                   .append("SRL", Arrays.asList(arraySrl))
                                   .append("relation", Arrays.asList(arrayRelation))
                                   .append("SA", Arrays.asList(arraySa))
                                   .append("ZA", Arrays.asList(arrayZa));
            }

            Document documentSentence = new Document("sentence", Arrays.asList(arraySentence));

            BasicDBObject searchUpdateTitle = new BasicDBObject().append("title.text", getEtriFile.getTitle().getText());
            BasicDBObject update = new BasicDBObject("$set", documentSentence);

            etriDB.getetriCollection().updateOne(searchUpdateTitle, update);
        }
    }





    public void korWordPress(KeyEvent keyEvent) {
        if(keyEvent.getCode() == KeyCode.UP){
            if(korTagIndex > 0) {
                korTagIndex--;
                korWordEvent(korTagIndex);
            }
        }
        if(keyEvent.getCode() == KeyCode.DOWN){
            if(korTagIndex+1 < getWsd.size()) {
                korTagIndex++;
                korWordEvent(korTagIndex);
            }
        }
    }

    private void korWordEvent(int index){
        korMeanText.setText(data.get(index).getTableMean());
        korExText.setText(data.get(index).getTableEx());
    }

    public void Question(KeyEvent keyEvent) {
        if(keyEvent.getCode() == KeyCode.ENTER){
            String tempString = InfoQA.getText();
            try {
                etriDB.getEtriQA().insertOne(new Document("name", qaName).append("file", fileTitle)
                                                                         .append("sentenceIndex", sentenceIndex).append("morpIndex", tagIndex)
                                                                         .append("question", tempString));
                InfoQA.clear();
            }catch (NullPointerException e){
                try {
                    etriDB.getEtriQA().insertOne(new Document("name", qaName).append("question", tempString));
                    InfoQA.clear();
                }catch (NullPointerException e2){
                    InfoQA.setText("디비에 접속한 후 질문해주세요.");
                }
            }
        }
    }

    //작업시간
    @FXML Button pauseButton;
    @FXML Text workTime;
    long pauseTimeStart;
    long pauseTimeEnd;
    long pauseTime = 0L;
    boolean pauseBool = false;
    private void TimeUpdate(){
        endTime = System.currentTimeMillis() - startTime - pauseTime;
        workTime.setText(String.valueOf(tempTime + (int)(endTime/1000)));

        Document saveDocument = new Document("name", qaName).append("file", fileTitle)
                                                            .append("sentenceIndex", sentenceIndex).append("morpIndex", tagIndex)
                                                            .append("time", (tempTime + (int)(endTime/1000)));
        BasicDBObject saveName = new BasicDBObject().append("name", qaName);
        BasicDBObject save = new BasicDBObject("$set", saveDocument);
        etriDB.getEtriSave().updateOne(saveName, save);
    }
    public void WorkPause(MouseEvent mouseEvent) {
        if(pauseBool == false){
            pauseTimeStart = System.currentTimeMillis();
            pauseBool = true;
            pauseButton.setText("작업재개");
        }else if(pauseBool == true){
            pauseTimeEnd = System.currentTimeMillis();
            pauseTime = pauseTime + pauseTimeEnd - pauseTimeStart;
            pauseBool = false;
            pauseButton.setText("일시중지");
        }
    }

    public void WorkExit(MouseEvent mouseEvent) {
        endTime = System.currentTimeMillis() - startTime - pauseTime;
        workTime.setText(String.valueOf(tempTime + (int)(endTime/1000)));

        Document saveDocument = new Document("name", qaName).append("file", fileTitle)
                                                            .append("sentenceIndex", sentenceIndex).append("morpIndex", tagIndex)
                                                            .append("time", (tempTime + (int)(endTime/1000)));
        BasicDBObject saveName = new BasicDBObject().append("name", qaName);
        BasicDBObject save = new BasicDBObject("$set", saveDocument);
        etriDB.getEtriSave().updateOne(saveName, save);
    }


    //형태소 오분석 텍스트 저장
    public void WrongMorpCheck(MouseEvent mouseEvent) {
        String backupText = String.valueOf(list_file.getSelectionModel().getSelectedItems()).replace("[", "").replace("]", "")
                            + " " + sentenceIndex + " " + tagIndex;
        try {
            String filePath = "C:/Users/Public/EtriBackup_WSD_wrongMorp.txt";
            try {
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                               Charset.forName("UTF-8"));
                PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                printWriter.println(backupText);
                printWriter.close();
            } catch (FileNotFoundException e) {
                File file = new File(filePath);
                FileWriter fileWriter = new FileWriter(file, true);
                fileWriter.close();

                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                               Charset.forName("UTF-8"));
                PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                printWriter.println(backupText);
                printWriter.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @FXML Button removeButton;
    @FXML Button currentButton;
    @FXML Button nextButton;
    public void RemoveWord(MouseEvent mouseEvent) {
        //텍스트 백업
        String backupText = String.valueOf(list_file.getSelectionModel().getSelectedItems()).replace("[", "").replace("]", "")
                            + " " + sentenceIndex + " " + tagIndex + " " + "remove";
        try {
            String filePath = "C:/Users/Public/Legal_WSD.txt";
            try {
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                               Charset.forName("UTF-8"));
                PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                printWriter.println(backupText);
                printWriter.close();
            } catch (FileNotFoundException e) {
                File file = new File(filePath);
                FileWriter fileWriter = new FileWriter(file, true);
                fileWriter.close();

                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                               Charset.forName("UTF-8"));
                PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                printWriter.println(backupText);
                printWriter.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        //텍스트 백업

        List<Sentence> updateSentence = getEtriFile.getSentenceList();
        List<WSD> updateWsd;
        Document[] arraySentence = new Document[updateSentence.size()];
        Document[] arrayWsd;

        for (int i = 0; i < updateSentence.size(); i++) {
            updateWsd = updateSentence.get(i).getWsdList();         //자바용 배열
            arrayWsd = new Document[updateWsd.size()];        //업데이트용 배열

            int arrayNeSize = arrayWsd.length;
            boolean removeCheck = false;
            for (int j = 0; j < updateWsd.size(); j++) {    //자바내에서 업데이트
                if(removeCheck == false) {
                    if (i == sentenceIndex && j == tagIndex) {
                        if(j != updateWsd.size()-1) {
                            updateWsd.get(j).setText(updateWsd.get(j + 1).getText());
                            updateWsd.get(j).setType(updateWsd.get(j + 1).getType());
                            updateWsd.get(j).setWeight(updateWsd.get(j + 1).getWeight());
                            updateWsd.get(j).setId(updateWsd.get(j + 1).getId() - 1);
                            updateWsd.get(j).setPosition(updateWsd.get(j + 1).getPosition());
                            updateWsd.get(j).setScode(updateWsd.get(j + 1).getScode());
                            updateWsd.get(j).setBegin(updateWsd.get(j + 1).getBegin());
                            updateWsd.get(j).setEnd(updateWsd.get(j + 1).getEnd());
                        }
                        removeCheck = true;
                    } else {
                        updateWsd.get(j).setText(updateWsd.get(j).getText());
                        updateWsd.get(j).setType(updateWsd.get(j).getType());
                        updateWsd.get(j).setWeight(updateWsd.get(j).getWeight());
                        updateWsd.get(j).setId(updateWsd.get(j).getId());
                        updateWsd.get(j).setPosition(updateWsd.get(j).getPosition());
                        updateWsd.get(j).setScode(updateWsd.get(j).getScode());
                        updateWsd.get(j).setBegin(updateWsd.get(j).getBegin());
                        updateWsd.get(j).setEnd(updateWsd.get(j).getEnd());
                    }
                }else{
                    if(j+1 != arrayNeSize) {
                        updateWsd.get(j).setText(updateWsd.get(j + 1).getText());
                        updateWsd.get(j).setType(updateWsd.get(j + 1).getType());
                        updateWsd.get(j).setWeight(updateWsd.get(j + 1).getWeight());
                        updateWsd.get(j).setId(updateWsd.get(j + 1).getId() - 1);
                        updateWsd.get(j).setPosition(updateWsd.get(j + 1).getPosition());
                        updateWsd.get(j).setScode(updateWsd.get(j + 1).getScode());
                        updateWsd.get(j).setBegin(updateWsd.get(j + 1).getBegin());
                        updateWsd.get(j).setEnd(updateWsd.get(j + 1).getEnd());
                    }
                }
            }
            if(removeCheck == true){
                updateWsd.remove(updateWsd.size()-1);
            }
            //배열 크기 재정의
            Document[] tempArray = new Document[updateWsd.size()];
            for(int k=0; k<updateWsd.size(); k++){
                tempArray[k] = arrayWsd[k];
            }
            arrayWsd = tempArray;

            for(int k=0; k<updateWsd.size(); k++){
                arrayWsd[k] = new Document("text", updateWsd.get(k).getText())
                             .append("type", updateWsd.get(k).getType())
                             .append("id", updateWsd.get(k).getId())
                             .append("weight", updateWsd.get(k).getWeight())
                             .append("position", updateWsd.get(k).getPosition())
                             .append("scode", updateWsd.get(k).getScode())
                             .append("begin", updateWsd.get(k).getBegin())
                             .append("end", updateWsd.get(k).getEnd());
            }
            AnotherUpdate(updateSentence, i);

            arraySentence[i] = new Document("id", updateSentence.get(i).getId())
                               .append("reserve_str", updateSentence.get(i).getReserve_str())
                               .append("text", updateSentence.get(i).getText())
                               .append("morp", Arrays.asList(arrayMorp))
                               .append("morp_eval", Arrays.asList(arrayMorpEval))
                               .append("WSD", Arrays.asList(arrayWsd))
                               .append("word", Arrays.asList(arrayWord))
                               .append("NE", Arrays.asList(arrayNe))
                               .append("chunk", Arrays.asList(arrayChunk))
                               .append("dependency",Arrays.asList(arrayDependency))
                               .append("phrase_dependency", Arrays.asList(arrayPhraseDependency))
                               .append("SRL", Arrays.asList(arraySrl))
                               .append("relation", Arrays.asList(arrayRelation))
                               .append("SA", Arrays.asList(arraySa))
                               .append("ZA", Arrays.asList(arrayZa));
        }

        Document documentSentence = new Document("sentence", Arrays.asList(arraySentence));

        BasicDBObject searchUpdateTitle = new BasicDBObject().append("title.text", getEtriFile.getTitle().getText());
        BasicDBObject update = new BasicDBObject("$set", documentSentence);

        etriDB.getetriCollection().updateOne(searchUpdateTitle, update);


        TimeUpdate();
        sentenceEvent(sentenceIndex);
    }

    public void CurrentWord(MouseEvent mouseEvent) {
        List<Sentence> updateSentence = getEtriFile.getSentenceList();
        List<WSD> updateWsd;

        Document[] arraySentence = new Document[updateSentence.size()];
        Document[] arrayWsd;

        for (int i = 0; i < updateSentence.size(); i++) {
            updateWsd = updateSentence.get(i).getWsdList();
            arrayWsd = new Document[updateWsd.size()];

            int arrayWsdSize = arrayWsd.length;
            int dbIndexCount = 0;

            for (int j = 0; j < updateWsd.size(); j++) {
                if (i == sentenceIndex && j == tagIndex) {
                    //텍스트 백업
                    String backupText = String.valueOf(list_file.getSelectionModel().getSelectedItems()).replace("[", "").replace("]", "")
                                        + " " + sentenceIndex + " " + tagIndex + " CURRENT";
                    try {
                        String filePath = "C:/Users/Public/Legal_WSD.txt";
                        try {
                            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                           Charset.forName("UTF-8"));
                            PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                            printWriter.println(backupText);
                            printWriter.close();
                        } catch (FileNotFoundException e) {
                            File file = new File(filePath);
                            FileWriter fileWriter = new FileWriter(file, true);
                            fileWriter.close();

                            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                           Charset.forName("UTF-8"));
                            PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                            printWriter.println(backupText);
                            printWriter.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    //텍스트 백업

                    //배열 크기 재정의
                    Document[] tempArray = new Document[arrayWsdSize + 1];
                    for(int k=0; k<arrayWsdSize; k++){
                        tempArray[k] = arrayWsd[k];
                    }
                    arrayWsd = tempArray;
                    arrayWsdSize++;

                    boolean checkUpdateWsd = false;
                    int tempUpdateWsdSize = updateWsd.size();
                    WSD tempWSD = new WSD(new Document("text", updateWsd.get(tempUpdateWsdSize-1).getText())
                                          .append("type", updateWsd.get(tempUpdateWsdSize-1).getType())
                                          .append("id", updateWsd.get(tempUpdateWsdSize-1).getId())
                                          .append("weight", updateWsd.get(tempUpdateWsdSize-1).getWeight())
                                          .append("position",updateWsd.get(tempUpdateWsdSize-1).getPosition())
                                          .append("scode", updateWsd.get(tempUpdateWsdSize-1).getScode())
                                          .append("begin", updateWsd.get(tempUpdateWsdSize-1).getBegin())
                                          .append("end", updateWsd.get(tempUpdateWsdSize-1).getEnd()));
                    updateWsd.add(updateWsd.size(), tempWSD);
                    for(int k=updateWsd.size()-1; k>=0; k--){
                        if(checkUpdateWsd == false){
                            if(k == j){
                                updateWsd.get(k).setText("");
                                updateWsd.get(k).setType("");
                                updateWsd.get(k).setId(j);
                                updateWsd.get(k).setWeight(1.0);
                                updateWsd.get(k).setPosition(0);
                                updateWsd.get(k).setScode("00");
                                updateWsd.get(k).setBegin(0);
                                updateWsd.get(k).setEnd(0);
                                checkUpdateWsd = true;
                            }else{
                                updateWsd.get(k).setText(updateWsd.get(k-1).getText());
                                updateWsd.get(k).setType(updateWsd.get(k-1).getType());
                                updateWsd.get(k).setId(updateWsd.get(k-1).getId() + 1);
                                updateWsd.get(k).setWeight(updateWsd.get(k-1).getWeight());
                                updateWsd.get(k).setPosition(updateWsd.get(k-1).getPosition());
                                updateWsd.get(k).setScode(updateWsd.get(k-1).getScode());
                                updateWsd.get(k).setBegin(updateWsd.get(k-1).getBegin());
                                updateWsd.get(k).setEnd(updateWsd.get(k-1).getEnd());
                            }
                        }else{

                        }
                    }

                    for(int k=0; k<updateWsd.size(); k++){
                        arrayWsd[k] = new Document("text", updateWsd.get(k).getText())
                        .append("type", updateWsd.get(k).getType())
                        .append("id", updateWsd.get(k).getId())
                        .append("weight", updateWsd.get(k).getWeight())
                        .append("position", updateWsd.get(k).getPosition())
                        .append("scode", updateWsd.get(k).getScode())
                        .append("begin", updateWsd.get(k).getBegin())
                        .append("end", updateWsd.get(k).getEnd());
                    }
                    j++;
                } else {
                    arrayWsd[j] = new Document("text", updateWsd.get(dbIndexCount).getText())
                                  .append("type", updateWsd.get(dbIndexCount).getType())
                                  .append("id", updateWsd.get(dbIndexCount).getId())
                                  .append("weight", updateWsd.get(dbIndexCount).getWeight())
                                  .append("position",updateWsd.get(dbIndexCount).getPosition())
                                  .append("scode", updateWsd.get(dbIndexCount).getScode())
                                  .append("begin", updateWsd.get(dbIndexCount).getBegin())
                                  .append("end", updateWsd.get(dbIndexCount).getEnd());
                }
                dbIndexCount++;
            }
            AnotherUpdate(updateSentence, i);

            arraySentence[i] = new Document("id", updateSentence.get(i).getId())
                                .append("reserve_str", updateSentence.get(i).getReserve_str())
                                .append("text", updateSentence.get(i).getText())
                                .append("morp", Arrays.asList(arrayMorp))
                                .append("morp_eval", Arrays.asList(arrayMorpEval))
                                .append("WSD", Arrays.asList(arrayWsd))
                                .append("word", Arrays.asList(arrayWord))
                                .append("NE", Arrays.asList(arrayNe))
                                .append("chunk", Arrays.asList(arrayChunk))
                                .append("dependency",Arrays.asList(arrayDependency))
                                .append("phrase_dependency", Arrays.asList(arrayPhraseDependency))
                                .append("SRL", Arrays.asList(arraySrl))
                                .append("relation", Arrays.asList(arrayRelation))
                                .append("SA", Arrays.asList(arraySa))
                                .append("ZA", Arrays.asList(arrayZa));
        }

        Document documentSentence = new Document("sentence", Arrays.asList(arraySentence));

        BasicDBObject searchUpdateTitle = new BasicDBObject().append("title.text", getEtriFile.getTitle().getText());
        BasicDBObject update = new BasicDBObject("$set", documentSentence);

        etriDB.getetriCollection().updateOne(searchUpdateTitle, update);

        TimeUpdate();
        sentenceEvent(sentenceIndex);
    }

    public void NextWord(MouseEvent mouseEvent) {
        List<Sentence> updateSentence = getEtriFile.getSentenceList();
        List<WSD> updateWsd;

        Document[] arraySentence = new Document[updateSentence.size()];
        Document[] arrayWsd;

        for (int i = 0; i < updateSentence.size(); i++) {
            updateWsd = updateSentence.get(i).getWsdList();
            arrayWsd = new Document[updateWsd.size()];

            int arrayWsdSize = arrayWsd.length;
            int dbIndexCount = 0;

            for (int j = 0; j < updateWsd.size(); j++) {
                if (i == sentenceIndex && j == tagIndex) {
                    //텍스트 백업
                    String backupText = String.valueOf(list_file.getSelectionModel().getSelectedItems()).replace("[", "").replace("]", "")
                                        + " " + sentenceIndex + " " + tagIndex + " NEXT";
                    try {
                        String filePath = "C:/Users/Public/Legal_WSD.txt";
                        try {
                            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                           Charset.forName("UTF-8"));
                            PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                            printWriter.println(backupText);
                            printWriter.close();
                        } catch (FileNotFoundException e) {
                            File file = new File(filePath);
                            FileWriter fileWriter = new FileWriter(file, true);
                            fileWriter.close();

                            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                           Charset.forName("UTF-8"));
                            PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                            printWriter.println(backupText);
                            printWriter.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    //텍스트 백업

//                    배열 크기 재정의
                    Document[] tempArray = new Document[arrayWsdSize + 1];
                    for(int k=0; k<arrayWsdSize; k++){
                        tempArray[k] = arrayWsd[k];
                    }
                    arrayWsd = tempArray;
                    arrayWsdSize++;

                    boolean checkUpdateWsd = false;
                    int tempUpdateWsdSize = updateWsd.size();
                    WSD tempWSD = new WSD(new Document("text", updateWsd.get(tempUpdateWsdSize-1).getText())
                                          .append("type", updateWsd.get(tempUpdateWsdSize-1).getType())
                                          .append("id", updateWsd.get(tempUpdateWsdSize-1).getId())
                                          .append("weight", updateWsd.get(tempUpdateWsdSize-1).getWeight())
                                          .append("position",updateWsd.get(tempUpdateWsdSize-1).getPosition())
                                          .append("scode", updateWsd.get(tempUpdateWsdSize-1).getScode())
                                          .append("begin", updateWsd.get(tempUpdateWsdSize-1).getBegin())
                                          .append("end", updateWsd.get(tempUpdateWsdSize-1).getEnd()));
                    updateWsd.add(updateWsd.size(), tempWSD);
                    for(int k=updateWsd.size()-1; k>=0; k--){
                        if(checkUpdateWsd == false){
                            if(k == j + 1){
                                updateWsd.get(k).setText("");
                                updateWsd.get(k).setType("");
                                updateWsd.get(k).setId(j + 1);
                                updateWsd.get(k).setWeight(1.0);
                                updateWsd.get(k).setPosition(-1);
                                updateWsd.get(k).setScode("");
                                updateWsd.get(k).setBegin(0);
                                updateWsd.get(k).setEnd(0);
                                checkUpdateWsd = true;
                            }else{
                                updateWsd.get(k).setText(updateWsd.get(k-1).getText());
                                updateWsd.get(k).setType(updateWsd.get(k-1).getType());
                                updateWsd.get(k).setId(updateWsd.get(k-1).getId() + 1);
                                updateWsd.get(k).setWeight(updateWsd.get(k-1).getWeight());
                                updateWsd.get(k).setPosition(updateWsd.get(k-1).getPosition());
                                updateWsd.get(k).setScode(updateWsd.get(k-1).getScode());
                                updateWsd.get(k).setBegin(updateWsd.get(k-1).getBegin());
                                updateWsd.get(k).setEnd(updateWsd.get(k-1).getEnd());
                            }
                        }else{

                        }
                    }

                    for(int k=0; k<updateWsd.size(); k++){
                        arrayWsd[k] = new Document("text", updateWsd.get(k).getText())
                                      .append("type", updateWsd.get(k).getType())
                                      .append("id", updateWsd.get(k).getId())
                                      .append("weight", updateWsd.get(k).getWeight())
                                      .append("position", updateWsd.get(k).getPosition())
                                      .append("scode", updateWsd.get(k).getScode())
                                      .append("begin", updateWsd.get(k).getBegin())
                                      .append("end", updateWsd.get(k).getEnd());
                    }
                    j++;
                } else {
                    arrayWsd[j] = new Document("text", updateWsd.get(dbIndexCount).getText())
                                  .append("type", updateWsd.get(dbIndexCount).getType())
                                  .append("id", updateWsd.get(dbIndexCount).getId())
                                  .append("weight", updateWsd.get(dbIndexCount).getWeight())
                                  .append("position",updateWsd.get(dbIndexCount).getPosition())
                                  .append("scode", updateWsd.get(dbIndexCount).getScode())
                                  .append("begin", updateWsd.get(dbIndexCount).getBegin())
                                  .append("end", updateWsd.get(dbIndexCount).getEnd());
                }
                dbIndexCount++;
            }
            AnotherUpdate(updateSentence, i);

            arraySentence[i] = new Document("id", updateSentence.get(i).getId())
                               .append("reserve_str", updateSentence.get(i).getReserve_str())
                               .append("text", updateSentence.get(i).getText())
                               .append("morp", Arrays.asList(arrayMorp))
                               .append("morp_eval", Arrays.asList(arrayMorpEval))
                               .append("WSD", Arrays.asList(arrayWsd))
                               .append("word", Arrays.asList(arrayWord))
                               .append("NE", Arrays.asList(arrayNe))
                               .append("chunk", Arrays.asList(arrayChunk))
                               .append("dependency",Arrays.asList(arrayDependency))
                               .append("phrase_dependency", Arrays.asList(arrayPhraseDependency))
                               .append("SRL", Arrays.asList(arraySrl))
                               .append("relation", Arrays.asList(arrayRelation))
                               .append("SA", Arrays.asList(arraySa))
                               .append("ZA", Arrays.asList(arrayZa));
        }

        Document documentSentence = new Document("sentence", Arrays.asList(arraySentence));

        BasicDBObject searchUpdateTitle = new BasicDBObject().append("title.text", getEtriFile.getTitle().getText());
        BasicDBObject update = new BasicDBObject("$set", documentSentence);

        etriDB.getetriCollection().updateOne(searchUpdateTitle, update);

        TimeUpdate();
        sentenceEvent(sentenceIndex);
    }



    public void editLemma(KeyEvent keyEvent) {
        if(keyEvent.getCode() == KeyCode.ENTER) {
            endTime = System.currentTimeMillis() - startTime - pauseTime;
            workTime.setText(String.valueOf(tempTime + (int) (endTime / 1000)));

            Document saveDocument = new Document("name", qaName).append("file", fileTitle)
                                                                .append("sentenceIndex", sentenceIndex).append("morpIndex", tagIndex)
                                                                .append("time", (tempTime + (int) (endTime / 1000)));
            BasicDBObject saveName = new BasicDBObject().append("name", qaName);
            BasicDBObject save = new BasicDBObject("$set", saveDocument);
            etriDB.getEtriSave().updateOne(saveName, save);

            String tempLemma = lemmaText.getText();
            lemmaText.setText(String.valueOf(tempLemma));


            if (list_wsd.getSelectionModel().getSelectedIndex() == -1) {
                //팝업창 경고 뛰워주면 좋을듯
                InfoQA.setText("어절을 선택하고 누르세요.");
            } else {
                InfoQA.clear();
                List<Sentence> updateSentence = getEtriFile.getSentenceList();
                List<WSD> updateWSD;
                Document[] arraySentence = new Document[updateSentence.size()];
                Document[] arrayWSD;

                //텍스트 백업
                String backupText = String.valueOf(list_file.getSelectionModel().getSelectedItems()).replace("[", "").replace("]", "")
                                    + " " + sentenceIndex + " " + tagIndex + " lemma " + tempLemma;
                try {
                    String filePath = "C:/Users/Public/Legal_WSD.txt";
                    try {
                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                       Charset.forName("UTF-8"));
                        PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                        printWriter.println(backupText);
                        printWriter.close();
                    } catch (FileNotFoundException e) {
                        File file = new File(filePath);
                        FileWriter fileWriter = new FileWriter(file, true);
                        fileWriter.close();

                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                       Charset.forName("UTF-8"));
                        PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                        printWriter.println(backupText);
                        printWriter.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //텍스트 백업


                for (int i = 0; i < updateSentence.size(); i++) {
                    updateWSD = updateSentence.get(i).getWsdList();
                    arrayWSD = new Document[updateWSD.size()];

                    for (int j = 0; j < updateWSD.size(); j++) {
                        if (i == sentenceIndex && j == tagIndex) {
                            arrayWSD[j] = new Document("id", updateWSD.get(j).getId())
                                          .append("text", tempLemma)
                                          .append("type", updateWSD.get(j).getType())
                                          .append("scode", updateWSD.get(j).getScode())
                                          .append("begin", updateWSD.get(j).getBegin())
                                          .append("end", updateWSD.get(j).getEnd())
                                          .append("weight", 1)
                                          .append("position", updateWSD.get(j).getPosition());
                            //업데이트된거 바로(?) 볼 수 있게
                            updateWSD.get(j).setText(tempLemma);
                            updateWSD.get(j).setWeight(1);
                        } else {
                            arrayWSD[j] = new Document("id", updateWSD.get(j).getId())
                                         .append("text", updateWSD.get(j).getText())
                                         .append("type", updateWSD.get(j).getType())
                                         .append("scode", updateWSD.get(j).getScode())
                                         .append("begin", updateWSD.get(j).getBegin())
                                         .append("end", updateWSD.get(j).getEnd())
                                         .append("weight", updateWSD.get(j).getWeight())
                                         .append("position", updateWSD.get(j).getPosition());
                        }
                    }
                    AnotherUpdate(updateSentence, i);

                    arraySentence[i] = new Document("id", updateSentence.get(i).getId())
                                       .append("reserve_str", updateSentence.get(i).getReserve_str())
                                       .append("text", updateSentence.get(i).getText())
                                       .append("morp", Arrays.asList(arrayMorp))
                                       .append("morp_eval", Arrays.asList(arrayMorpEval))
                                       .append("WSD", Arrays.asList(arrayWSD))
                                       .append("word", Arrays.asList(arrayWord))
                                       .append("NE", Arrays.asList(arrayNe))
                                       .append("chunk", Arrays.asList(arrayChunk))
                                       .append("dependency",Arrays.asList(arrayDependency))
                                       .append("phrase_dependency", Arrays.asList(arrayPhraseDependency))
                                       .append("SRL", Arrays.asList(arraySrl))
                                       .append("relation", Arrays.asList(arrayRelation))
                                       .append("SA", Arrays.asList(arraySa))
                                       .append("ZA", Arrays.asList(arrayZa));
                }

                Document documentSentence = new Document("sentence", Arrays.asList(arraySentence));

                BasicDBObject searchUpdateTitle = new BasicDBObject().append("title.text", getEtriFile.getTitle().getText());
                BasicDBObject update = new BasicDBObject("$set", documentSentence);

                etriDB.getetriCollection().updateOne(searchUpdateTitle, update);
            }

            //            positionText.clear();
            TimeUpdate();
            sentenceEvent(sentenceIndex);
            wsdEvent(tagIndex);
        }
    }

    public void editType(KeyEvent keyEvent) {
        if(keyEvent.getCode() == KeyCode.ENTER) {
            endTime = System.currentTimeMillis() - startTime - pauseTime;
            workTime.setText(String.valueOf(tempTime + (int) (endTime / 1000)));

            Document saveDocument = new Document("name", qaName).append("file", fileTitle)
                                                                .append("sentenceIndex", sentenceIndex).append("morpIndex", tagIndex)
                                                                .append("time", (tempTime + (int) (endTime / 1000)));
            BasicDBObject saveName = new BasicDBObject().append("name", qaName);
            BasicDBObject save = new BasicDBObject("$set", saveDocument);
            etriDB.getEtriSave().updateOne(saveName, save);

            String tempType = typeText.getText();
            typeText.setText(String.valueOf(tempType));


            if (list_wsd.getSelectionModel().getSelectedIndex() == -1) {
                //팝업창 경고 뛰워주면 좋을듯
                InfoQA.setText("어절을 선택하고 누르세요.");
            } else {
                InfoQA.clear();
                List<Sentence> updateSentence = getEtriFile.getSentenceList();
                List<WSD> updateWSD;
                Document[] arraySentence = new Document[updateSentence.size()];
                Document[] arrayWSD;

                //텍스트 백업
                String backupText = String.valueOf(list_file.getSelectionModel().getSelectedItems()).replace("[", "").replace("]", "")
                                    + " " + sentenceIndex + " " + tagIndex + " type " + tempType;
                try {
                    String filePath = "C:/Users/Public/Legal_WSD.txt";
                    try {
                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                       Charset.forName("UTF-8"));
                        PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                        printWriter.println(backupText);
                        printWriter.close();
                    } catch (FileNotFoundException e) {
                        File file = new File(filePath);
                        FileWriter fileWriter = new FileWriter(file, true);
                        fileWriter.close();

                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                       Charset.forName("UTF-8"));
                        PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                        printWriter.println(backupText);
                        printWriter.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //텍스트 백업


                for (int i = 0; i < updateSentence.size(); i++) {
                    updateWSD = updateSentence.get(i).getWsdList();
                    arrayWSD = new Document[updateWSD.size()];

                    for (int j = 0; j < updateWSD.size(); j++) {
                        if (i == sentenceIndex && j == tagIndex) {
                            arrayWSD[j] = new Document("id", updateWSD.get(j).getId())
                                          .append("text", updateWSD.get(j).getText())
                                          .append("type", tempType)
                                          .append("scode", updateWSD.get(j).getScode())
                                          .append("begin", updateWSD.get(j).getBegin())
                                          .append("end", updateWSD.get(j).getEnd())
                                          .append("weight", 1)
                                          .append("position", updateWSD.get(j).getPosition());
                            //업데이트된거 바로(?) 볼 수 있게
                            updateWSD.get(j).setType(tempType);
                            updateWSD.get(j).setWeight(1);
                        } else {
                            arrayWSD[j] = new Document("id", updateWSD.get(j).getId())
                                          .append("text", updateWSD.get(j).getText())
                                          .append("type", updateWSD.get(j).getType())
                                          .append("scode", updateWSD.get(j).getScode())
                                          .append("begin", updateWSD.get(j).getBegin())
                                          .append("end", updateWSD.get(j).getEnd())
                                          .append("weight", updateWSD.get(j).getWeight())
                                          .append("position", updateWSD.get(j).getPosition());
                        }
                    }
                    AnotherUpdate(updateSentence, i);

                    arraySentence[i] = new Document("id", updateSentence.get(i).getId())
                                       .append("reserve_str", updateSentence.get(i).getReserve_str())
                                       .append("text", updateSentence.get(i).getText())
                                       .append("morp", Arrays.asList(arrayMorp))
                                       .append("morp_eval", Arrays.asList(arrayMorpEval))
                                       .append("WSD", Arrays.asList(arrayWSD))
                                       .append("word", Arrays.asList(arrayWord))
                                       .append("NE", Arrays.asList(arrayNe))
                                       .append("chunk", Arrays.asList(arrayChunk))
                                       .append("dependency",Arrays.asList(arrayDependency))
                                       .append("phrase_dependency", Arrays.asList(arrayPhraseDependency))
                                       .append("SRL", Arrays.asList(arraySrl))
                                       .append("relation", Arrays.asList(arrayRelation))
                                       .append("SA", Arrays.asList(arraySa))
                                       .append("ZA", Arrays.asList(arrayZa));
                }

                Document documentSentence = new Document("sentence", Arrays.asList(arraySentence));

                BasicDBObject searchUpdateTitle = new BasicDBObject().append("title.text", getEtriFile.getTitle().getText());
                BasicDBObject update = new BasicDBObject("$set", documentSentence);

                etriDB.getetriCollection().updateOne(searchUpdateTitle, update);
            }

            //            positionText.clear();
            TimeUpdate();
            sentenceEvent(sentenceIndex);
            wsdEvent(tagIndex);
        }
    }

    public void editPosition(KeyEvent keyEvent) {
        if(keyEvent.getCode() == KeyCode.ENTER) {
            endTime = System.currentTimeMillis() - startTime - pauseTime;
            workTime.setText(String.valueOf(tempTime + (int) (endTime / 1000)));

            Document saveDocument = new Document("name", qaName).append("file", fileTitle)
                                                                .append("sentenceIndex", sentenceIndex).append("morpIndex", tagIndex)
                                                                .append("time", (tempTime + (int) (endTime / 1000)));
            BasicDBObject saveName = new BasicDBObject().append("name", qaName);
            BasicDBObject save = new BasicDBObject("$set", saveDocument);
            etriDB.getEtriSave().updateOne(saveName, save);

            String tempPosition = positionText.getText();
            positionText.setText(String.valueOf(tempPosition));


            if (list_wsd.getSelectionModel().getSelectedIndex() == -1) {
                //팝업창 경고 뛰워주면 좋을듯
                InfoQA.setText("어절을 선택하고 누르세요.");
            } else {
                InfoQA.clear();
                List<Sentence> updateSentence = getEtriFile.getSentenceList();
                List<WSD> updateWSD;
                Document[] arraySentence = new Document[updateSentence.size()];
                Document[] arrayWSD;

                //텍스트 백업
                String backupText = String.valueOf(list_file.getSelectionModel().getSelectedItems()).replace("[", "").replace("]", "")
                                    + " " + sentenceIndex + " " + tagIndex + " position " + tempPosition;
                try {
                    String filePath = "C:/Users/Public/Legal_WSD.txt";
                    try {
                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                       Charset.forName("UTF-8"));
                        PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                        printWriter.println(backupText);
                        printWriter.close();
                    } catch (FileNotFoundException e) {
                        File file = new File(filePath);
                        FileWriter fileWriter = new FileWriter(file, true);
                        fileWriter.close();

                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                       Charset.forName("UTF-8"));
                        PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                        printWriter.println(backupText);
                        printWriter.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //텍스트 백업


                for (int i = 0; i < updateSentence.size(); i++) {
                    updateWSD = updateSentence.get(i).getWsdList();
                    arrayWSD = new Document[updateWSD.size()];

                    for (int j = 0; j < updateWSD.size(); j++) {
                        if (i == sentenceIndex && j == tagIndex) {
                            arrayWSD[j] = new Document("id", updateWSD.get(j).getId())
                                          .append("text", updateWSD.get(j).getText())
                                          .append("type", updateWSD.get(j).getType())
                                          .append("scode", updateWSD.get(j).getScode())
                                          .append("begin", updateWSD.get(j).getBegin())
                                          .append("end", updateWSD.get(j).getEnd())
                                          .append("weight", 1)
                                          .append("position", Integer.parseInt(tempPosition));
                            //업데이트된거 바로(?) 볼 수 있게
                            updateWSD.get(j).setPosition(Integer.parseInt(tempPosition));
                            updateWSD.get(j).setWeight(1);
                        } else {
                            arrayWSD[j] = new Document("id", updateWSD.get(j).getId())
                                          .append("text", updateWSD.get(j).getText())
                                          .append("type", updateWSD.get(j).getType())
                                          .append("scode", updateWSD.get(j).getScode())
                                          .append("begin", updateWSD.get(j).getBegin())
                                          .append("end", updateWSD.get(j).getEnd())
                                          .append("weight", updateWSD.get(j).getWeight())
                                          .append("position", updateWSD.get(j).getPosition());
                        }
                    }
                    AnotherUpdate(updateSentence, i);

                    arraySentence[i] = new Document("id", updateSentence.get(i).getId())
                                       .append("reserve_str", updateSentence.get(i).getReserve_str())
                                       .append("text", updateSentence.get(i).getText())
                                       .append("morp", Arrays.asList(arrayMorp))
                                       .append("morp_eval", Arrays.asList(arrayMorpEval))
                                       .append("WSD", Arrays.asList(arrayWSD))
                                       .append("word", Arrays.asList(arrayWord))
                                       .append("NE", Arrays.asList(arrayNe))
                                       .append("chunk", Arrays.asList(arrayChunk))
                                       .append("dependency",Arrays.asList(arrayDependency))
                                       .append("phrase_dependency", Arrays.asList(arrayPhraseDependency))
                                       .append("SRL", Arrays.asList(arraySrl))
                                       .append("relation", Arrays.asList(arrayRelation))
                                       .append("SA", Arrays.asList(arraySa))
                                       .append("ZA", Arrays.asList(arrayZa));
                }

                Document documentSentence = new Document("sentence", Arrays.asList(arraySentence));

                BasicDBObject searchUpdateTitle = new BasicDBObject().append("title.text", getEtriFile.getTitle().getText());
                BasicDBObject update = new BasicDBObject("$set", documentSentence);

                etriDB.getetriCollection().updateOne(searchUpdateTitle, update);
            }

            //            positionText.clear();
            TimeUpdate();
            sentenceEvent(sentenceIndex);
            wsdEvent(tagIndex);
        }
    }

    public void editBegin(KeyEvent keyEvent) {
        if(keyEvent.getCode() == KeyCode.ENTER) {
            endTime = System.currentTimeMillis() - startTime - pauseTime;
            workTime.setText(String.valueOf(tempTime + (int) (endTime / 1000)));

            Document saveDocument = new Document("name", qaName).append("file", fileTitle)
                                                                .append("sentenceIndex", sentenceIndex).append("morpIndex", tagIndex)
                                                                .append("time", (tempTime + (int) (endTime / 1000)));
            BasicDBObject saveName = new BasicDBObject().append("name", qaName);
            BasicDBObject save = new BasicDBObject("$set", saveDocument);
            etriDB.getEtriSave().updateOne(saveName, save);

            String tempBegin = beginText.getText();
            beginText.setText(String.valueOf(tempBegin));


            if (list_wsd.getSelectionModel().getSelectedIndex() == -1) {
                //팝업창 경고 뛰워주면 좋을듯
                InfoQA.setText("어절을 선택하고 누르세요.");
            } else {
                InfoQA.clear();
                List<Sentence> updateSentence = getEtriFile.getSentenceList();
                List<WSD> updateWSD;
                Document[] arraySentence = new Document[updateSentence.size()];
                Document[] arrayWSD;

                //텍스트 백업
                String backupText = String.valueOf(list_file.getSelectionModel().getSelectedItems()).replace("[", "").replace("]", "")
                                    + " " + sentenceIndex + " " + tagIndex + " begin " + tempBegin;
                try {
                    String filePath = "C:/Users/Public/Legal_WSD.txt";
                    try {
                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                       Charset.forName("UTF-8"));
                        PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                        printWriter.println(backupText);
                        printWriter.close();
                    } catch (FileNotFoundException e) {
                        File file = new File(filePath);
                        FileWriter fileWriter = new FileWriter(file, true);
                        fileWriter.close();

                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                       Charset.forName("UTF-8"));
                        PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                        printWriter.println(backupText);
                        printWriter.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //텍스트 백업


                for (int i = 0; i < updateSentence.size(); i++) {
                    updateWSD = updateSentence.get(i).getWsdList();
                    arrayWSD = new Document[updateWSD.size()];

                    for (int j = 0; j < updateWSD.size(); j++) {
                        if (i == sentenceIndex && j == tagIndex) {
                            arrayWSD[j] = new Document("id", updateWSD.get(j).getId())
                                          .append("text", updateWSD.get(j).getText())
                                          .append("type", updateWSD.get(j).getType())
                                          .append("scode", updateWSD.get(j).getScode())
                                          .append("begin", Integer.parseInt(tempBegin))
                                          .append("end", updateWSD.get(j).getEnd())
                                          .append("weight", 1)
                                          .append("position", updateWSD.get(j).getPosition());
                            //업데이트된거 바로(?) 볼 수 있게
                            updateWSD.get(j).setBegin(Integer.parseInt(tempBegin));
                            updateWSD.get(j).setWeight(1);
                        } else {
                            arrayWSD[j] = new Document("id", updateWSD.get(j).getId())
                                          .append("text", updateWSD.get(j).getText())
                                          .append("type", updateWSD.get(j).getType())
                                          .append("scode", updateWSD.get(j).getScode())
                                          .append("begin", updateWSD.get(j).getBegin())
                                          .append("end", updateWSD.get(j).getEnd())
                                          .append("weight", updateWSD.get(j).getWeight())
                                          .append("position", updateWSD.get(j).getPosition());
                        }
                    }
                    AnotherUpdate(updateSentence, i);

                    arraySentence[i] = new Document("id", updateSentence.get(i).getId())
                                       .append("reserve_str", updateSentence.get(i).getReserve_str())
                                       .append("text", updateSentence.get(i).getText())
                                       .append("morp", Arrays.asList(arrayMorp))
                                       .append("morp_eval", Arrays.asList(arrayMorpEval))
                                       .append("WSD", Arrays.asList(arrayWSD))
                                       .append("word", Arrays.asList(arrayWord))
                                       .append("NE", Arrays.asList(arrayNe))
                                       .append("chunk", Arrays.asList(arrayChunk))
                                       .append("dependency",Arrays.asList(arrayDependency))
                                       .append("phrase_dependency", Arrays.asList(arrayPhraseDependency))
                                       .append("SRL", Arrays.asList(arraySrl))
                                       .append("relation", Arrays.asList(arrayRelation))
                                       .append("SA", Arrays.asList(arraySa))
                                       .append("ZA", Arrays.asList(arrayZa));
                }

                Document documentSentence = new Document("sentence", Arrays.asList(arraySentence));

                BasicDBObject searchUpdateTitle = new BasicDBObject().append("title.text", getEtriFile.getTitle().getText());
                BasicDBObject update = new BasicDBObject("$set", documentSentence);

                etriDB.getetriCollection().updateOne(searchUpdateTitle, update);
            }

            //            positionText.clear();
            TimeUpdate();
            sentenceEvent(sentenceIndex);
            wsdEvent(tagIndex);
        }
    }

    public void editEnd(KeyEvent keyEvent) {
        if(keyEvent.getCode() == KeyCode.ENTER) {
            endTime = System.currentTimeMillis() - startTime - pauseTime;
            workTime.setText(String.valueOf(tempTime + (int) (endTime / 1000)));

            Document saveDocument = new Document("name", qaName).append("file", fileTitle)
                                                                .append("sentenceIndex", sentenceIndex).append("morpIndex", tagIndex)
                                                                .append("time", (tempTime + (int) (endTime / 1000)));
            BasicDBObject saveName = new BasicDBObject().append("name", qaName);
            BasicDBObject save = new BasicDBObject("$set", saveDocument);
            etriDB.getEtriSave().updateOne(saveName, save);

            String tempEnd = endText.getText();
            endText.setText(String.valueOf(tempEnd));


            if (list_wsd.getSelectionModel().getSelectedIndex() == -1) {
                //팝업창 경고 뛰워주면 좋을듯
                InfoQA.setText("어절을 선택하고 누르세요.");
            } else {
                InfoQA.clear();
                List<Sentence> updateSentence = getEtriFile.getSentenceList();
                List<WSD> updateWSD;
                Document[] arraySentence = new Document[updateSentence.size()];
                Document[] arrayWSD;

                //텍스트 백업
                String backupText = String.valueOf(list_file.getSelectionModel().getSelectedItems()).replace("[", "").replace("]", "")
                                    + " " + sentenceIndex + " " + tagIndex + " end " + tempEnd;
                try {
                    String filePath = "C:/Users/Public/Legal_WSD.txt";
                    try {
                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                       Charset.forName("UTF-8"));
                        PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                        printWriter.println(backupText);
                        printWriter.close();
                    } catch (FileNotFoundException e) {
                        File file = new File(filePath);
                        FileWriter fileWriter = new FileWriter(file, true);
                        fileWriter.close();

                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                       Charset.forName("UTF-8"));
                        PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                        printWriter.println(backupText);
                        printWriter.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //텍스트 백업


                for (int i = 0; i < updateSentence.size(); i++) {
                    updateWSD = updateSentence.get(i).getWsdList();
                    arrayWSD = new Document[updateWSD.size()];

                    for (int j = 0; j < updateWSD.size(); j++) {
                        if (i == sentenceIndex && j == tagIndex) {
                            arrayWSD[j] = new Document("id", updateWSD.get(j).getId())
                                          .append("text", updateWSD.get(j).getText())
                                          .append("type", updateWSD.get(j).getType())
                                          .append("scode", updateWSD.get(j).getScode())
                                          .append("begin", updateWSD.get(j).getBegin())
                                          .append("end", Integer.parseInt(tempEnd))
                                          .append("weight", 1)
                                          .append("position", updateWSD.get(j).getPosition());
                            //업데이트된거 바로(?) 볼 수 있게
                            updateWSD.get(j).setEnd(Integer.parseInt(tempEnd));
                            updateWSD.get(j).setWeight(1);
                        } else {
                            arrayWSD[j] = new Document("id", updateWSD.get(j).getId())
                                          .append("text", updateWSD.get(j).getText())
                                          .append("type", updateWSD.get(j).getType())
                                          .append("scode", updateWSD.get(j).getScode())
                                          .append("begin", updateWSD.get(j).getBegin())
                                          .append("end", updateWSD.get(j).getEnd())
                                          .append("weight", updateWSD.get(j).getWeight())
                                          .append("position", updateWSD.get(j).getPosition());
                        }
                    }
                    AnotherUpdate(updateSentence, i);

                    arraySentence[i] = new Document("id", updateSentence.get(i).getId())
                                       .append("reserve_str", updateSentence.get(i).getReserve_str())
                                       .append("text", updateSentence.get(i).getText())
                                       .append("morp", Arrays.asList(arrayMorp))
                                       .append("morp_eval", Arrays.asList(arrayMorpEval))
                                       .append("WSD", Arrays.asList(arrayWSD))
                                       .append("word", Arrays.asList(arrayWord))
                                       .append("NE", Arrays.asList(arrayNe))
                                       .append("chunk", Arrays.asList(arrayChunk))
                                       .append("dependency",Arrays.asList(arrayDependency))
                                       .append("phrase_dependency", Arrays.asList(arrayPhraseDependency))
                                       .append("SRL", Arrays.asList(arraySrl))
                                       .append("relation", Arrays.asList(arrayRelation))
                                       .append("SA", Arrays.asList(arraySa))
                                       .append("ZA", Arrays.asList(arrayZa));
                }

                Document documentSentence = new Document("sentence", Arrays.asList(arraySentence));

                BasicDBObject searchUpdateTitle = new BasicDBObject().append("title.text", getEtriFile.getTitle().getText());
                BasicDBObject update = new BasicDBObject("$set", documentSentence);

                etriDB.getetriCollection().updateOne(searchUpdateTitle, update);
            }

            //            positionText.clear();
            TimeUpdate();
            sentenceEvent(sentenceIndex);
            wsdEvent(tagIndex);
        }
    }
}
