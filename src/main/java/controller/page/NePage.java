package controller.page;

import com.mongodb.BasicDBObject;
import com.mongodb.client.model.Projections;
import dbconnector.EtriDB;
import dbconnector.KorDic;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import models.EtriFile;
import models.EtriSave;
import models.sentence.Chunk;
import models.sentence.Dependency;
import models.sentence.Morp;
import models.sentence.Morp_eval;
import models.sentence.NE;
import models.sentence.Phrase_dependency;
import models.sentence.Relation0;
import models.sentence.SA;
import models.sentence.SRL;
import models.sentence.Sentence;
import models.sentence.WSD;
import models.sentence.Word;
import models.sentence.ZA;
import models.title.Title;
import org.bson.Document;
import search_kor_dic.SearchKorData;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import static com.mongodb.client.model.Filters.eq;

public class NePage implements Initializable{

    @FXML private ListView<String> list_file;
    @FXML private ListView<String> list_id;
    @FXML private ListView<String> list_ne;
    @FXML private ListView<String> list_morp;

    EtriDB etriDB;
    EtriSave etriSave;
    long startTime = System.currentTimeMillis();
    long endTime;
    int tempTime;
    //디비 접속 버튼
    String qaName = "지정되지 않은 사용자";  //qa할때 저장할 이름
    public void ConnectDBButton(MouseEvent mouseEvent) {

        final Button button = (Button) mouseEvent.getSource();
        final String buttonText = button.getText();

        List<String> tempList = new ArrayList<>();
        if (buttonText.equals("민숙") && InfoQA.getText().equals("3숙")) {
            etriDB = EtriDB.getInstance(buttonText);
            qaName = "민숙";
        } else if (buttonText.equals("희연") && InfoQA.getText().equals("1연")) {
            etriDB = EtriDB.getInstance(buttonText);
            qaName = "희연";
        } else if (buttonText.equals("덕진") && InfoQA.getText().equals("4덕진")) {
            etriDB = EtriDB.getInstance(buttonText);
            qaName = "덕진";
        } else if (buttonText.equals("영훈") && InfoQA.getText().equals("훈2")) {
            etriDB = EtriDB.getInstance(buttonText);
            qaName = "영훈";
        }else if (buttonText.equals("천솔") && InfoQA.getText().equals("천2")) {
            etriDB = EtriDB.getInstance(buttonText);
            qaName = "천솔";
        } else if (buttonText.equals("지원") && InfoQA.getText().equals("3원")) {
            etriDB = EtriDB.getInstance(buttonText);
            qaName = "지원";
        } else if (buttonText.equals("시은") && InfoQA.getText().equals("4시은")) {
            etriDB = EtriDB.getInstance(buttonText);
            qaName = "시은";
        } else if (buttonText.equals("현진") && InfoQA.getText().equals("1진")) {
            etriDB = EtriDB.getInstance(buttonText);
            qaName = "현진";
        } else if(buttonText.equals("실험용") && InfoQA.getText().equals("nlp456")){
            etriDB = EtriDB.getInstance(buttonText);
            qaName = "실험용";
        }
        for (Document document : etriDB.getetriCollection().find().projection(Projections.include("title")))
            tempList.add(new Title((Document) document.get("title")).getText());
        list_file.setItems(FXCollections.observableArrayList(tempList));

        String loadText = "";
        for(Document document: etriDB.getEtriSave().find(eq("name", qaName))) {
            loadText = "마지막으로 태깅한 위치 : " + document.get("name") + " " + document.get("file")
                       + " " + document.get("sentenceIndex") + " " + document.get("morpIndex");
            tempTime = (int) document.get("time");
        }
        InfoQA.setText(loadText);
        workTime.setText(String.valueOf(tempTime));

        startTime = System.currentTimeMillis();
    }

    //파일 리스트 불러오기 -> ConnectDBButton으로 대체
    List<String> korWordList;
    List<String> korHomographList;
    List<String> korMeanList;
    List<String> korPosList;
    List<String> korExList;
    //NE ListView 저장
    @FXML private ListView<String> ne_list_ps;
    @FXML private ListView<String> ne_list_lc;
    @FXML private ListView<String> ne_list_og;
    @FXML private ListView<String> ne_list_af;
    @FXML private ListView<String> ne_list_dt;
    @FXML private ListView<String> ne_list_ti;
    @FXML private ListView<String> ne_list_cv;
    @FXML private ListView<String> ne_list_am;
    @FXML private ListView<String> ne_list_pt;
    @FXML private ListView<String> ne_list_qt;
    @FXML private ListView<String> ne_list_fd;
    @FXML private ListView<String> ne_list_tr;
    @FXML private ListView<String> ne_list_ev;
    @FXML private ListView<String> ne_list_mt;
    @FXML private ListView<String> ne_list_tm;

    public void initialize(final URL url, final ResourceBundle resourceBundle){
        //표준국어대사전 단어 저장
        korWordList = new ArrayList<>();
        korHomographList = new ArrayList<>();
        korMeanList = new ArrayList<>();
        korPosList = new ArrayList<>();
        korExList = new ArrayList<>();
        for (Document document : korDic.getKorStandardDic().find().projection(Projections.include())) {
            korWordList.add(document.get("word").toString());
            korHomographList.add(document.get("homograph").toString());
            korMeanList.add(document.get("mean").toString());
            korPosList.add(document.get("pos").toString());
            korExList.add(document.get("ex").toString());
        }

        //NE 리스트
        List<String> neList = new ArrayList<>();
        neList.add("PS_NAME 인명");
        ne_list_ps.setItems(FXCollections.observableArrayList(neList));
        neList.clear();

        neList.add("LC_OTHERS 지명");
        neList.add("LCP_COUNTRY 국가");
        neList.add("LCP_PROVINCE 도/주 지역명");
        neList.add("LCP_COUNTY 군/면/읍/리/동");
        neList.add("LCP_CITY 도시");
        neList.add("LCP_CAPITALCITY 수도");
        neList.add("LCG_RIVER 강/호수/연못");
        neList.add("LCG_OCEAN 해양/바다");
        neList.add("LCG_BAY 반도/만");
        neList.add("LCG_MOUNTAIN 산/산맥/능선");
        neList.add("LCG_ISLAND 섬/제도");
        neList.add("LCG_CONTINENT 대륙");
        neList.add("LC_TOUR 관광명소");
        neList.add("LC_SPACE 천체 명칭");
        ne_list_lc.setItems(FXCollections.observableArrayList(neList));
        neList.clear();

        neList.add("OG_OHTERS 기타 기관/단체");
        neList.add("OGG_ECONOMY 경제 기관/단체");
        neList.add("OGG_EDUCATION 교육 기관/단체");
        neList.add("OGG_MILITARY 군사 기관/단체");
        neList.add("OGG_MEDIA 미디어 기관/단체");
        neList.add("OGG_SPORTS 스포츠 기관/단체");
        neList.add("OGG_ART 예술 기관/단체");
        neList.add("OGG_MEDICINE 의료 기관/단체");
        neList.add("OGG_RELIGION 종교 기관/단체");
        neList.add("OGG_SCIENCE 과학 기관/단체");
        neList.add("OGG_LIBRARY 도서관");
        neList.add("OGG_LAW 법률 기관/단체");
        neList.add("OGG_POLITICS 공공기관");
        neList.add("OGG_FOOD 음식점");
        neList.add("OGG_HOTEL 숙박업소");
        ne_list_og.setItems(FXCollections.observableArrayList(neList));
        neList.clear();

        neList.add("AF_CULTURAL_ASSET 문화재");
        neList.add("AF_BUILDING 건축물");
        neList.add("AF_MUSICAL_INSTRUMENT 악기");
        neList.add("AF_ROAD 도로");
        neList.add("AF_WEAPON 무기");
        neList.add("AF_TRANSPORT 교통수단");
        neList.add("AF_WORKS 작품명");
        neList.add("AFW_DOCUMENT 도서/서적 작품");
        neList.add("AFW_PERFORMANCE 춤/무용 작품");
        neList.add("AFW_VIDEO 영화 작품");
        neList.add("AFW_ART_CRAFT 미술 작품");
        neList.add("AFW_MUSIC 음악 작품");
        neList.add("AF_WARES 상품/제품");
        ne_list_af.setItems(FXCollections.observableArrayList(neList));
        neList.clear();

        neList.add("DT_OTHERS 기타 날짜");
        neList.add("DT_DURATION 날짜 기간");
        neList.add("DT_DAY 일/절기");
        neList.add("DT_MONTH 달(월)");
        neList.add("DT_YEAR 년");
        neList.add("DT_SEASON 계절");
        neList.add("DT_GEOAGE 지질시대");
        neList.add("DT_DYNASTY 왕조시대");
        ne_list_dt.setItems(FXCollections.observableArrayList(neList));
        neList.clear();

        neList.add("TI_OTHERS 기타 시간");
        neList.add("TI_DURATION 시간 기간");
        neList.add("TI_HOUR 시각");
        neList.add("TI_MINUTE 분");
        neList.add("TI_SECOND 초");
        ne_list_ti.setItems(FXCollections.observableArrayList(neList));
        neList.clear();

        neList.add("CV_NAME 문명/문화");
        neList.add("CV_TRIBE 민족/종족");
        neList.add("CV_SPORTS 스포츠/레져");
        neList.add("CV_SPORTS_INST 스포츠 용품");
        neList.add("CV_POLICY 제도/정책");
        neList.add("CV_TAX 조세");
        neList.add("CV_FUNDS 연금/기금/자금/펀드");
        neList.add("CV_LANGUAGE 언어");
        neList.add("CV_BUILDING_TYPE 건축양식");
        neList.add("CV_FOOD 음식");
        neList.add("CV_DRINK 음료수");
        neList.add("CV_CLOTHING 의복/섬유");
        neList.add("CV_POSITION 직위/직책/스포츠포지션");
        neList.add("CV_RELATION 인간 관계");
        neList.add("CV_OCCUPATION 직업");
        neList.add("CV_CURRENCY 통화");
        neList.add("CV_PRIZE 상");
        neList.add("CV_LAW 법/법률");
        neList.add("CV_FOOD_STYLE 음식종류");
        ne_list_cv.setItems(FXCollections.observableArrayList(neList));
        neList.clear();

        neList.add("AM_OTHERS 기타 동물");
        neList.add("AM_INSECT 곤충");
        neList.add("AM_BIRD 조류");
        neList.add("AM_FISH 어류");
        neList.add("AM_MAMMALIA 포유류");
        neList.add("AM_AMPHIBIA 양서류");
        neList.add("AM_REPTILIA 파충류");
        neList.add("AM_TYPE 동물 분류명칭");
        neList.add("AM_PART 몸 부위");
        ne_list_am.setItems(FXCollections.observableArrayList(neList));
        neList.clear();

        neList.add("PT_OTHERS 기타 식물");
        neList.add("PT_FRUIT 과일");
        neList.add("PT_FLOWER 꽃");
        neList.add("PT_TREE 나무");
        neList.add("PT_GRASS 풀");
        neList.add("PT_TYPE 식물 유형");
        neList.add("PT_PART 식물 부위");
        ne_list_pt.setItems(FXCollections.observableArrayList(neList));
        neList.clear();

        neList.add("QT_OTHERS 기타 수량");
        neList.add("QT_AGE 나이");
        neList.add("QT_SIZE 크기/넓이");
        neList.add("QT_LENGTH 길이/거리/높이");
        neList.add("QT_COUNT 개수/빈도");
        neList.add("QT_MAN_COUNT 인원수");
        neList.add("QT_WEIGHT 무게");
        neList.add("QT_PERCENTAGE 비율");
        neList.add("QT_SPEED 속도");
        neList.add("QT_TEMPERATURE 온도");
        neList.add("QT_VOLUME 부피");
        neList.add("QT_ORDER 순서");
        neList.add("QT_PRICE 금액");
        neList.add("QT_PHONE 전화번호");
        neList.add("QT_SPORTS 스포츠 수량");
        neList.add("QT_CHANNEL 채널");
        neList.add("QT_ALBUM 앨범 수량");
        neList.add("QT_ZIPCODE 우편번호");
        ne_list_qt.setItems(FXCollections.observableArrayList(neList));
        neList.clear();

        neList.add("FD_OTHERS 기타 학문분야/학파");
        neList.add("FD_SCIENCE 과학 학문분야/학파");
        neList.add("FD_SOCIAL_SCIENCE 사회과학 학문분야/학파");
        neList.add("FD_MEDICINE 의학 학문분야/학파");
        neList.add("FD_ART 예술 학문분야/학파");
        neList.add("FD_PHILOSOPHY 철학 학문분야/학파");
        ne_list_fd.setItems(FXCollections.observableArrayList(neList));
        neList.clear();

        neList.add("TR_OTHERS 기타 이론/법칙/원리");
        neList.add("TR_SCIENCE 과학/기술의 이론/법칙/방식");
        neList.add("TR_SOCIAL_SCIENCE 사회과학 이론/법칙/방법/원리/사상");
        neList.add("TR_ART 예술 이론/법칙/방식/양식/사조");
        neList.add("TR_PHILOSOPHY 철학 이론/사상");
        neList.add("TR_MEDICINE 의학 용법/처방/진단법");
        ne_list_tr.setItems(FXCollections.observableArrayList(neList));
        neList.clear();

        neList.add("EV_OTHERS 기타 사건/사고");
        neList.add("EV_ACTIVITY 사회운동/선언");
        neList.add("EV_WAR_REVOLUTION 전쟁/혁명");
        neList.add("EV_SPORTS 스포츠 행사");
        neList.add("EV_FESTIVAL 축제");
        ne_list_ev.setItems(FXCollections.observableArrayList(neList));
        neList.clear();

        neList.add("MT_ELEMENT 원소");
        neList.add("MT_METAL 금속");
        neList.add("MT_ROCK 암석");
        neList.add("MT_CHEMICAL 화학물");
        ne_list_mt.setItems(FXCollections.observableArrayList(neList));
        neList.clear();

        neList.add("TM_COLOR 색");
        neList.add("TM_DIRECTION 방향");
        neList.add("TM_CLIMATE 기후지역");
        neList.add("TM_SHAPE 모양/형태");
        neList.add("TM_CELL_TISSUE 세보/조직/기관");
        neList.add("TMM_DISEASE 질병");
        neList.add("TMM_DRUG 약/약품");
        neList.add("TMI_HW IT 하드웨어");
        neList.add("TMI_SW IT 소프트웨어");
        neList.add("TMI_SITE URL주소");
        neList.add("TMI_EMAIL 이메일주소");
        neList.add("TMI_MODEL 제품 모델");
        neList.add("TMI_SERVICE IT 서비스용어");
        neList.add("TMI_PROJECT 프로젝트");
        neList.add("TMIG_GENRE 게임 장르");
        neList.add("TM_SPORTS 스포츠 용어");
        ne_list_tm.setItems(FXCollections.observableArrayList(neList));
        neList.clear();
    }

    EtriFile getEtriFile;
    //파일 리스트 내부 문장 리스트 불러오기
    String fileTitle;
    public void clickFile(MouseEvent mouseEvent) {
        fileTitle = list_file.getSelectionModel().getSelectedItem();

        getEtriFile = new EtriFile(etriDB.getetriCollection().find(eq("title.text", fileTitle)).iterator().next());

        List<String> tempIDList = new ArrayList<>();
        for(int i=0; i<getEtriFile.getSentenceList().size(); i++)
            tempIDList.add(String.valueOf(getEtriFile.getSentenceList().get(i).getId()));
        list_id.setItems(FXCollections.observableArrayList(tempIDList));

        initSentence();
        initLemmaType();
        initListMorp();
    }

    List<Sentence> getSentence;
    List<NE> getNe;
    List<Morp> getMorp;
    @FXML private Text sentenceText;
    //파일 리스트 내부 문장 내부 형태소 분석 리스트 불러오기
    private int sentenceIndex;
    public void clickId(MouseEvent mouseEvent) {
        sentenceIndex = list_id.getSelectionModel().getSelectedIndex();
        sentenceEvent(sentenceIndex);
    }
    private void sentenceEvent(int index){
        getSentence = getEtriFile.getSentenceList();
        getNe = getSentence.get(index).getNeList();
        getMorp = getSentence.get(index).getMorpList();

        String sentence = getSentence.get(index).getText();
        List<String> tempNeList = new ArrayList<>();
        List<String> tempMorpList = new ArrayList<>();
        for(int i=0; i<getNe.size(); i++) {
            tempNeList.add(String.valueOf(getNe.get(i).getId() + " " + getNe.get(i).getText()));
        }
        list_ne.setItems(FXCollections.observableArrayList(tempNeList));

        for(int i=0; i<getMorp.size(); i++){
            tempMorpList.add(getMorp.get(i).getId() + " " + getMorp.get(i).getLemma());
        }
        list_morp.setItems(FXCollections.observableArrayList(tempMorpList));

        sentenceText.setText(sentence);

        initLemmaType();
    }

    @FXML private TextField lemmaText;
    @FXML private Text typeText;
    @FXML private Text weightText;
    @FXML private TextField beginText;
    @FXML private TextField endText;
    //파일 리스트 내부 문장 내부 형태소 분석 결과 불러오기
    private int tagIndex;
    public void clickNe(MouseEvent mouseEvent){
        tagIndex = list_ne.getSelectionModel().getSelectedIndex();
        neEvent(tagIndex);
    }

    public void pressNe(KeyEvent keyEvent) {
        if(keyEvent.getCode() == KeyCode.UP){
            if(tagIndex > 0) {
                tagIndex--;
                neEvent(tagIndex);
            }
        }
        if(keyEvent.getCode() == KeyCode.DOWN){
            if(tagIndex+1 < getNe.size()) {
                tagIndex++;
                neEvent(tagIndex);
            }
        }
    }

    private void neEvent(int index){
        lemmaText.setText(getNe.get(index).getText());
        typeText.setText(getNe.get(index).getType());
        weightText.setText(String.valueOf(getNe.get(index).getWeight()));
        beginText.setText(String.valueOf(getNe.get(index).getBegin()));
        endText.setText(String.valueOf(getNe.get(index).getEnd()));

        korView(getNe.get(index).getText());
    }


    private void initSentence(){
        sentenceText.setText("");
    }
    private void initLemmaType(){
        lemmaText.setText("");
        typeText.setText("");
        weightText.setText("");
        beginText.setText("");
        endText.setText("");
    }
    private void initListMorp(){
        list_ne.setItems(FXCollections.observableArrayList(""));
    }


    //업데이트
    @FXML private TextField InfoQA;
    private String tempNETag;
    private void Update_NE(String updateTag){
        TimeUpdate();

        typeText.setText(updateTag);

        if (list_ne.getSelectionModel().getSelectedIndex() == -1) {
            //팝업창 경고 뛰워주면 좋을듯
            InfoQA.setText("어절을 선택하고 누르세요.");
        } else {
            InfoQA.clear();
            List<Sentence> updateSentence = getEtriFile.getSentenceList();
            List<NE> updateNe;
            Document[] arraySentence = new Document[updateSentence.size()];
            Document[] arrayNe;

            //텍스트 백업
            String backupText = String.valueOf(list_file.getSelectionModel().getSelectedItems()).replace("[", "").replace("]", "")
                                + " " + sentenceIndex + " " + tagIndex + " " + updateTag;
            try {
                String filePath = "C:/Users/Public/EtriBackup_NE.txt";
                try {
                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                   Charset.forName("UTF-8"));
                    PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                    printWriter.println(backupText);
                    printWriter.close();
                } catch (FileNotFoundException e) {
                    File file = new File(filePath);
                    FileWriter fileWriter = new FileWriter(file, true);
                    fileWriter.close();

                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                   Charset.forName("UTF-8"));
                    PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                    printWriter.println(backupText);
                    printWriter.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            //텍스트 백업

            for (int i = 0; i < updateSentence.size(); i++) {
                updateNe = updateSentence.get(i).getNeList();
                arrayNe = new Document[updateNe.size()];

                for (int j = 0; j < updateNe.size(); j++) {
                    if (i == sentenceIndex && j == tagIndex) {
                        arrayNe[j] = new Document("text", updateNe.get(j).getText())
                                       .append("type", updateTag)
                                       .append("id", updateNe.get(j).getId())
                                       .append("weight", updateNe.get(j).getWeight())
                                       .append("begin", updateNe.get(j).getBegin())
                                       .append("end", updateNe.get(j).getEnd())
                                       .append("common_noun", updateNe.get(j).getCommon_noun());
                        //업데이트된거 바로(?) 볼 수 있게
                        updateNe.get(j).setType(updateTag);

                    } else {
                        arrayNe[j] = new Document("text", updateNe.get(j).getText())
                                       .append("type", updateNe.get(j).getType())
                                       .append("id", updateNe.get(j).getId())
                                       .append("weight", updateNe.get(j).getWeight())
                                       .append("begin", updateNe.get(j).getBegin())
                                       .append("end", updateNe.get(j).getEnd())
                                       .append("common_noun", updateNe.get(j).getCommon_noun());
                    }
                }
                AnotherUpdate(updateSentence, i);

                arraySentence[i] = new Document("id", updateSentence.get(i).getId())
                                   .append("reserve_str", updateSentence.get(i).getReserve_str())
                                   .append("text", updateSentence.get(i).getText())
                                   .append("morp", Arrays.asList(arrayMorp))
                                   .append("morp_eval", Arrays.asList(arrayMorpEval))
                                   .append("WSD", Arrays.asList(arrayWsd))
                                   .append("word", Arrays.asList(arrayWord))
                                   .append("NE", Arrays.asList(arrayNe))
                                   .append("chunk", Arrays.asList(arrayChunk))
                                   .append("dependency",Arrays.asList(arrayDependency))
                                   .append("phrase_dependency", Arrays.asList(arrayPhraseDependency))
                                   .append("SRL", Arrays.asList(arraySrl))
                                   .append("relation", Arrays.asList(arrayRelation))
                                   .append("SA", Arrays.asList(arraySa))
                                   .append("ZA", Arrays.asList(arrayZa));
            }

            Document documentSentence = new Document("sentence", Arrays.asList(arraySentence));

            BasicDBObject searchUpdateTitle = new BasicDBObject().append("title.text", getEtriFile.getTitle().getText());
            BasicDBObject update = new BasicDBObject("$set", documentSentence);

            etriDB.getetriCollection().updateOne(searchUpdateTitle, update);
        }
    }

    public void clickPS(MouseEvent mouseEvent) {
        String tempNE = "";
        if(ne_list_ps != null)
            tempNE = ne_list_ps.getSelectionModel().getSelectedItem();
        String neTag = tempNE.split(" ")[0];

        tempNETag = neTag;
        if(mouseEvent.getClickCount() == 2){
            Update_NE(neTag);
        }
    }

    public void clickLC(MouseEvent mouseEvent) {
        String tempNE = "";
        if(ne_list_lc != null)
            tempNE = ne_list_lc.getSelectionModel().getSelectedItem();
        String neTag = tempNE.split(" ")[0];

        tempNETag = neTag;
        if(mouseEvent.getClickCount() == 2){
            Update_NE(neTag);
        }
    }

    public void clickOG(MouseEvent mouseEvent) {
        String tempNE = "";
        if(ne_list_og != null)
            tempNE = ne_list_og.getSelectionModel().getSelectedItem();
        String neTag = tempNE.split(" ")[0];

        tempNETag = neTag;
        if(mouseEvent.getClickCount() == 2){
            Update_NE(neTag);
        }
    }

    public void clickAF(MouseEvent mouseEvent) {
        String tempNE = "";
        if(ne_list_af != null)
            tempNE = ne_list_af.getSelectionModel().getSelectedItem();
        String neTag = tempNE.split(" ")[0];

        tempNETag = neTag;
        if(mouseEvent.getClickCount() == 2){
            Update_NE(neTag);
        }
    }

    public void clickDT(MouseEvent mouseEvent) {
        String tempNE = "";
        if(ne_list_dt != null)
            tempNE = ne_list_dt.getSelectionModel().getSelectedItem();
        String neTag = tempNE.split(" ")[0];

        tempNETag = neTag;
        if(mouseEvent.getClickCount() == 2){
            Update_NE(neTag);
        }
    }

    public void clickTI(MouseEvent mouseEvent) {
        String tempNE = "";
        if(ne_list_ti != null)
            tempNE = ne_list_ti.getSelectionModel().getSelectedItem();
        String neTag = tempNE.split(" ")[0];

        tempNETag = neTag;
        if(mouseEvent.getClickCount() == 2){
            Update_NE(neTag);
        }
    }

    public void clickCV(MouseEvent mouseEvent) {
        String tempNE = "";
        if(ne_list_cv != null)
            tempNE = ne_list_cv.getSelectionModel().getSelectedItem();
        String neTag = tempNE.split(" ")[0];

        tempNETag = neTag;
        if(mouseEvent.getClickCount() == 2){
            Update_NE(neTag);
        }
    }

    public void clickAM(MouseEvent mouseEvent) {
        String tempNE = "";
        if(ne_list_am != null)
            tempNE = ne_list_am.getSelectionModel().getSelectedItem();
        String neTag = tempNE.split(" ")[0];

        tempNETag = neTag;
        if(mouseEvent.getClickCount() == 2){
            Update_NE(neTag);
        }
    }

    public void clickPT(MouseEvent mouseEvent) {
        String tempNE = "";
        if(ne_list_pt != null)
            tempNE = ne_list_pt.getSelectionModel().getSelectedItem();
        String neTag = tempNE.split(" ")[0];

        tempNETag = neTag;
        if(mouseEvent.getClickCount() == 2){
            Update_NE(neTag);
        }
    }

    public void clickQT(MouseEvent mouseEvent) {
        String tempNE = "";
        if(ne_list_qt != null)
            tempNE = ne_list_qt.getSelectionModel().getSelectedItem();
        String neTag = tempNE.split(" ")[0];

        tempNETag = neTag;
        if(mouseEvent.getClickCount() == 2){
            Update_NE(neTag);
        }
    }

    public void clickFD(MouseEvent mouseEvent) {
        String tempNE = "";
        if(ne_list_fd != null)
            tempNE = ne_list_fd.getSelectionModel().getSelectedItem();
        String neTag = tempNE.split(" ")[0];

        tempNETag = neTag;
        if(mouseEvent.getClickCount() == 2){
            Update_NE(neTag);
        }
    }

    public void clickTR(MouseEvent mouseEvent) {
        String tempNE = "";
        if(ne_list_tr != null)
            tempNE = ne_list_tr.getSelectionModel().getSelectedItem();
        String neTag = tempNE.split(" ")[0];

        tempNETag = neTag;
        if(mouseEvent.getClickCount() == 2){
            Update_NE(neTag);
        }
    }

    public void clickEV(MouseEvent mouseEvent) {
        String tempNE = "";
        if(ne_list_ev != null)
            tempNE = ne_list_ev.getSelectionModel().getSelectedItem();
        String neTag = tempNE.split(" ")[0];

        tempNETag = neTag;
        if(mouseEvent.getClickCount() == 2){
            Update_NE(neTag);
        }
    }

    public void clickMT(MouseEvent mouseEvent) {
        String tempNE = "";
        if(ne_list_mt != null)
            tempNE = ne_list_mt.getSelectionModel().getSelectedItem();
        String neTag = tempNE.split(" ")[0];

        tempNETag = neTag;
        if(mouseEvent.getClickCount() == 2){
            Update_NE(neTag);
        }
    }

    public void clickTM(MouseEvent mouseEvent) {
        String tempNE = "";
        if(ne_list_tm != null)
            tempNE = ne_list_tm.getSelectionModel().getSelectedItem();
        String neTag = tempNE.split(" ")[0];

        tempNETag = neTag;
        if(mouseEvent.getClickCount() == 2){
            Update_NE(neTag);
        }
    }

    //나머지 업데이트용
    List<WSD> updateWsd;
    List<Morp> updateMorp;
    List<Word> updateWord;
    List<ZA> updateZa;
    List<SRL> updateSrl;
    List<Relation0> updateRelation;
    List<Dependency> updateDependency;
    List<SA> updateSa;
    List<Morp_eval> updateMorpEval;
    List<Chunk> updateChunk;
    List<Phrase_dependency> updatePhraseDependency;
    Document[] arrayWsd;
    Document[] arrayMorp;
    Document[] arrayWord;
    Document[] arrayZa;
    Document[] arraySrl;
    Document[] arrayRelation;
    Document[] arrayDependency;
    Document[] arraySa;
    Document[] arrayMorpEval;
    Document[] arrayChunk;
    Document[] arrayPhraseDependency;
    private void AnotherUpdate(List<Sentence> tempSentence, int index){
        updateWsd = tempSentence.get(index).getWsdList();
        updateMorp = tempSentence.get(index).getMorpList();
        updateWord = tempSentence.get(index).getWordList();
        updateZa = tempSentence.get(index).getZaList();
        updateSrl = tempSentence.get(index).getSrlList();
        updateRelation = tempSentence.get(index).getRelation0List();
        updateDependency = tempSentence.get(index).getDependencyList();
        updateSa = tempSentence.get(index).getSaList();
        updateMorpEval = tempSentence.get(index).getMorp_evalList();
        updateChunk = tempSentence.get(index).getChunkList();
        updatePhraseDependency = tempSentence.get(index).getPhrase_dependencyList();

        arrayWsd = new Document[updateWsd.size()];
        arrayMorp = new Document[updateMorp.size()];
        arrayWord = new Document[updateWord.size()];
        arrayZa = new Document[updateZa.size()];
        arraySrl = new Document[updateSrl.size()];
        arrayRelation = new Document[updateRelation.size()];
        arrayDependency = new Document[updateDependency.size()];
        arraySa = new Document[updateSa.size()];
        arrayMorpEval = new Document[updateMorpEval.size()];
        arrayChunk = new Document[updateChunk.size()];
        arrayPhraseDependency = new Document[updatePhraseDependency.size()];

        for(int i=0; i<updateWsd.size(); i++){
            arrayWsd[i] = new Document("id", updateWsd.get(i).getId()).append("text", updateWsd.get(i).getText())
                                                                      .append("type", updateWsd.get(i).getType())
                                                                      .append("scode", updateWsd.get(i).getScode())
                                                                      .append("weight", updateWsd.get(i).getWeight())
                                                                      .append("position", updateWsd.get(i).getPosition())
                                                                      .append("begin", updateWsd.get(i).getBegin())
                                                                      .append("end", updateWsd.get(i).getEnd());
        }
        for(int i = 0; i < updateMorp.size(); i++){
            arrayMorp[i] = new Document("id", updateMorp.get(i).getId()).append("lemma", updateMorp.get(i).getLemma())
                                                                        .append("type", updateMorp.get(i).getType())
                                                                        .append("position", updateMorp.get(i).getPosition())
                                                                        .append("weight", updateMorp.get(i).getWeight());
        }
        for(int i=0; i<updateWord.size(); i++){
            arrayWord[i] = new Document("id", updateWord.get(i).getId()).append("text", updateWord.get(i).getText())
                                                                        .append("type", updateWord.get(i).getType())
                                                                        .append("begin", updateWord.get(i).getBegin())
                                                                        .append("end", updateWord.get(i).getEnd());
        }
        for(int i=0; i<updateZa.size(); i++){
            arrayZa[i] = new Document("id", updateZa.get(i).getId()).append("verb_wid", updateZa.get(i).getVerb_wid())
                                                                    .append("ant_sid", updateZa.get(i).getAnt_sid())
                                                                    .append("ant_wid", updateZa.get(i).getAnt_wid())
                                                                    .append("type", updateZa.get(i).getType())
                                                                    .append("istitle", updateZa.get(i).getIstitle())
                                                                    .append("weight", updateZa.get(i).getWeight());
        }
        for(int i=0; i<updateSrl.size(); i++){
            Document[] arrayArgument = new Document[updateSrl.get(i).getArgumentList().size()];
            for(int j=0; j<arrayArgument.length; j++){
                arrayArgument[j] = new Document("type", updateSrl.get(i).getArgumentList().get(j).getType())
                                   .append("word_id", updateSrl.get(i).getArgumentList().get(j).getWord_id())
                                   .append("text", updateSrl.get(i).getArgumentList().get(j).getText())
                                   .append("weight", updateSrl.get(i).getArgumentList().get(j).getWeight());
            }
            arraySrl[i] = new Document("verb", updateSrl.get(i).getVerb()).append("sense", updateSrl.get(i).getSense())
                                                                          .append("word_id", updateSrl.get(i).getWord_id())
                                                                          .append("weight", updateSrl.get(i).getWeight())
                                                                          .append("argument", Arrays.asList(arrayArgument));
        }
        for(int i=0; i<updateRelation.size(); i++){
            arrayRelation[i] = new Document();
        }
        for(int i=0; i<updateDependency.size(); i++){
            List<Integer> modList = new ArrayList<>();
            for(int j=0; j<updateDependency.get(i).getModList().size(); j++)
                modList.add(updateDependency.get(i).getModList().get(j).getMods());
            arrayDependency[i] = new Document("id", updateDependency.get(i).getId()).append("text", updateDependency.get(i).getText())
                                                                                    .append("head", updateDependency.get(i).getHead())
                                                                                    .append("label", updateDependency.get(i).getLabel())
                                                                                    .append("mod", modList)
                                                                                    .append("weight", updateDependency.get(i).getWeight());
        }
        for(int i=0; i<updateSa.size(); i++){
            arraySa[i] = new Document();
        }
        for(int i=0; i<updateMorpEval.size(); i++){
            arrayMorpEval[i] = new Document("id", updateMorpEval.get(i).getId()).append("result", updateMorpEval.get(i).getResult())
                                                                                .append("target", updateMorpEval.get(i).getTarget())
                                                                                .append("word_id", updateMorpEval.get(i).getWord_id())
                                                                                .append("m_begin", updateMorpEval.get(i).getM_begin())
                                                                                .append("m_end", updateMorpEval.get(i).getM_end());
        }
        for(int i=0; i<updateChunk.size(); i++){
            arrayChunk[i] = new Document();
//            arrayChunk[i] = new Document("id", updateChunk.get(i).getId()).append("type", updateChunk.get(i).getType())
//                                                                          .append("begin", updateChunk.get(i).getBegin())
//                                                                          .append("end", updateChunk.get(i).getEnd())
//                                                                          .append("text", updateChunk.get(i).getText());
        }
        for(int i=0; i<updatePhraseDependency.size(); i++){
            List<Integer> subPhraseList = new ArrayList<>();
            for(int j=0; j<updatePhraseDependency.get(i).getSub_phraseList().size(); j++)
                subPhraseList.add(updatePhraseDependency.get(i).getSub_phraseList().get(j).getSub_phrases());
            Document[] arrayElement = new Document[updatePhraseDependency.get(i).getElementList().size()];
            for(int j=0; j<arrayElement.length; j++){
                arrayElement[j] = new Document("text", updatePhraseDependency.get(i).getElementList().get(j).getText())
                                  .append("label", updatePhraseDependency.get(i).getElementList().get(j).getLabel())
                                  .append("begin", updatePhraseDependency.get(i).getElementList().get(j).getBegin())
                                  .append("end", updatePhraseDependency.get(i).getElementList().get(j).getEnd())
                                  .append("ne_type", updatePhraseDependency.get(i).getElementList().get(j).getNe_type());
            }
            arrayPhraseDependency[i] = new Document("id", updatePhraseDependency.get(i).getId())
                                       .append("label", updatePhraseDependency.get(i).getLabel())
                                       .append("text", updatePhraseDependency.get(i).getText())
                                       .append("begin", updatePhraseDependency.get(i).getBegin())
                                       .append("end", updatePhraseDependency.get(i).getEnd())
                                       .append("key_begin", updatePhraseDependency.get(i).getKey_begin())
                                       .append("head_phrase", updatePhraseDependency.get(i).getHead_phrase())
                                       .append("sub_phrase", subPhraseList)
                                       .append("weight", updatePhraseDependency.get(i).getWeight())
                                       .append("element", Arrays.asList(arrayElement));
        }
    }



    //표준국어대사전 검색
    @FXML private TextField findKorWord;
    @FXML private TableView<SearchKorData> tableKorView = new TableView<>();
    KorDic korDic = KorDic.getInstance();
    ObservableList<SearchKorData> data = tableKorView.getItems();
    private void korView(String word){
        data = tableKorView.getItems();
        data.removeAll(tableKorView.getItems());
        for(int i=0; i<korWordList.size(); i++){
            if(korWordList.get(i).equals(word))
                data.add(new SearchKorData(korHomographList.get(i), korMeanList.get(i), korExList.get(i), korPosList.get(i)));
        }
    }

    public void korWordFind(KeyEvent keyEvent) {
        if(keyEvent.getCode() == KeyCode.ENTER){
            String inputText = findKorWord.getText();
            korView(inputText);
            findKorWord.clear();
        }
    }

    @FXML private Text korMeanText;
    @FXML private Text korExText;
    private int korTagIndex;
    public void korWordClick(MouseEvent mouseEvent) {
        korTagIndex = tableKorView.getSelectionModel().getFocusedIndex();

        if(mouseEvent.getButton() == MouseButton.PRIMARY){
            korMeanText.setText(data.get(korTagIndex).getTableMean());
//            korExText.setText(data.get(korTagIndex).getTableEx());
        }
    }

    public void korWordPress(KeyEvent keyEvent){
        if(keyEvent.getCode() == KeyCode.UP){
            if(korTagIndex > 0) {
                korTagIndex--;
                korWordEvent(korTagIndex);
            }
        }
        if(keyEvent.getCode() == KeyCode.DOWN){
            if(korTagIndex+1 < getNe.size()) {
                korTagIndex++;
                korWordEvent(korTagIndex);
            }
        }
    }

    private void korWordEvent(int index){
        korMeanText.setText(data.get(index).getTableMean());
//        korExText.setText(data.get(index).getTableEx());
    }

    public void Question(KeyEvent keyEvent) {
        if(keyEvent.getCode() == KeyCode.ENTER){
            String tempString = InfoQA.getText();
            try {
                etriDB.getEtriQA().insertOne(new Document("name", qaName).append("file", fileTitle)
                                                                         .append("sentenceIndex", sentenceIndex).append("morpIndex", tagIndex)
                                                                         .append("question", tempString));
                InfoQA.clear();
            }catch (NullPointerException e){
                try {
                    etriDB.getEtriQA().insertOne(new Document("name", qaName).append("question", tempString));
                    InfoQA.clear();
                }catch (NullPointerException e2){
                    InfoQA.setText("디비에 접속한 후 질문해주세요.");
                }
            }
        }
    }

    //작업시간
    @FXML Button pauseButton;
    @FXML Text workTime;
    long pauseTimeStart;
    long pauseTimeEnd;
    long pauseTime = 0L;
    boolean pauseBool = false;
    private void TimeUpdate(){
        endTime = System.currentTimeMillis() - startTime - pauseTime;
        workTime.setText(String.valueOf(tempTime + (int)(endTime/1000)));

        Document saveDocument = new Document("name", qaName).append("file", fileTitle)
                                                            .append("sentenceIndex", sentenceIndex).append("morpIndex", tagIndex)
                                                            .append("time", (tempTime + (int)(endTime/1000)));
        BasicDBObject saveName = new BasicDBObject().append("name", qaName);
        BasicDBObject save = new BasicDBObject("$set", saveDocument);
        etriDB.getEtriSave().updateOne(saveName, save);
    }
    public void WorkPause(MouseEvent mouseEvent) {
        if(pauseBool == false){
            pauseTimeStart = System.currentTimeMillis();
            pauseBool = true;
            pauseButton.setText("작업재개");
        }else if(pauseBool == true){
            pauseTimeEnd = System.currentTimeMillis();
            pauseTime = pauseTime + pauseTimeEnd - pauseTimeStart;
            pauseBool = false;
            pauseButton.setText("일시중지");
        }
    }

    public void WorkExit(MouseEvent mouseEvent) {
        TimeUpdate();
    }


    //현재 위치에 넣을것인지, 다음 위치에 넣을것인지 체크하기 위한 버튼
    @FXML Button removeButton;
    @FXML Button curButton;
    @FXML Button nextButton;
    private String currentORnext;
    public void CurrentCheck(MouseEvent mouseEvent) {
        currentORnext = curButton.getText();
        InputNewNE();
    }

    public void NextCheck(MouseEvent mouseEvent) {
        currentORnext = nextButton.getText();
        InputNewNE();
    }

    public void InputNewNE() { //업데이트for문에서 if문 인덱스 걸어서 insert
        List<Sentence> updateSentence = getEtriFile.getSentenceList();
        List<NE> updateNe;
        Document[] arraySentence = new Document[updateSentence.size()];
        Document[] arrayNe;

        for (int i = 0; i < updateSentence.size(); i++) {
            updateNe = updateSentence.get(i).getNeList();         //자바용 배열
            arrayNe = new Document[updateNe.size()];        //업데이트용 배열

            int arrayNeSize = arrayNe.length;
            int dbIndexCount = 0;
            for (int j = 0; j < arrayNeSize; j++) {    //자바내에서 업데이트
                if (i == sentenceIndex && j == tagIndex) {
                    //텍스트 백업
                    String backupText = String.valueOf(list_file.getSelectionModel().getSelectedItems()).replace("[", "").replace("]", "")
                                        + " " + sentenceIndex + " " + tagIndex + " " + "INSERT" + " " + tempNETag;
                    try {
                        String filePath = "C:/Users/Public/EtriBackup_addNE.txt";
                        try {
                            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                           Charset.forName("UTF-8"));
                            PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                            printWriter.println(backupText);
                            printWriter.close();
                        } catch (FileNotFoundException e) {
                            File file = new File(filePath);
                            FileWriter fileWriter = new FileWriter(file, true);
                            fileWriter.close();

                            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                           Charset.forName("UTF-8"));
                            PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                            printWriter.println(backupText);
                            printWriter.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    //텍스트 백업

                    if(currentORnext.equals("current")) {
                        //배열 크기 재정의
                        Document[] tempArray = new Document[arrayNeSize + 1];
                        for (int k = 0; k < arrayNeSize; k++) {
                            tempArray[k] = arrayNe[k];
                        }
                        arrayNe = tempArray;
                        arrayNeSize++;

                        boolean checkUpdateNE = false;
                        int tempUpdateNeSize = updateNe.size();
                        NE tempNE = new NE(new Document("text", updateNe.get(tempUpdateNeSize-1).getText())
                                           .append("type", updateNe.get(tempUpdateNeSize-1).getType())
                                           .append("weight", updateNe.get(tempUpdateNeSize-1).getWeight())
                                           .append("id", updateNe.get(tempUpdateNeSize-1).getId()+1)
                                           .append("begin", updateNe.get(tempUpdateNeSize-1).getBegin())
                                           .append("end", updateNe.get(tempUpdateNeSize-1).getEnd())
                                           .append("common_noun", updateNe.get(tempUpdateNeSize-1).getCommon_noun()));
                        updateNe.add(updateNe.size(), tempNE);
                        for(int k=updateNe.size()-1; k>=0; k--){
                            if(checkUpdateNE == false) {
                                if (k == j) {
                                    updateNe.get(k).setText("");
                                    updateNe.get(k).setType("");
                                    updateNe.get(k).setWeight(Double.valueOf(1));
                                    updateNe.get(k).setId(j);
                                    updateNe.get(k).setBegin(-1);
                                    updateNe.get(k).setEnd(-1);
                                    updateNe.get(k).setCommon_noun(0);
                                    checkUpdateNE = true;
                                } else {
                                    updateNe.get(k).setText(updateNe.get(k - 1).getText());
                                    updateNe.get(k).setType(updateNe.get(k - 1).getType());
                                    updateNe.get(k).setWeight(updateNe.get(k - 1).getWeight());
                                    updateNe.get(k).setId(updateNe.get(k - 1).getId() + 1);
                                    updateNe.get(k).setBegin(updateNe.get(k - 1).getBegin());
                                    updateNe.get(k).setEnd(updateNe.get(k - 1).getEnd());
                                    updateNe.get(k).setCommon_noun(updateNe.get(k - 1).getCommon_noun());
                                }
                            }else{

                            }
                        }

                        for(int k=0; k<updateNe.size(); k++){
                            arrayNe[k] = new Document("text", updateNe.get(k).getText())
                                         .append("type", updateNe.get(k).getType())
                                         .append("id", updateNe.get(k).getId())
                                         .append("weight", updateNe.get(k).getWeight())
                                         .append("begin", updateNe.get(k).getBegin())
                                         .append("end", updateNe.get(k).getEnd())
                                         .append("common_noun", updateNe.get(k).getCommon_noun());
                        }
                        j++;
                    }else if(currentORnext.equals("next")){
                        //배열 크기 재정의
                        Document[] tempArray = new Document[arrayNeSize + 1];
                        for (int k = 0; k < arrayNeSize; k++) {
                            tempArray[k] = arrayNe[k];
                        }
                        arrayNe = tempArray;
                        arrayNeSize++;

                        boolean checkUpdateNE = false;
                        int tempUpdateNeSize = updateNe.size();
                        NE tempNE = new NE(new Document("text", updateNe.get(tempUpdateNeSize-1).getText())
                                           .append("type", updateNe.get(tempUpdateNeSize-1).getType())
                                           .append("weight", updateNe.get(tempUpdateNeSize-1).getWeight())
                                           .append("id", updateNe.get(tempUpdateNeSize-1).getId()+1)
                                           .append("begin", updateNe.get(tempUpdateNeSize-1).getBegin())
                                           .append("end", updateNe.get(tempUpdateNeSize-1).getEnd())
                                           .append("common_noun", updateNe.get(tempUpdateNeSize-1).getCommon_noun()));
                        updateNe.add(updateNe.size(), tempNE);
                        for(int k=updateNe.size()-1; k>=0; k--){
                            if(checkUpdateNE == false) {
                                if (k == j+1) {
                                    updateNe.get(k).setText("");
                                    updateNe.get(k).setType("");
                                    updateNe.get(k).setWeight(Double.valueOf(1));
                                    updateNe.get(k).setId(j+1);
                                    updateNe.get(k).setBegin(-1);
                                    updateNe.get(k).setEnd(-1);
                                    updateNe.get(k).setCommon_noun(0);
                                    checkUpdateNE = true;
                                } else {
                                    updateNe.get(k).setText(updateNe.get(k - 1).getText());
                                    updateNe.get(k).setType(updateNe.get(k - 1).getType());
                                    updateNe.get(k).setWeight(updateNe.get(k - 1).getWeight());
                                    updateNe.get(k).setId(updateNe.get(k - 1).getId() + 1);
                                    updateNe.get(k).setBegin(updateNe.get(k - 1).getBegin());
                                    updateNe.get(k).setEnd(updateNe.get(k - 1).getEnd());
                                    updateNe.get(k).setCommon_noun(updateNe.get(k - 1).getCommon_noun());
                                }
                            }else{

                            }
                        }

                        for(int k=0; k<updateNe.size(); k++){
                            arrayNe[k] = new Document("text", updateNe.get(k).getText())
                                         .append("type", updateNe.get(k).getType())
                                         .append("id", updateNe.get(k).getId())
                                         .append("weight", updateNe.get(k).getWeight())
                                         .append("begin", updateNe.get(k).getBegin())
                                         .append("end", updateNe.get(k).getEnd())
                                         .append("common_noun", updateNe.get(k).getCommon_noun());
                        }
                        j++;
                    }
                } else {
                    arrayNe[j] = new Document("text", updateNe.get(dbIndexCount).getText())
                                 .append("type", updateNe.get(dbIndexCount).getType())
                                 .append("id", updateNe.get(dbIndexCount).getId())
                                 .append("weight", updateNe.get(dbIndexCount).getWeight())
                                 .append("begin", updateNe.get(dbIndexCount).getBegin())
                                 .append("end", updateNe.get(dbIndexCount).getEnd())
                                 .append("common_noun", updateNe.get(dbIndexCount).getCommon_noun());
                }
                dbIndexCount++;
            }

            arraySentence[i] = new Document("id", updateSentence.get(i).getId())
                               .append("reserve_str", updateSentence.get(i).getReserve_str())
                               .append("text", updateSentence.get(i).getText())
                               .append("morp", Arrays.asList(arrayMorp))
                               .append("morp_eval", Arrays.asList(arrayMorpEval))
                               .append("WSD", Arrays.asList(arrayWsd))
                               .append("word", Arrays.asList(arrayWord))
                               .append("NE", Arrays.asList(arrayNe))
                               .append("chunk", Arrays.asList(arrayChunk))
                               .append("dependency",Arrays.asList(arrayDependency))
                               .append("phrase_dependency", Arrays.asList(arrayPhraseDependency))
                               .append("SRL", Arrays.asList(arraySrl))
                               .append("relation", Arrays.asList(arrayRelation))
                               .append("SA", Arrays.asList(arraySa))
                               .append("ZA", Arrays.asList(arrayZa));
        }

        Document documentSentence = new Document("sentence", Arrays.asList(arraySentence));

        BasicDBObject searchUpdateTitle = new BasicDBObject().append("title.text", getEtriFile.getTitle().getText());
        BasicDBObject update = new BasicDBObject("$set", documentSentence);

        etriDB.getetriCollection().updateOne(searchUpdateTitle, update);


        TimeUpdate();
        sentenceEvent(sentenceIndex);
    }

    //개체명 삭제
    public void RemoveNE(MouseEvent mouseEvent) {
        //텍스트 백업
        String backupText = String.valueOf(list_file.getSelectionModel().getSelectedItems()).replace("[", "").replace("]", "")
                            + " " + sentenceIndex + " " + tagIndex + " " + "remove";
        try {
            String filePath = "C:/Users/Public/EtriBackup_addNE.txt";
            try {
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                               Charset.forName("UTF-8"));
                PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                printWriter.println(backupText);
                printWriter.close();
            } catch (FileNotFoundException e) {
                File file = new File(filePath);
                FileWriter fileWriter = new FileWriter(file, true);
                fileWriter.close();

                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                               Charset.forName("UTF-8"));
                PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                printWriter.println(backupText);
                printWriter.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        //텍스트 백업

        List<Sentence> updateSentence = getEtriFile.getSentenceList();
        List<NE> updateNe;
        Document[] arraySentence = new Document[updateSentence.size()];
        Document[] arrayNe;

        for (int i = 0; i < updateSentence.size(); i++) {
            updateNe = updateSentence.get(i).getNeList();         //자바용 배열
            arrayNe = new Document[updateNe.size()];        //업데이트용 배열

            int arrayNeSize = arrayNe.length;
            boolean removeCheck = false;
            for (int j = 0; j < updateNe.size(); j++) {    //자바내에서 업데이트
                if(removeCheck == false) {
                    if (i == sentenceIndex && j == tagIndex) {
                        if(j != updateNe.size()-1) {
                            updateNe.get(j).setText(updateNe.get(j + 1).getText());
                            updateNe.get(j).setType(updateNe.get(j + 1).getType());
                            updateNe.get(j).setWeight(updateNe.get(j + 1).getWeight());
                            updateNe.get(j).setId(updateNe.get(j + 1).getId() - 1);
                            updateNe.get(j).setBegin(updateNe.get(j + 1).getBegin());
                            updateNe.get(j).setEnd(updateNe.get(j + 1).getEnd());
                            updateNe.get(j).setCommon_noun(updateNe.get(j + 1).getCommon_noun());
                        }
                        removeCheck = true;
                    } else {
                        updateNe.get(j).setText(updateNe.get(j).getText());
                        updateNe.get(j).setType(updateNe.get(j).getType());
                        updateNe.get(j).setWeight(updateNe.get(j).getWeight());
                        updateNe.get(j).setId(updateNe.get(j).getId());
                        updateNe.get(j).setBegin(updateNe.get(j).getBegin());
                        updateNe.get(j).setEnd(updateNe.get(j).getEnd());
                        updateNe.get(j).setCommon_noun(updateNe.get(j).getCommon_noun());
                    }
                }else{
                    if(j+1 != arrayNeSize) {
                        updateNe.get(j).setText(updateNe.get(j + 1).getText());
                        updateNe.get(j).setType(updateNe.get(j + 1).getType());
                        updateNe.get(j).setWeight(updateNe.get(j + 1).getWeight());
                        updateNe.get(j).setId(updateNe.get(j + 1).getId() - 1);
                        updateNe.get(j).setBegin(updateNe.get(j + 1).getBegin());
                        updateNe.get(j).setEnd(updateNe.get(j + 1).getEnd());
                        updateNe.get(j).setCommon_noun(updateNe.get(j + 1).getCommon_noun());
                    }
                }
            }
            if(removeCheck == true){
                updateNe.remove(updateNe.size()-1);
            }
            //배열 크기 재정의
            Document[] tempArray = new Document[updateNe.size()];
            for(int k=0; k<updateNe.size(); k++){
                tempArray[k] = arrayNe[k];
            }
            arrayNe = tempArray;

            for(int k=0; k<updateNe.size(); k++){
                arrayNe[k] = new Document("text", updateNe.get(k).getText())
                             .append("type", updateNe.get(k).getType())
                             .append("id", updateNe.get(k).getId())
                             .append("weight", updateNe.get(k).getWeight())
                             .append("begin", updateNe.get(k).getBegin())
                             .append("end", updateNe.get(k).getEnd())
                             .append("common_noun", updateNe.get(k).getCommon_noun());
            }
            AnotherUpdate(updateSentence, i);

            arraySentence[i] = new Document("id", updateSentence.get(i).getId())
                               .append("reserve_str", updateSentence.get(i).getReserve_str())
                               .append("text", updateSentence.get(i).getText())
                               .append("morp", Arrays.asList(arrayMorp))
                               .append("morp_eval", Arrays.asList(arrayMorpEval))
                               .append("WSD", Arrays.asList(arrayWsd))
                               .append("word", Arrays.asList(arrayWord))
                               .append("NE", Arrays.asList(arrayNe))
                               .append("chunk", Arrays.asList(arrayChunk))
                               .append("dependency",Arrays.asList(arrayDependency))
                               .append("phrase_dependency", Arrays.asList(arrayPhraseDependency))
                               .append("SRL", Arrays.asList(arraySrl))
                               .append("relation", Arrays.asList(arrayRelation))
                               .append("SA", Arrays.asList(arraySa))
                               .append("ZA", Arrays.asList(arrayZa));
        }

        Document documentSentence = new Document("sentence", Arrays.asList(arraySentence));

        BasicDBObject searchUpdateTitle = new BasicDBObject().append("title.text", getEtriFile.getTitle().getText());
        BasicDBObject update = new BasicDBObject("$set", documentSentence);

        etriDB.getetriCollection().updateOne(searchUpdateTitle, update);


        TimeUpdate();
        sentenceEvent(sentenceIndex);
    }
    public void InsertNE(MouseEvent mouseEvent) {
        //텍스트 백업
        String backupText = String.valueOf(list_file.getSelectionModel().getSelectedItems()).replace("[", "").replace("]", "")
                            + " " + sentenceIndex + " " + tagIndex + " " + "insert";
        try {
            String filePath = "C:/Users/Public/EtriBackup_addNE.txt";
            try {
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                               Charset.forName("UTF-8"));
                PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                printWriter.println(backupText);
                printWriter.close();
            } catch (FileNotFoundException e) {
                File file = new File(filePath);
                FileWriter fileWriter = new FileWriter(file, true);
                fileWriter.close();

                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                               Charset.forName("UTF-8"));
                PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                printWriter.println(backupText);
                printWriter.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        //텍스트 백업

        List<Sentence> updateSentence = getEtriFile.getSentenceList();
        List<NE> updateNe;
        Document[] arraySentence = new Document[updateSentence.size()];
        Document[] arrayNe;

        for (int i = 0; i < updateSentence.size(); i++) {
            updateNe = updateSentence.get(i).getNeList();         //자바용 배열

            NE insertNE = new NE(new Document("id", 0). append("text", "").append("type", "")
                                                      .append("begin", -1).append("end", -1)
                                                      .append("weight", 1).append("common_noun", 0));
            //for-j로 들어갈 수 없으니 여기서 설정
            if (i == sentenceIndex) {
                updateNe.add(0, insertNE);
            }
            for (int j = 0; j < updateNe.size(); j++) {    //자바내에서 업데이트
                if (i == sentenceIndex) {

                }else{
                    updateNe.get(j).setText(updateNe.get(j).getText());
                    updateNe.get(j).setType(updateNe.get(j).getType());
                    updateNe.get(j).setWeight(updateNe.get(j).getWeight());
                    updateNe.get(j).setId(updateNe.get(j).getId());
                    updateNe.get(j).setBegin(updateNe.get(j).getBegin());
                    updateNe.get(j).setEnd(updateNe.get(j).getEnd());
                    updateNe.get(j).setCommon_noun(updateNe.get(j).getCommon_noun());
                }
            }

            //배열 크기 재정의
            arrayNe = new Document[updateNe.size()];
            Document[] tempArray = new Document[updateNe.size()];
            for(int k=0; k<updateNe.size(); k++){
                tempArray[k] = arrayNe[k];
            }
            arrayNe = tempArray;

            if(updateNe.size() == 0){
                AnotherUpdate(updateSentence, i);

                arraySentence[i] = new Document("id", updateSentence.get(i).getId())
                                   .append("reserve_str", updateSentence.get(i).getReserve_str())
                                   .append("text", updateSentence.get(i).getText())
                                   .append("WSD", Arrays.asList(arrayWsd))
                                   .append("morp", Arrays.asList(arrayMorp))
                                   .append("NE", Arrays.asList(arrayNe))
                                   .append("word", Arrays.asList(arrayWord))
                                   .append("ZA", Arrays.asList(arrayZa))
                                   .append("SRL", Arrays.asList(arraySrl))
                                   .append("relation", Arrays.asList(arrayRelation))
                                   .append("dependency", Arrays.asList(arrayDependency))
                                   .append("SA", Arrays.asList(arraySa))
                                   .append("morp_eval", Arrays.asList(arrayMorpEval))
                                   .append("chunk", Arrays.asList(arrayChunk))
                                   .append("phrase_dependency", Arrays.asList(arrayPhraseDependency));
            }
            else {
                for (int j = 0; j < updateNe.size(); j++) {
                    arrayNe[j] = new Document("id", updateNe.get(j).getId())
                                 .append("text", updateNe.get(j).getText())
                                 .append("type", updateNe.get(j).getType())
                                 .append("begin", updateNe.get(j).getBegin())
                                 .append("end", updateNe.get(j).getEnd())
                                 .append("weight", updateNe.get(j).getWeight())
                                 .append("common_noun", updateNe.get(j).getCommon_noun());

                    AnotherUpdate(updateSentence, i);

                    arraySentence[i] = new Document("id", updateSentence.get(i).getId())
                                       .append("reserve_str", updateSentence.get(i).getReserve_str())
                                       .append("text", updateSentence.get(i).getText())
                                       .append("WSD", Arrays.asList(arrayWsd))
                                       .append("morp", Arrays.asList(arrayMorp))
                                       .append("NE", Arrays.asList(arrayNe))
                                       .append("word", Arrays.asList(arrayWord))
                                       .append("ZA", Arrays.asList(arrayZa))
                                       .append("SRL", Arrays.asList(arraySrl))
                                       .append("relation", Arrays.asList(arrayRelation))
                                       .append("dependency", Arrays.asList(arrayDependency))
                                       .append("SA", Arrays.asList(arraySa))
                                       .append("morp_eval", Arrays.asList(arrayMorpEval))
                                       .append("chunk", Arrays.asList(arrayChunk))
                                       .append("phrase_dependency", Arrays.asList(arrayPhraseDependency));
                }
            }
        }

        Document documentSentence = new Document("sentence", Arrays.asList(arraySentence));

        BasicDBObject searchUpdateTitle = new BasicDBObject().append("title.text", getEtriFile.getTitle().getText());
        BasicDBObject update = new BasicDBObject("$set", documentSentence);

        etriDB.getetriCollection().updateOne(searchUpdateTitle, update);


        TimeUpdate();
        sentenceEvent(sentenceIndex);
        sentenceEvent(sentenceIndex);
    }

    public void editLemma(KeyEvent keyEvent) {
        if(keyEvent.getCode() == KeyCode.ENTER) {
            endTime = System.currentTimeMillis() - startTime - pauseTime;
            workTime.setText(String.valueOf(tempTime + (int) (endTime / 1000)));

            Document saveDocument = new Document("name", qaName).append("file", fileTitle)
                                                                .append("sentenceIndex", sentenceIndex).append("morpIndex", tagIndex)
                                                                .append("time", (tempTime + (int) (endTime / 1000)));
            BasicDBObject saveName = new BasicDBObject().append("name", qaName);
            BasicDBObject save = new BasicDBObject("$set", saveDocument);
            etriDB.getEtriSave().updateOne(saveName, save);

            String tempLemma = lemmaText.getText();
            lemmaText.setText(String.valueOf(tempLemma));


            if (list_ne.getSelectionModel().getSelectedIndex() == -1) {
                //팝업창 경고 뛰워주면 좋을듯
                InfoQA.setText("어절을 선택하고 누르세요.");
            } else {
                InfoQA.clear();
                List<Sentence> updateSentence = getEtriFile.getSentenceList();
                List<NE> updateNE;
                Document[] arraySentence = new Document[updateSentence.size()];
                Document[] arrayNE;

                //텍스트 백업
                String backupText = String.valueOf(list_file.getSelectionModel().getSelectedItems()).replace("[", "").replace("]", "")
                                    + " " + sentenceIndex + " " + tagIndex + " lemma " + tempLemma;
                try {
                    String filePath = "C:/Users/Public/Legal_NE.txt";
                    try {
                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                       Charset.forName("UTF-8"));
                        PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                        printWriter.println(backupText);
                        printWriter.close();
                    } catch (FileNotFoundException e) {
                        File file = new File(filePath);
                        FileWriter fileWriter = new FileWriter(file, true);
                        fileWriter.close();

                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                       Charset.forName("UTF-8"));
                        PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                        printWriter.println(backupText);
                        printWriter.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //텍스트 백업


                for (int i = 0; i < updateSentence.size(); i++) {
                    updateNE = updateSentence.get(i).getNeList();
                    arrayNE = new Document[updateNE.size()];

                    for (int j = 0; j < updateNE.size(); j++) {
                        if (i == sentenceIndex && j == tagIndex) {
                            arrayNE[j] = new Document("id", updateNE.get(j).getId())
                                         .append("text", tempLemma)
                                         .append("type", updateNE.get(j).getType())
                                         .append("begin", updateNE.get(j).getBegin())
                                         .append("end", updateNE.get(j).getEnd())
                                         .append("weight", 1)
                                         .append("common_noun", updateNE.get(j).getCommon_noun());
                            //업데이트된거 바로(?) 볼 수 있게
                            updateNE.get(j).setText(tempLemma);
                            updateNE.get(j).setWeight(1);
                        } else {
                            arrayNE[j] = new Document("id", updateNE.get(j).getId())
                                         .append("text", updateNE.get(j).getText())
                                         .append("type", updateNE.get(j).getType())
                                         .append("begin", updateNE.get(j).getBegin())
                                         .append("end", updateNE.get(j).getEnd())
                                         .append("weight", updateNE.get(j).getWeight())
                                         .append("common_noun", updateNE.get(j).getCommon_noun());
                        }
                    }
                    AnotherUpdate(updateSentence, i);

                    arraySentence[i] = new Document("id", updateSentence.get(i).getId())
                                       .append("reserve_str", updateSentence.get(i).getReserve_str())
                                       .append("text", updateSentence.get(i).getText())
                                       .append("morp", Arrays.asList(arrayMorp))
                                       .append("morp_eval", Arrays.asList(arrayMorpEval))
                                       .append("WSD", Arrays.asList(arrayWsd))
                                       .append("word", Arrays.asList(arrayWord))
                                       .append("NE", Arrays.asList(arrayNE))
                                       .append("chunk", Arrays.asList(arrayChunk))
                                       .append("dependency",Arrays.asList(arrayDependency))
                                       .append("phrase_dependency", Arrays.asList(arrayPhraseDependency))
                                       .append("SRL", Arrays.asList(arraySrl))
                                       .append("relation", Arrays.asList(arrayRelation))
                                       .append("SA", Arrays.asList(arraySa))
                                       .append("ZA", Arrays.asList(arrayZa));
                }

                Document documentSentence = new Document("sentence", Arrays.asList(arraySentence));

                BasicDBObject searchUpdateTitle = new BasicDBObject().append("title.text", getEtriFile.getTitle().getText());
                BasicDBObject update = new BasicDBObject("$set", documentSentence);

                etriDB.getetriCollection().updateOne(searchUpdateTitle, update);
            }

            //            positionText.clear();
            TimeUpdate();
            sentenceEvent(sentenceIndex);
            neEvent(tagIndex);
        }
    }

    public void editBegin(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.ENTER) {
            endTime = System.currentTimeMillis() - startTime - pauseTime;
            workTime.setText(String.valueOf(tempTime + (int) (endTime / 1000)));

            Document saveDocument = new Document("name", qaName).append("file", fileTitle)
                                                                .append("sentenceIndex", sentenceIndex).append("morpIndex", tagIndex)
                                                                .append("time", (tempTime + (int) (endTime / 1000)));
            BasicDBObject saveName = new BasicDBObject().append("name", qaName);
            BasicDBObject save = new BasicDBObject("$set", saveDocument);
            etriDB.getEtriSave().updateOne(saveName, save);

            int tempBegin = Integer.parseInt(beginText.getText());
            beginText.setText(String.valueOf(tempBegin));


            if (list_ne.getSelectionModel().getSelectedIndex() == -1) {
                //팝업창 경고 뛰워주면 좋을듯
                InfoQA.setText("어절을 선택하고 누르세요.");
            } else {
                InfoQA.clear();
                List<Sentence> updateSentence = getEtriFile.getSentenceList();
                List<NE> updateNE;
                Document[] arraySentence = new Document[updateSentence.size()];
                Document[] arrayNE;

                //텍스트 백업
                String backupText = String.valueOf(list_file.getSelectionModel().getSelectedItems()).replace("[", "").replace("]", "")
                                    + " " + sentenceIndex + " " + tagIndex + " begin " + tempBegin;
                try {
                    String filePath = "C:/Users/Public/Legal_NE.txt";
                    try {
                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                       Charset.forName("UTF-8"));
                        PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                        printWriter.println(backupText);
                        printWriter.close();
                    } catch (FileNotFoundException e) {
                        File file = new File(filePath);
                        FileWriter fileWriter = new FileWriter(file, true);
                        fileWriter.close();

                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                       Charset.forName("UTF-8"));
                        PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                        printWriter.println(backupText);
                        printWriter.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //텍스트 백업


                for (int i = 0; i < updateSentence.size(); i++) {
                    updateNE = updateSentence.get(i).getNeList();
                    arrayNE = new Document[updateNE.size()];

                    for (int j = 0; j < updateNE.size(); j++) {
                        if (i == sentenceIndex && j == tagIndex) {
                            arrayNE[j] = new Document("id", updateNE.get(j).getId())
                                         .append("text", updateNE.get(j).getText())
                                         .append("type", updateNE.get(j).getType())
                                         .append("begin", tempBegin)
                                         .append("end", updateNE.get(j).getEnd())
                                         .append("weight", 1)
                                         .append("common_noun", updateNE.get(j).getCommon_noun());
                            //업데이트된거 바로(?) 볼 수 있게
                            updateNE.get(j).setBegin(tempBegin);
                            updateNE.get(j).setWeight(1);
                        } else {
                            arrayNE[j] = new Document("id", updateNE.get(j).getId())
                                         .append("text", updateNE.get(j).getText())
                                         .append("type", updateNE.get(j).getType())
                                         .append("begin", updateNE.get(j).getBegin())
                                         .append("end", updateNE.get(j).getEnd())
                                         .append("weight", updateNE.get(j).getWeight())
                                         .append("common_noun", updateNE.get(j).getCommon_noun());
                        }
                    }
                    AnotherUpdate(updateSentence, i);

                    arraySentence[i] = new Document("id", updateSentence.get(i).getId())
                                       .append("reserve_str", updateSentence.get(i).getReserve_str())
                                       .append("text", updateSentence.get(i).getText())
                                       .append("morp", Arrays.asList(arrayMorp))
                                       .append("morp_eval", Arrays.asList(arrayMorpEval))
                                       .append("WSD", Arrays.asList(arrayWsd))
                                       .append("word", Arrays.asList(arrayWord))
                                       .append("NE", Arrays.asList(arrayNE))
                                       .append("chunk", Arrays.asList(arrayChunk))
                                       .append("dependency",Arrays.asList(arrayDependency))
                                       .append("phrase_dependency", Arrays.asList(arrayPhraseDependency))
                                       .append("SRL", Arrays.asList(arraySrl))
                                       .append("relation", Arrays.asList(arrayRelation))
                                       .append("SA", Arrays.asList(arraySa))
                                       .append("ZA", Arrays.asList(arrayZa));
                }

                Document documentSentence = new Document("sentence", Arrays.asList(arraySentence));

                BasicDBObject searchUpdateTitle = new BasicDBObject().append("title.text", getEtriFile.getTitle().getText());
                BasicDBObject update = new BasicDBObject("$set", documentSentence);

                etriDB.getetriCollection().updateOne(searchUpdateTitle, update);
            }

            //            positionText.clear();
            TimeUpdate();
            sentenceEvent(sentenceIndex);
            neEvent(tagIndex);
        }
    }

    public void editEnd(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.ENTER) {
            endTime = System.currentTimeMillis() - startTime - pauseTime;
            workTime.setText(String.valueOf(tempTime + (int) (endTime / 1000)));

            Document saveDocument = new Document("name", qaName).append("file", fileTitle)
                                                                .append("sentenceIndex", sentenceIndex).append("morpIndex", tagIndex)
                                                                .append("time", (tempTime + (int) (endTime / 1000)));
            BasicDBObject saveName = new BasicDBObject().append("name", qaName);
            BasicDBObject save = new BasicDBObject("$set", saveDocument);
            etriDB.getEtriSave().updateOne(saveName, save);

            int tempEnd = Integer.parseInt(endText.getText());
            endText.setText(String.valueOf(tempEnd));


            if (list_ne.getSelectionModel().getSelectedIndex() == -1) {
                //팝업창 경고 뛰워주면 좋을듯
                InfoQA.setText("어절을 선택하고 누르세요.");
            } else {
                InfoQA.clear();
                List<Sentence> updateSentence = getEtriFile.getSentenceList();
                List<NE> updateNE;
                Document[] arraySentence = new Document[updateSentence.size()];
                Document[] arrayNE;

                //텍스트 백업
                String backupText = String.valueOf(list_file.getSelectionModel().getSelectedItems()).replace("[", "").replace("]", "")
                                    + " " + sentenceIndex + " " + tagIndex + " end " + tempEnd;
                try {
                    String filePath = "C:/Users/Public/Legal_NE.txt";
                    try {
                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                       Charset.forName("UTF-8"));
                        PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                        printWriter.println(backupText);
                        printWriter.close();
                    } catch (FileNotFoundException e) {
                        File file = new File(filePath);
                        FileWriter fileWriter = new FileWriter(file, true);
                        fileWriter.close();

                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                       Charset.forName("UTF-8"));
                        PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                        printWriter.println(backupText);
                        printWriter.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //텍스트 백업


                for (int i = 0; i < updateSentence.size(); i++) {
                    updateNE = updateSentence.get(i).getNeList();
                    arrayNE = new Document[updateNE.size()];

                    for (int j = 0; j < updateNE.size(); j++) {
                        if (i == sentenceIndex && j == tagIndex) {
                            arrayNE[j] = new Document("id", updateNE.get(j).getId())
                                         .append("text", updateNE.get(j).getText())
                                         .append("type", updateNE.get(j).getType())
                                         .append("begin", updateNE.get(j).getBegin())
                                         .append("end", tempEnd)
                                         .append("weight", 1)
                                         .append("common_noun", updateNE.get(j).getCommon_noun());
                            //업데이트된거 바로(?) 볼 수 있게
                            updateNE.get(j).setEnd(tempEnd);
                            updateNE.get(j).setWeight(1);
                        } else {
                            arrayNE[j] = new Document("id", updateNE.get(j).getId())
                                         .append("text", updateNE.get(j).getText())
                                         .append("type", updateNE.get(j).getType())
                                         .append("begin", updateNE.get(j).getBegin())
                                         .append("end", updateNE.get(j).getEnd())
                                         .append("weight", updateNE.get(j).getWeight())
                                         .append("common_noun", updateNE.get(j).getCommon_noun());
                        }
                    }
                    AnotherUpdate(updateSentence, i);

                    arraySentence[i] = new Document("id", updateSentence.get(i).getId())
                                       .append("reserve_str", updateSentence.get(i).getReserve_str())
                                       .append("text", updateSentence.get(i).getText())
                                       .append("morp", Arrays.asList(arrayMorp))
                                       .append("morp_eval", Arrays.asList(arrayMorpEval))
                                       .append("WSD", Arrays.asList(arrayWsd))
                                       .append("word", Arrays.asList(arrayWord))
                                       .append("NE", Arrays.asList(arrayNE))
                                       .append("chunk", Arrays.asList(arrayChunk))
                                       .append("dependency",Arrays.asList(arrayDependency))
                                       .append("phrase_dependency", Arrays.asList(arrayPhraseDependency))
                                       .append("SRL", Arrays.asList(arraySrl))
                                       .append("relation", Arrays.asList(arrayRelation))
                                       .append("SA", Arrays.asList(arraySa))
                                       .append("ZA", Arrays.asList(arrayZa));
                }

                Document documentSentence = new Document("sentence", Arrays.asList(arraySentence));

                BasicDBObject searchUpdateTitle = new BasicDBObject().append("title.text", getEtriFile.getTitle().getText());
                BasicDBObject update = new BasicDBObject("$set", documentSentence);

                etriDB.getetriCollection().updateOne(searchUpdateTitle, update);
            }

            //            positionText.clear();
            TimeUpdate();
            sentenceEvent(sentenceIndex);
            neEvent(tagIndex);
        }
    }
}
