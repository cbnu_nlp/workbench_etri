package controller.page;

import com.mongodb.BasicDBObject;
import com.mongodb.client.model.Projections;
import dbconnector.EtriDB;
import dbconnector.KorDic;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import models.EtriFile;
import models.EtriSave;
import models.sentence.Chunk;
import models.sentence.Dependency;
import models.sentence.Morp;
import models.sentence.Morp_eval;
import models.sentence.NE;
import models.sentence.Phrase_dependency;
import models.sentence.Relation0;
import models.sentence.SA;
import models.sentence.SRL;
import models.sentence.Sentence;
import models.sentence.WSD;
import models.sentence.Word;
import models.sentence.ZA;
import models.sentence.srl.Argument;
import models.title.Title;
import org.bson.Document;
import search_kor_dic.SearchKorData;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

import static com.mongodb.client.model.Filters.eq;

public class MorphPage implements Initializable{

    @FXML private ListView<String> list_file;
    @FXML private ListView<String> list_id;
    @FXML private ListView<String> list_morp;
    @FXML private ListView<String> list_word;
    @FXML private ListView<String> list_longSearch;

    EtriDB etriDB;
    EtriSave etriSave;
    long startTime;
    long endTime;
    int tempTime;       //디비에 저장되어 있는 시간
    //디비 접속 버튼
    String qaName = "지정되지 않은 사용자";  //qa할때 저장할 이름

    //buttonText.eqauls if문 안에 QA 텍스트 읽어서 사용자마다 비밀번호? 입력해서 접속하게
    public void ConnectDBButton(MouseEvent mouseEvent) {

        final Button button = (Button) mouseEvent.getSource();
        final String buttonText = button.getText();

        List<String> tempList = new ArrayList<>();
        if (buttonText.equals("민숙") && InfoQA.getText().equals("3숙")) {
            etriDB = EtriDB.getInstance(buttonText);
            qaName = "민숙";
        } else if (buttonText.equals("희연") && InfoQA.getText().equals("1연")) {
            etriDB = EtriDB.getInstance(buttonText);
            qaName = "희연";
        } else if (buttonText.equals("덕진") && InfoQA.getText().equals("4덕진")) {
            etriDB = EtriDB.getInstance(buttonText);
            qaName = "덕진";
        } else if (buttonText.equals("영훈") && InfoQA.getText().equals("훈2")) {
            etriDB = EtriDB.getInstance(buttonText);
            qaName = "영훈";
        }else if (buttonText.equals("천솔") && InfoQA.getText().equals("천2")) {
            etriDB = EtriDB.getInstance(buttonText);
            qaName = "천솔";
        } else if (buttonText.equals("지원") && InfoQA.getText().equals("3원")) {
            etriDB = EtriDB.getInstance(buttonText);
            qaName = "지원";
        } else if (buttonText.equals("시은") && InfoQA.getText().equals("4시은")) {
            etriDB = EtriDB.getInstance(buttonText);
            qaName = "시은";
        } else if (buttonText.equals("현진") && InfoQA.getText().equals("1진")) {
            etriDB = EtriDB.getInstance(buttonText);
            qaName = "현진";
        } else if(buttonText.equals("실험용") && InfoQA.getText().equals("nlp456")){
            etriDB = EtriDB.getInstance(buttonText);
            qaName = "실험용";
        }
        for (Document document : etriDB.getetriCollection().find().projection(Projections.include("title"))) {
            tempList.add(new Title((Document) document.get("title")).getText());
        }
        list_file.setItems(FXCollections.observableArrayList(tempList));

        String loadText = "";
        for(Document document: etriDB.getEtriSave().find(eq("name", qaName))) {
            loadText = "마지막으로 태깅한 위치 : " + document.get("name") + " " + document.get("file")
                       + " " + document.get("sentenceIndex") + " " + document.get("morpIndex");
            tempTime = (int) document.get("time");
        }
        InfoQA.setText(loadText);
        workTime.setText(String.valueOf(tempTime));

        startTime = System.currentTimeMillis();
    }

//파일 리스트 불러오기 -> ConnectDBButton으로 대체
    List<String> korWordList;
    List<String> korHomographList;
    List<String> korMeanList;
    List<String> korPosList;
    List<String> korExList;
    public void initialize(final URL url, final ResourceBundle resourceBundle){
        //표준국어대사전 단어 저장
        korWordList = new ArrayList<>();
        korHomographList = new ArrayList<>();
        korMeanList = new ArrayList<>();
        korPosList = new ArrayList<>();
        korExList = new ArrayList<>();
        for (Document document : korDic.getKorStandardDic().find().projection(Projections.include())) {
            korWordList.add(document.get("word").toString());
            korHomographList.add(document.get("homograph").toString());
            korMeanList.add(document.get("mean").toString());
            korPosList.add(document.get("pos").toString());
            korExList.add(document.get("ex").toString());
        }
//        List<String> tempList = new ArrayList<>();
//
//        for(Document document : etriDB.getetriCollection().find().projection(Projections.include("title")))
//            tempList.add(new Title((Document) document.get("title")).getText());
//        list_file.setItems(FXCollections.observableArrayList(tempList));
    }

    EtriFile getEtriFile;
//파일 리스트 내부 문장 리스트 불러오기
    String fileTitle;
    public void clickFile(MouseEvent mouseEvent) {
        fileTitle = list_file.getSelectionModel().getSelectedItem();

        getEtriFile = new EtriFile(etriDB.getetriCollection().find(eq("title.text", fileTitle)).iterator().next());

        List<String> tempIDList = new ArrayList<>();
        for(int i=0; i<getEtriFile.getSentenceList().size(); i++)
            tempIDList.add(String.valueOf(getEtriFile.getSentenceList().get(i).getId()));
        list_id.setItems(FXCollections.observableArrayList(tempIDList));

        initSentence();
        initLemmaType();
        initListMorp();
        initListLongSearch();
    }

    List<Sentence> getSentence;
    List<Morp> getMorp;
    List<Word> getWord;
    @FXML private Text sentenceText;
//파일 리스트 내부 문장 내부 형태소 분석 리스트 불러오기
    private int sentenceIndex;
    public void clickId(MouseEvent mouseEvent) {
        sentenceIndex = list_id.getSelectionModel().getSelectedIndex();
        sentenceEvent(sentenceIndex);
    }
    private void sentenceEvent(int index){
        getSentence = getEtriFile.getSentenceList();
        getMorp = getSentence.get(sentenceIndex).getMorpList();

        String sentence = getSentence.get(sentenceIndex).getText() + "\n\n";
        List<String> tempMorpList = new ArrayList<>();
        for(int i=0; i<getMorp.size(); i++) {
            tempMorpList.add(String.valueOf(getMorp.get(i).getId() + " " + getMorp.get(i).getLemma()));
            sentence += getMorp.get(i).getLemma() + "/" + getMorp.get(i).getType() + " ";
        }
        list_morp.setItems(FXCollections.observableArrayList(tempMorpList));

        sentenceText.setText(sentence);

        getWord = getSentence.get(sentenceIndex).getWordList();
        List<String> tempWordList = new ArrayList<>();
        for(int i=0; i<getWord.size(); i++){
            tempWordList.add(getWord.get(i).getId() + " " + getWord.get(i).getText());
        }
        list_word.setItems(FXCollections.observableArrayList(tempWordList));


        initLemmaType();
        initListLongSearch();
    }

    @FXML private TextField lemmaText;
    @FXML private Text typeText;
    @FXML private Text idText;
    @FXML private Text weightText;
    @FXML private TextField positionText;
//파일 리스트 내부 문장 내부 형태소 분석 결과 불러오기
    private int tagIndex;
    public void clickMorp(MouseEvent mouseEvent){
        tagIndex = list_morp.getSelectionModel().getSelectedIndex();
        morpEvent(tagIndex);
    }

    public void pressMorp(KeyEvent keyEvent) {
        if(keyEvent.getCode() == KeyCode.UP){
            if(tagIndex > 0) {
                tagIndex--;
                morpEvent(tagIndex);
            }
        }
        if(keyEvent.getCode() == KeyCode.DOWN){
            if(tagIndex+1 < getMorp.size()) {
                tagIndex++;
                morpEvent(tagIndex);
            }
        }
    }

    //정규표현식도 좀 수정해야될텐데..
    private void morpEvent(int index){
        lemmaText.setText(getMorp.get(index).getLemma());
        typeText.setText(getMorp.get(index).getType());
        idText.setText(String.valueOf(getMorp.get(index).getId()));
        weightText.setText(String.valueOf(getMorp.get(index).getWeight()));
        positionText.setText(String.valueOf(getMorp.get(index).getPosition()));


        //while로 검색결과가 없으면 인덱스를 1개씩 빼면서 하면 될듯?
        //최장일치 검색
        List<String> tempSearchList = new ArrayList<>();
        String tempWord = "";
        String lemma = getMorp.get(index).getLemma();
        //        String pattern = "^[";
        //        for(int i=0; i<lemma.length(); i++){
        //            pattern += lemma.charAt(i) + "]+[";
        //        }
        //        pattern += "가-힣]*$";
        String pattern = "^[" + lemma.charAt(0) + "]+[가-힣]*$";

        for(int i = 0; i < korWordList.size(); i++){
            if(tempWord.equals(korWordList.get(i))) {
                //중복되는 단어들은 패스
            }else {
                if (Pattern.matches(pattern, korWordList.get(i)))
                    tempSearchList.add(korWordList.get(i));
                tempWord = korWordList.get(i);
            }
        }
        Collections.sort(tempSearchList);
        List<String> serachList = new ArrayList<>();
        tempWord = "";
        for(int i=0; i<tempSearchList.size(); i++){
            if(tempWord.equals(tempSearchList.get(i))){
                //정렬 후 중복되는 단어들 다시 패스
            }else{
                if(Pattern.matches(pattern, tempSearchList.get(i)))
                    serachList.add(tempSearchList.get(i));
                tempWord = tempSearchList.get(i);
            }
        }
        list_longSearch.setItems(FXCollections.observableArrayList(serachList));
    }


    @FXML TextField wordBeginText;
    @FXML TextField wordEndText;
    private int wordIndex;
    public void clickWord(MouseEvent mouseEvent) {
        wordIndex = list_word.getSelectionModel().getSelectedIndex();
        wordEvent(wordIndex);
    }
    private void wordEvent(int index){
        wordBeginText.setText(String.valueOf(getWord.get(index).getBegin()));
        wordEndText.setText(String.valueOf(getWord.get(index).getEnd()));
    }


    private void initSentence(){
        sentenceText.setText("");
    }
    private void initLemmaType(){
        lemmaText.setText("");
        typeText.setText("");
        idText.setText("");
        weightText.setText("");
        positionText.setText("");
    }
    private void initListMorp(){
        list_morp.setItems(FXCollections.observableArrayList(""));
    }
    private void initListLongSearch(){
        list_longSearch.setItems(FXCollections.observableArrayList(""));
    }


    //나머지 업데이트용
    List<WSD> updateWsd;
    List<NE> updateNe;
    List<Word> updateWord;
    List<ZA> updateZa;
    List<SRL> updateSrl;
    List<Relation0> updateRelation;
    List<Dependency> updateDependency;
    List<SA> updateSa;
    List<Morp_eval> updateMorpEval;
    List<Chunk> updateChunk;
    List<Phrase_dependency> updatePhraseDependency;
    Document[] arrayWsd;
    Document[] arrayNe;
    Document[] arrayWord;
    Document[] arrayZa;
    Document[] arraySrl;
    Document[] arrayRelation;
    Document[] arrayDependency;
    Document[] arraySa;
    Document[] arrayMorpEval;
    Document[] arrayChunk;
    Document[] arrayPhraseDependency;
    private void AnotherUpdate(List<Sentence> tempSentence, int index){
        updateWsd = tempSentence.get(index).getWsdList();
        updateNe = tempSentence.get(index).getNeList();
        updateWord = tempSentence.get(index).getWordList();
        updateZa = tempSentence.get(index).getZaList();
        updateSrl = tempSentence.get(index).getSrlList();
        updateRelation = tempSentence.get(index).getRelation0List();
        updateDependency = tempSentence.get(index).getDependencyList();
        updateSa = tempSentence.get(index).getSaList();
        updateMorpEval = tempSentence.get(index).getMorp_evalList();
        updateChunk = tempSentence.get(index).getChunkList();
        updatePhraseDependency = tempSentence.get(index).getPhrase_dependencyList();

        arrayWsd = new Document[updateWsd.size()];
        arrayNe = new Document[updateNe.size()];
        arrayWord = new Document[updateWord.size()];
        arrayZa = new Document[updateZa.size()];
        arraySrl = new Document[updateSrl.size()];
        arrayRelation = new Document[updateRelation.size()];
        arrayDependency = new Document[updateDependency.size()];
        arraySa = new Document[updateSa.size()];
        arrayMorpEval = new Document[updateMorpEval.size()];
        arrayChunk = new Document[updateChunk.size()];
        arrayPhraseDependency = new Document[updatePhraseDependency.size()];

        for(int i=0; i<updateWsd.size(); i++){
            arrayWsd[i] = new Document("id", updateWsd.get(i).getId()).append("text", updateWsd.get(i).getText())
                                                                      .append("type", updateWsd.get(i).getType())
                                                                      .append("scode", updateWsd.get(i).getScode())
                                                                      .append("weight", updateWsd.get(i).getWeight())
                                                                      .append("position", updateWsd.get(i).getPosition())
                                                                      .append("begin", updateWsd.get(i).getBegin())
                                                                      .append("end", updateWsd.get(i).getEnd());
        }
        for(int i=0; i<updateNe.size(); i++){
            arrayNe[i] = new Document("id", updateNe.get(i).getId()).append("text", updateNe.get(i).getText())
                                                                    .append("type", updateNe.get(i).getType())
                                                                    .append("begin", updateNe.get(i).getBegin())
                                                                    .append("end", updateNe.get(i).getEnd())
                                                                    .append("weight", updateNe.get(i).getWeight())
                                                                    .append("common_noun", updateNe.get(i).getCommon_noun());
        }
        for(int i=0; i<updateWord.size(); i++){
            arrayWord[i] = new Document("id", updateWord.get(i).getId()).append("text", updateWord.get(i).getText())
                                                                        .append("type", updateWord.get(i).getType())
                                                                        .append("begin", updateWord.get(i).getBegin())
                                                                        .append("end", updateWord.get(i).getEnd());
        }
        for(int i=0; i<updateZa.size(); i++){
            arrayZa[i] = new Document("id", updateZa.get(i).getId()).append("verb_wid", updateZa.get(i).getVerb_wid())
                                    .append("ant_sid", updateZa.get(i).getAnt_sid()).append("ant_wid", updateZa.get(i).getAnt_wid())
                                    .append("type", updateZa.get(i).getType()).append("istitle", updateZa.get(i).getIstitle())
                                    .append("weight", updateZa.get(i).getWeight());
        }
        for(int i=0; i<updateSrl.size(); i++){
            Document[] arrayArgument = new Document[updateSrl.get(i).getArgumentList().size()];
            for(int j=0; j<arrayArgument.length; j++){
                arrayArgument[j] = new Document("type", updateSrl.get(i).getArgumentList().get(j).getType())
                                            .append("word_id", updateSrl.get(i).getArgumentList().get(j).getWord_id())
                                            .append("text", updateSrl.get(i).getArgumentList().get(j).getText())
                                            .append("weight", updateSrl.get(i).getArgumentList().get(j).getWeight());
            }
            arraySrl[i] = new Document("verb", updateSrl.get(i).getVerb()).append("sense", updateSrl.get(i).getSense())
                                .append("word_id", updateSrl.get(i).getWord_id()).append("weight", updateSrl.get(i).getWeight())
                                .append("argument", Arrays.asList(arrayArgument));
        }
        for(int i=0; i<updateRelation.size(); i++){
            arrayRelation[i] = new Document();
        }
        for(int i=0; i<updateDependency.size(); i++){
            List<Integer> modList = new ArrayList<>();
            for(int j=0; j<updateDependency.get(i).getModList().size(); j++)
                modList.add(updateDependency.get(i).getModList().get(j).getMods());
            arrayDependency[i] = new Document("id", updateDependency.get(i).getId()).append("text", updateDependency.get(i).getText())
                                    .append("head", updateDependency.get(i).getHead()).append("label", updateDependency.get(i).getLabel())
                                    .append("mod", modList).append("weight", updateDependency.get(i).getWeight());
        }
        for(int i=0; i<updateSa.size(); i++){
            arraySa[i] = new Document();
        }
        for(int i=0; i<updateMorpEval.size(); i++){
            arrayMorpEval[i] = new Document("id", updateMorpEval.get(i).getId()).append("result", updateMorpEval.get(i).getResult())
                                                                                .append("target", updateMorpEval.get(i).getTarget())
                                                                                .append("word_id", updateMorpEval.get(i).getWord_id())
                                                                                .append("m_begin", updateMorpEval.get(i).getM_begin())
                                                                                .append("m_end", updateMorpEval.get(i).getM_end());
        }
        for(int i=0; i<updateChunk.size(); i++){
            arrayChunk[i] = new Document();
//            arrayChunk[i] = new Document("id", updateChunk.get(i).getId()).append("type", updateChunk.get(i).getType())
//                                        .append("begin", updateChunk.get(i).getBegin()).append("end", updateChunk.get(i).getEnd())
//                                        .append("text", updateChunk.get(i).getText());
        }
        for(int i=0; i<updatePhraseDependency.size(); i++){
            List<Integer> subPhraseList = new ArrayList<>();
            for(int j=0; j<updatePhraseDependency.get(i).getSub_phraseList().size(); j++)
                subPhraseList.add(updatePhraseDependency.get(i).getSub_phraseList().get(j).getSub_phrases());
            Document[] arrayElement = new Document[updatePhraseDependency.get(i).getElementList().size()];
            for(int j=0; j<arrayElement.length; j++){
                arrayElement[j] = new Document("text", updatePhraseDependency.get(i).getElementList().get(j).getText())
                                  .append("label", updatePhraseDependency.get(i).getElementList().get(j).getLabel())
                                  .append("begin", updatePhraseDependency.get(i).getElementList().get(j).getBegin())
                                  .append("end", updatePhraseDependency.get(i).getElementList().get(j).getEnd())
                                  .append("ne_type", updatePhraseDependency.get(i).getElementList().get(j).getNe_type());
            }
            arrayPhraseDependency[i] = new Document("id", updatePhraseDependency.get(i).getId())
                                       .append("label", updatePhraseDependency.get(i).getLabel())
                                       .append("text", updatePhraseDependency.get(i).getText())
                                       .append("begin", updatePhraseDependency.get(i).getBegin())
                                       .append("end", updatePhraseDependency.get(i).getEnd())
                                       .append("key_begin", updatePhraseDependency.get(i).getKey_begin())
                                       .append("head_phrase", updatePhraseDependency.get(i).getHead_phrase())
                                       .append("sub_phrase", subPhraseList)
                                       .append("weight", updatePhraseDependency.get(i).getWeight())
                                       .append("element", Arrays.asList(arrayElement));
        }
    }

//업데이트
    @FXML private TextField InfoQA;
    public void morpButton(MouseEvent mouseEvent){
        endTime = System.currentTimeMillis() - startTime - pauseTime;
        workTime.setText(String.valueOf(tempTime + (int)(endTime/1000)));

        Document saveDocument = new Document("name", qaName).append("file", fileTitle)
                                    .append("sentenceIndex", sentenceIndex).append("morpIndex", tagIndex)
                                    .append("time", (tempTime + (int)(endTime/1000)));
        BasicDBObject saveName = new BasicDBObject().append("name", qaName);
        BasicDBObject save = new BasicDBObject("$set", saveDocument);
        etriDB.getEtriSave().updateOne(saveName, save);


        final Button button = (Button) mouseEvent.getSource();
        final String buttonText = button.getText();
        typeText.setText(buttonText);


        if (list_morp.getSelectionModel().getSelectedIndex() == -1) {
            //팝업창 경고 뛰워주면 좋을듯
            InfoQA.setText("어절을 선택하고 누르세요.");
        } else {
            InfoQA.clear();
            List<Sentence> updateSentence = getEtriFile.getSentenceList();
            List<Morp> updateMorp;
            Document[] arraySentence = new Document[updateSentence.size()];
            Document[] tempArrayMorp;

            //텍스트 백업
            String backupText = String.valueOf(list_file.getSelectionModel().getSelectedItems()).replace("[", "").replace("]", "")
                                + " " + sentenceIndex + " " + tagIndex + " type " + buttonText;
            try {
                String filePath = "C:/Users/Public/Legal_morp.txt";
                try {
                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                   Charset.forName("UTF-8"));
                    PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                    printWriter.println(backupText);
                    printWriter.close();
                } catch (FileNotFoundException e) {
                    File file = new File(filePath);
                    FileWriter fileWriter = new FileWriter(file, true);
                    fileWriter.close();

                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                   Charset.forName("UTF-8"));
                    PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                    printWriter.println(backupText);
                    printWriter.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            //텍스트 백업


            for (int i = 0; i < updateSentence.size(); i++) {
                updateMorp = updateSentence.get(i).getMorpList();
                tempArrayMorp = new Document[updateMorp.size()];

                for (int j = 0; j < updateMorp.size(); j++) {
                    if (i == sentenceIndex && j == tagIndex) {
                        tempArrayMorp[j] = new Document("lemma", updateMorp.get(j).getLemma())
                                       .append("type", buttonText)
                                       .append("id", updateMorp.get(j).getId())
                                       .append("weight", 1)
                                       .append("position", updateMorp.get(j).getPosition());
                        //업데이트된거 바로(?) 볼 수 있게
                        updateMorp.get(j).setType(buttonText);
                        updateMorp.get(j).setWeight(1);

                        String sentence = updateSentence.get(i).getText() + "\n\n";
                        for (int k = 0; k < updateMorp.size(); k++) {
                            sentence += updateMorp.get(k).getLemma() + "/" + updateMorp.get(k).getType() + " ";
                        }
                        sentenceText.setText(sentence);
                    } else {
                        tempArrayMorp[j] = new Document("lemma", updateMorp.get(j).getLemma())
                                       .append("type", updateMorp.get(j).getType())
                                       .append("id", updateMorp.get(j).getId())
                                       .append("weight", updateMorp.get(j).getWeight())
                                       .append("position", updateMorp.get(j).getPosition());
                    }
                }
                AnotherUpdate(updateSentence, i);

                arraySentence[i] = new Document("id", updateSentence.get(i).getId()).append("text", updateSentence.get(i).getText())
                        .append("morp", Arrays.asList(tempArrayMorp)).append("WSD", Arrays.asList(arrayWsd))
                        .append("NE", Arrays.asList(arrayNe)).append("word", Arrays.asList(arrayWord))
                        .append("ZA", Arrays.asList(arrayZa)).append("SRL", Arrays.asList(arraySrl))
                        .append("relation", Arrays.asList(arrayRelation)).append("dependency", Arrays.asList(arrayDependency))
                        .append("SA", Arrays.asList(arraySa)).append("morp_eval", Arrays.asList(arrayMorpEval))
                        .append("chunk", Arrays.asList(arrayChunk)).append("phrase_dependency", Arrays.asList(arrayPhraseDependency));
            }

            Document documentSentence = new Document("sentence", Arrays.asList(arraySentence));

            BasicDBObject searchUpdateTitle = new BasicDBObject().append("title.text", getEtriFile.getTitle().getText());
            BasicDBObject update = new BasicDBObject("$set", documentSentence);

            etriDB.getetriCollection().updateOne(searchUpdateTitle, update);
        }
        morpEvent(tagIndex);
    }

    //구현 안됨
    public void moveMorpList(KeyEvent keyEvent) {
        if(keyEvent.getCode() == KeyCode.LEFT){
            System.out.println("LEFT");

        }
    }



//표준국어대사전 검색
    @FXML private TextField findKorWord;
    @FXML private TableView<SearchKorData> tableKorView = new TableView<>();
    KorDic korDic = KorDic.getInstance();
    ObservableList<SearchKorData> data = tableKorView.getItems();
    private void korView(String word){
//        String pattern = "^[";
//        for(int i=0; i<word.length(); i)

        data = tableKorView.getItems();
        data.removeAll(tableKorView.getItems());
        for(int i=0; i<korWordList.size(); i++){
            if(korWordList.get(i).equals(word))
                data.add(new SearchKorData(korHomographList.get(i), korMeanList.get(i), korExList.get(i), korPosList.get(i)));
        }
    }

    public void korWordFind(KeyEvent keyEvent) {
        if(keyEvent.getCode() == KeyCode.ENTER){
            String inputText = findKorWord.getText();
            korView(inputText);
            findKorWord.clear();
        }
    }

    @FXML private Text korMeanText;
    @FXML private Text korExText;
    public void korWordClick(MouseEvent mouseEvent) {
        int tableIndex = tableKorView.getSelectionModel().getFocusedIndex();

        if(mouseEvent.getButton() == MouseButton.PRIMARY){
            korMeanText.setText(data.get(tableIndex).getTableMean());
            korExText.setText(data.get(tableIndex).getTableEx());
        }
    }

    //list_longSearch 단어 클릭하면 검색
    public void listKorSearch(MouseEvent mouseEvent) {
        String tempWord = String.valueOf(list_longSearch.getSelectionModel().getSelectedItems());
        korView(tempWord.replace("[", "").replace("]", ""));
    }


    public void Question(KeyEvent keyEvent) {
        if(keyEvent.getCode() == KeyCode.ENTER){
            String tempString = InfoQA.getText();
            try {
                etriDB.getEtriQA().insertOne(new Document("name", qaName).append("file", fileTitle)
                                             .append("sentenceIndex", sentenceIndex).append("morpIndex", tagIndex)
                                                                         .append("question", tempString));
                InfoQA.clear();
            }catch (NullPointerException e){
                try {
                    etriDB.getEtriQA().insertOne(new Document("name", qaName).append("question", tempString));
                    InfoQA.clear();
                }catch (NullPointerException e2){
                    InfoQA.setText("디비에 접속한 후 질문해주세요.");
                }
            }
        }
    }

    //작업시간
    @FXML Button pauseButton;
    @FXML Text workTime;
    long pauseTimeStart;
    long pauseTimeEnd;
    long pauseTime = 0L;
    boolean pauseBool = false;
    private void TimeUpdate(){
        endTime = System.currentTimeMillis() - startTime - pauseTime;
        workTime.setText(String.valueOf(tempTime + (int)(endTime/1000)));

        Document saveDocument = new Document("name", qaName).append("file", fileTitle)
                                                            .append("sentenceIndex", sentenceIndex).append("morpIndex", tagIndex)
                                                            .append("time", (tempTime + (int)(endTime/1000)));
        BasicDBObject saveName = new BasicDBObject().append("name", qaName);
        BasicDBObject save = new BasicDBObject("$set", saveDocument);
        etriDB.getEtriSave().updateOne(saveName, save);
    }
    public void WorkPause(MouseEvent mouseEvent) {
        if(pauseBool == false){
            pauseTimeStart = System.currentTimeMillis();
            pauseBool = true;
            pauseButton.setText("작업재개");
        }else if(pauseBool == true){
            pauseTimeEnd = System.currentTimeMillis();
            pauseTime = pauseTime + pauseTimeEnd - pauseTimeStart;
            pauseBool = false;
            pauseButton.setText("일시중지");
        }
    }

    public void WorkExit(MouseEvent mouseEvent) {
        endTime = System.currentTimeMillis() - startTime - pauseTime;
        workTime.setText(String.valueOf(tempTime + (int)(endTime/1000)));

        Document saveDocument = new Document("name", qaName).append("file", fileTitle)
                                                            .append("sentenceIndex", sentenceIndex).append("morpIndex", tagIndex)
                                                            .append("time", (tempTime + (int)(endTime/1000)));
        BasicDBObject saveName = new BasicDBObject().append("name", qaName);
        BasicDBObject save = new BasicDBObject("$set", saveDocument);
        etriDB.getEtriSave().updateOne(saveName, save);
    }

    //개별 수정
    public void EditLemma(KeyEvent keyEvent) {
        if(keyEvent.getCode() == KeyCode.ENTER) {
            endTime = System.currentTimeMillis() - startTime - pauseTime;
            workTime.setText(String.valueOf(tempTime + (int) (endTime / 1000)));

            Document saveDocument = new Document("name", qaName).append("file", fileTitle)
                                                                .append("sentenceIndex", sentenceIndex).append("morpIndex", tagIndex)
                                                                .append("time", (tempTime + (int) (endTime / 1000)));
            BasicDBObject saveName = new BasicDBObject().append("name", qaName);
            BasicDBObject save = new BasicDBObject("$set", saveDocument);
            etriDB.getEtriSave().updateOne(saveName, save);

            String tempLemma = lemmaText.getText();
            lemmaText.setText(String.valueOf(tempLemma));


            if (list_morp.getSelectionModel().getSelectedIndex() == -1) {
                //팝업창 경고 뛰워주면 좋을듯
                InfoQA.setText("어절을 선택하고 누르세요.");
            } else {
                InfoQA.clear();
                List<Sentence> updateSentence = getEtriFile.getSentenceList();
                List<Morp> updateMorp;
                Document[] arraySentence = new Document[updateSentence.size()];
                Document[] arrayMorp;

                //텍스트 백업
                String backupText = String.valueOf(list_file.getSelectionModel().getSelectedItems()).replace("[", "").replace("]", "")
                                    + " " + sentenceIndex + " " + tagIndex + " lemma " + tempLemma;
                try {
                    String filePath = "C:/Users/Public/Legal_morp.txt";
                    try {
                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                       Charset.forName("UTF-8"));
                        PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                        printWriter.println(backupText);
                        printWriter.close();
                    } catch (FileNotFoundException e) {
                        File file = new File(filePath);
                        FileWriter fileWriter = new FileWriter(file, true);
                        fileWriter.close();

                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                       Charset.forName("UTF-8"));
                        PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                        printWriter.println(backupText);
                        printWriter.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //텍스트 백업


                for (int i = 0; i < updateSentence.size(); i++) {
                    updateMorp = updateSentence.get(i).getMorpList();
                    arrayMorp = new Document[updateMorp.size()];

                    for (int j = 0; j < updateMorp.size(); j++) {
                        if (i == sentenceIndex && j == tagIndex) {
                            arrayMorp[j] = new Document("lemma", tempLemma)
                                           .append("type", updateMorp.get(j).getType())
                                           .append("id", updateMorp.get(j).getId())
                                           .append("weight", 1)
                                           .append("position", updateMorp.get(j).getPosition());
                            //업데이트된거 바로(?) 볼 수 있게
                            updateMorp.get(j).setLemma(tempLemma);
                            updateMorp.get(j).setWeight(1);

                            String sentence = updateSentence.get(i).getText() + "\n\n";
                            for (int k = 0; k < updateMorp.size(); k++) {
                                sentence += updateMorp.get(k).getLemma() + "/" + updateMorp.get(k).getType() + " ";
                            }
                            sentenceText.setText(sentence);
                        } else {
                            arrayMorp[j] = new Document("lemma", updateMorp.get(j).getLemma())
                                           .append("type", updateMorp.get(j).getType())
                                           .append("id", updateMorp.get(j).getId())
                                           .append("weight", updateMorp.get(j).getWeight())
                                           .append("position", updateMorp.get(j).getPosition());
                        }
                    }
                    AnotherUpdate(updateSentence, i);

                    arraySentence[i] = new Document("id", updateSentence.get(i).getId())
                                       .append("reserve_str", updateSentence.get(i).getReserve_str())
                                       .append("text", updateSentence.get(i).getText())
                                       .append("morp", Arrays.asList(arrayMorp))
                                       .append("morp_eval", Arrays.asList(arrayMorpEval))
                                       .append("WSD", Arrays.asList(arrayWsd))
                                       .append("word", Arrays.asList(arrayWord))
                                       .append("NE", Arrays.asList(arrayNe))
                                       .append("chunk", Arrays.asList(arrayChunk))
                                       .append("dependency",Arrays.asList(arrayDependency))
                                       .append("phrase_dependency", Arrays.asList(arrayPhraseDependency))
                                       .append("SRL", Arrays.asList(arraySrl))
                                       .append("relation", Arrays.asList(arrayRelation))
                                       .append("SA", Arrays.asList(arraySa))
                                       .append("ZA", Arrays.asList(arrayZa));
                }

                Document documentSentence = new Document("sentence", Arrays.asList(arraySentence));

                BasicDBObject searchUpdateTitle = new BasicDBObject().append("title.text", getEtriFile.getTitle().getText());
                BasicDBObject update = new BasicDBObject("$set", documentSentence);

                etriDB.getetriCollection().updateOne(searchUpdateTitle, update);
            }

            //            positionText.clear();
            TimeUpdate();
            sentenceEvent(sentenceIndex);
            morpEvent(tagIndex);
        }
    }
    public void EditPosition(KeyEvent keyEvent) {
        if(keyEvent.getCode() == KeyCode.ENTER) {
            endTime = System.currentTimeMillis() - startTime - pauseTime;
            workTime.setText(String.valueOf(tempTime + (int) (endTime / 1000)));

            Document saveDocument = new Document("name", qaName).append("file", fileTitle)
                                                                .append("sentenceIndex", sentenceIndex).append("morpIndex", tagIndex)
                                                                .append("time", (tempTime + (int) (endTime / 1000)));
            BasicDBObject saveName = new BasicDBObject().append("name", qaName);
            BasicDBObject save = new BasicDBObject("$set", saveDocument);
            etriDB.getEtriSave().updateOne(saveName, save);

            int tempPosition = Integer.parseInt(positionText.getText());
            positionText.setText(String.valueOf(tempPosition));


            if (list_morp.getSelectionModel().getSelectedIndex() == -1) {
                //팝업창 경고 뛰워주면 좋을듯
                InfoQA.setText("어절을 선택하고 누르세요.");
            } else {
                InfoQA.clear();
                List<Sentence> updateSentence = getEtriFile.getSentenceList();
                List<Morp> updateMorp;
                Document[] arraySentence = new Document[updateSentence.size()];
                Document[] arrayMorp;

                //텍스트 백업
                String backupText = String.valueOf(list_file.getSelectionModel().getSelectedItems()).replace("[", "").replace("]", "")
                                    + " " + sentenceIndex + " " + tagIndex + " position " + tempPosition;
                try {
                    String filePath = "C:/Users/Public/Legal_morp.txt";
                    try {
                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                       Charset.forName("UTF-8"));
                        PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                        printWriter.println(backupText);
                        printWriter.close();
                    } catch (FileNotFoundException e) {
                        File file = new File(filePath);
                        FileWriter fileWriter = new FileWriter(file, true);
                        fileWriter.close();

                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                       Charset.forName("UTF-8"));
                        PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                        printWriter.println(backupText);
                        printWriter.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //텍스트 백업


                for (int i = 0; i < updateSentence.size(); i++) {
                    updateMorp = updateSentence.get(i).getMorpList();
                    arrayMorp = new Document[updateMorp.size()];

                    for (int j = 0; j < updateMorp.size(); j++) {
                        if (i == sentenceIndex && j == tagIndex) {
                            arrayMorp[j] = new Document("lemma", updateMorp.get(j).getLemma())
                                           .append("type", updateMorp.get(j).getType())
                                           .append("id", updateMorp.get(j).getId())
                                           .append("weight", 1)
                                           .append("position", tempPosition);
                            //업데이트된거 바로(?) 볼 수 있게
                            updateMorp.get(j).setPosition(tempPosition);
                            updateMorp.get(j).setWeight(1);

                            String sentence = updateSentence.get(i).getText() + "\n\n";
                            for (int k = 0; k < updateMorp.size(); k++) {
                                sentence += updateMorp.get(k).getLemma() + "/" + updateMorp.get(k).getType() + " ";
                            }
                            sentenceText.setText(sentence);
                        } else {
                            arrayMorp[j] = new Document("lemma", updateMorp.get(j).getLemma())
                                           .append("type", updateMorp.get(j).getType())
                                           .append("id", updateMorp.get(j).getId())
                                           .append("weight", updateMorp.get(j).getWeight())
                                           .append("position", updateMorp.get(j).getPosition());
                        }
                    }
                    AnotherUpdate(updateSentence, i);

                    arraySentence[i] = new Document("id", updateSentence.get(i).getId()).append("text", updateSentence.get(i).getText())
                                                            .append("morp", Arrays.asList(arrayMorp))
                                                            .append("WSD", Arrays.asList(arrayWsd))
                                                            .append("NE", Arrays.asList(arrayNe))
                                                            .append("word", Arrays.asList(arrayWord))
                                                            .append("ZA", Arrays.asList(arrayZa))
                                                            .append("SRL", Arrays.asList(arraySrl))
                                                            .append("relation", Arrays.asList(arrayRelation))
                                                            .append("dependency",Arrays.asList(arrayDependency))
                                                            .append("SA", Arrays.asList(arraySa))
                                                            .append("morp_eval", Arrays.asList(arrayMorpEval))
                                                            .append("chunk", Arrays.asList(arrayChunk))
                                                            .append("phrase_dependency", Arrays.asList(arrayPhraseDependency));
                }

                Document documentSentence = new Document("sentence", Arrays.asList(arraySentence));

                BasicDBObject searchUpdateTitle = new BasicDBObject().append("title.text", getEtriFile.getTitle().getText());
                BasicDBObject update = new BasicDBObject("$set", documentSentence);

                etriDB.getetriCollection().updateOne(searchUpdateTitle, update);
            }

//            positionText.clear();
            TimeUpdate();
            sentenceEvent(sentenceIndex);
            morpEvent(tagIndex);
        }
    }



    public void CurrentWord(MouseEvent mouseEvent) {
        List<Sentence> updateSentence = getEtriFile.getSentenceList();
        List<Morp> updateMorp;

        Document[] arraySentence = new Document[updateSentence.size()];
        Document[] arrayMorp;

        for (int i = 0; i < updateSentence.size(); i++) {
            updateMorp = updateSentence.get(i).getMorpList();
            arrayMorp = new Document[updateMorp.size()];

            int arrayMorpSize = arrayMorp.length;
            int dbIndexCount = 0;

            for (int j = 0; j < updateMorp.size(); j++) {
                if (i == sentenceIndex && j == tagIndex) {
                    //텍스트 백업
                    String backupText = String.valueOf(list_file.getSelectionModel().getSelectedItems()).replace("[", "").replace("]", "")
                                        + " " + sentenceIndex + " " + tagIndex + " INSERT CURRENT" ;
                    try {
                        String filePath = "C:/Users/Public/Legal_morp.txt";
                        try {
                            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                           Charset.forName("UTF-8"));
                            PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                            printWriter.println(backupText);
                            printWriter.close();
                        } catch (FileNotFoundException e) {
                            File file = new File(filePath);
                            FileWriter fileWriter = new FileWriter(file, true);
                            fileWriter.close();

                            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                           Charset.forName("UTF-8"));
                            PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                            printWriter.println(backupText);
                            printWriter.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    //텍스트 백업

                    //배열 크기 재정의
                    Document[] tempArray = new Document[arrayMorpSize + 1];
                    for(int k=0; k<arrayMorpSize; k++){
                        tempArray[k] = arrayMorp[k];
                    }
                    arrayMorp = tempArray;
                    arrayMorpSize++;

                    boolean checkUpdateMorp = false;
                    int tempUpdateMorpSize = updateMorp.size();
                    Morp tempMorp = new Morp(new Document("text", "??")
                                          .append("type", updateMorp.get(tempUpdateMorpSize-1).getType())
                                          .append("id", updateMorp.get(tempUpdateMorpSize-1).getId())
                                          .append("weight", updateMorp.get(tempUpdateMorpSize-1).getWeight())
                                          .append("position",updateMorp.get(tempUpdateMorpSize-1).getPosition()));
                    updateMorp.add(updateMorp.size(), tempMorp);
                    for(int k=updateMorp.size()-1; k>=0; k--){
                        if(checkUpdateMorp == false){
                            if(k == j){
                                updateMorp.get(k).setLemma("");
                                updateMorp.get(k).setType("");
                                updateMorp.get(k).setId(j);
                                updateMorp.get(k).setWeight(1.0);
                                updateMorp.get(k).setPosition(-1);
                                checkUpdateMorp = true;
                            }else{
                                updateMorp.get(k).setLemma(updateMorp.get(k-1).getLemma());
                                updateMorp.get(k).setType(updateMorp.get(k-1).getType());
                                updateMorp.get(k).setId(updateMorp.get(k-1).getId() + 1);
                                updateMorp.get(k).setWeight(updateMorp.get(k-1).getWeight());
                                updateMorp.get(k).setPosition(updateMorp.get(k-1).getPosition());
                            }
                        }else{

                        }
                    }

                    for(int k=0; k<updateMorp.size(); k++){
                        arrayMorp[k] = new Document("lemma", updateMorp.get(k).getLemma())
                                      .append("type", updateMorp.get(k).getType())
                                      .append("id", updateMorp.get(k).getId())
                                      .append("weight", updateMorp.get(k).getWeight())
                                      .append("position", updateMorp.get(k).getPosition());
                    }
                    j++;
                } else {
                    arrayMorp[j] = new Document("lemma", updateMorp.get(dbIndexCount).getLemma())
                                  .append("type", updateMorp.get(dbIndexCount).getType())
                                  .append("id", updateMorp.get(dbIndexCount).getId())
                                  .append("weight", updateMorp.get(dbIndexCount).getWeight())
                                  .append("position",updateMorp.get(dbIndexCount).getPosition());
                }
                dbIndexCount++;
            }
            AnotherUpdate(updateSentence, i);

            arraySentence[i] = new Document("id", updateSentence.get(i).getId()).append("text", updateSentence.get(i).getText())
                                                                    .append("WSD", Arrays.asList(arrayWsd))
                                                                    .append("morp", Arrays.asList(arrayMorp))
                                                                    .append("NE", Arrays.asList(arrayNe))
                                                                    .append("word", Arrays.asList(arrayWord))
                                                                    .append("ZA", Arrays.asList(arrayZa))
                                                                    .append("SRL", Arrays.asList(arraySrl))
                                                                    .append("relation", Arrays.asList(arrayRelation))
                                                                    .append("dependency", Arrays.asList(arrayDependency))
                                                                    .append("SA", Arrays.asList(arraySa))
                                                                    .append("morp_eval", Arrays.asList(arrayMorpEval))
                                                                    .append("chunk", Arrays.asList(arrayChunk))
                                                                    .append("phrase_dependency", Arrays.asList(arrayPhraseDependency));
        }

        Document documentSentence = new Document("sentence", Arrays.asList(arraySentence));

        BasicDBObject searchUpdateTitle = new BasicDBObject().append("title.text", getEtriFile.getTitle().getText());
        BasicDBObject update = new BasicDBObject("$set", documentSentence);

        etriDB.getetriCollection().updateOne(searchUpdateTitle, update);

        TimeUpdate();
        sentenceEvent(sentenceIndex);
    }

    public void NextWord(MouseEvent mouseEvent) {
        List<Sentence> updateSentence = getEtriFile.getSentenceList();
        List<Morp> updateMorp;

        Document[] arraySentence = new Document[updateSentence.size()];
        Document[] arrayMorp;

        for (int i = 0; i < updateSentence.size(); i++) {
            updateMorp = updateSentence.get(i).getMorpList();
            arrayMorp = new Document[updateMorp.size()];

            int arrayMorpSize = arrayMorp.length;
            int dbIndexCount = 0;

            for (int j = 0; j < updateMorp.size(); j++) {
                if (i == sentenceIndex && j == tagIndex) {
                    //텍스트 백업
                    String backupText = String.valueOf(list_file.getSelectionModel().getSelectedItems()).replace("[", "").replace("]", "")
                                        + " " + sentenceIndex + " " + tagIndex + " INSERT NEXT";
                    try {
                        String filePath = "C:/Users/Public/Legal_morp.txt";
                        try {
                            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                           Charset.forName("UTF-8"));
                            PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                            printWriter.println(backupText);
                            printWriter.close();
                        } catch (FileNotFoundException e) {
                            File file = new File(filePath);
                            FileWriter fileWriter = new FileWriter(file, true);
                            fileWriter.close();

                            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                                           Charset.forName("UTF-8"));
                            PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                            printWriter.println(backupText);
                            printWriter.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    //텍스트 백업

                    //                    배열 크기 재정의
                    Document[] tempArray = new Document[arrayMorpSize + 1];
                    for(int k=0; k<arrayMorpSize; k++){
                        tempArray[k] = arrayMorp[k];
                    }
                    arrayMorp = tempArray;
                    arrayMorpSize++;

                    boolean checkUpdateWsd = false;
                    int tempUpdateWsdSize = updateMorp.size();
                    Morp tempMorp = new Morp(new Document("lemma", "??")
                                          .append("type", updateMorp.get(tempUpdateWsdSize-1).getType())
                                          .append("id", updateMorp.get(tempUpdateWsdSize-1).getId())
                                          .append("weight", updateMorp.get(tempUpdateWsdSize-1).getWeight())
                                          .append("position",updateMorp.get(tempUpdateWsdSize-1).getPosition()));
                    updateMorp.add(updateMorp.size(), tempMorp);
                    for(int k=updateMorp.size()-1; k>=0; k--){
                        if(checkUpdateWsd == false){
                            if(k == j + 1){
                                updateMorp.get(k).setLemma("");
                                updateMorp.get(k).setType("");
                                updateMorp.get(k).setId(j + 1);
                                updateMorp.get(k).setWeight(1.0);
                                updateMorp.get(k).setPosition(-1);
                                checkUpdateWsd = true;
                            }else{
                                updateMorp.get(k).setLemma(updateMorp.get(k-1).getLemma());
                                updateMorp.get(k).setType(updateMorp.get(k-1).getType());
                                updateMorp.get(k).setId(updateMorp.get(k-1).getId() + 1);
                                updateMorp.get(k).setWeight(updateMorp.get(k-1).getWeight());
                                updateMorp.get(k).setPosition(updateMorp.get(k-1).getPosition());
                            }
                        }else{

                        }
                    }

                    for(int k=0; k<updateMorp.size(); k++){
                        arrayMorp[k] = new Document("lemma", updateMorp.get(k).getLemma())
                                      .append("type", updateMorp.get(k).getType())
                                      .append("id", updateMorp.get(k).getId())
                                      .append("weight", updateMorp.get(k).getWeight())
                                      .append("position", updateMorp.get(k).getPosition());
                    }
                    j++;
                } else {
                    arrayMorp[j] = new Document("lemma", updateMorp.get(dbIndexCount).getLemma())
                                  .append("type", updateMorp.get(dbIndexCount).getType())
                                  .append("id", updateMorp.get(dbIndexCount).getId())
                                  .append("weight", updateMorp.get(dbIndexCount).getWeight())
                                  .append("position",updateMorp.get(dbIndexCount).getPosition());
                }
                dbIndexCount++;
            }
            AnotherUpdate(updateSentence, i);

            arraySentence[i] = new Document("id", updateSentence.get(i).getId()).append("text", updateSentence.get(i).getText())
                                                                    .append("WSD", Arrays.asList(arrayWsd))
                                                                    .append("morp", Arrays.asList(arrayMorp))
                                                                    .append("NE", Arrays.asList(arrayNe))
                                                                    .append("word", Arrays.asList(arrayWord))
                                                                    .append("ZA", Arrays.asList(arrayZa))
                                                                    .append("SRL", Arrays.asList(arraySrl))
                                                                    .append("relation", Arrays.asList(arrayRelation))
                                                                    .append("dependency", Arrays.asList(arrayDependency))
                                                                    .append("SA", Arrays.asList(arraySa))
                                                                    .append("morp_eval", Arrays.asList(arrayMorpEval))
                                                                    .append("chunk", Arrays.asList(arrayChunk))
                                                                    .append("phrase_dependency", Arrays.asList(arrayPhraseDependency));
        }

        Document documentSentence = new Document("sentence", Arrays.asList(arraySentence));

        BasicDBObject searchUpdateTitle = new BasicDBObject().append("title.text", getEtriFile.getTitle().getText());
        BasicDBObject update = new BasicDBObject("$set", documentSentence);

        etriDB.getetriCollection().updateOne(searchUpdateTitle, update);

        sentenceEvent(sentenceIndex);
    }

    public void RemoveWord(MouseEvent mouseEvent) {
        //텍스트 백업
        String backupText = String.valueOf(list_file.getSelectionModel().getSelectedItems()).replace("[", "").replace("]", "")
                            + " " + sentenceIndex + " " + tagIndex + " REMOVE";
        try {
            String filePath = "C:/Users/Public/Legal_morp.txt";
            try {
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                               Charset.forName("UTF-8"));
                PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                printWriter.println(backupText);
                printWriter.close();
            } catch (FileNotFoundException e) {
                File file = new File(filePath);
                FileWriter fileWriter = new FileWriter(file, true);
                fileWriter.close();

                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filePath, true),
                                                                               Charset.forName("UTF-8"));
                PrintWriter printWriter = new PrintWriter(outputStreamWriter);
                printWriter.println(backupText);
                printWriter.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        //텍스트 백업

        List<Sentence> uSentence = getEtriFile.getSentenceList();
        List<Morp> updateMorp;
        Document[] arraySentence = new Document[uSentence.size()];
        Document[] arrayMorp;

        for (int i = 0; i < uSentence.size(); i++) {
            updateMorp = uSentence.get(i).getMorpList();         //자바용 배열
            arrayMorp = new Document[updateMorp.size()];        //업데이트용 배열

            int arrayNeSize = arrayMorp.length;
            boolean removeCheck = false;
            for (int j = 0; j < updateMorp.size(); j++) {    //자바내에서 업데이트
                if(removeCheck == false) {
                    if (i == sentenceIndex && j == tagIndex) {
                        if(j != updateMorp.size()-1) {
                            updateMorp.get(j).setLemma(updateMorp.get(j + 1).getLemma());
                            updateMorp.get(j).setType(updateMorp.get(j + 1).getType());
                            updateMorp.get(j).setWeight(updateMorp.get(j + 1).getWeight());
                            updateMorp.get(j).setId(updateMorp.get(j + 1).getId() - 1);
                            updateMorp.get(j).setPosition(updateMorp.get(j + 1).getPosition());
                        }
                        removeCheck = true;
                    } else {
                        updateMorp.get(j).setLemma(updateMorp.get(j).getLemma());
                        updateMorp.get(j).setType(updateMorp.get(j).getType());
                        updateMorp.get(j).setWeight(updateMorp.get(j).getWeight());
                        updateMorp.get(j).setId(updateMorp.get(j).getId());
                        updateMorp.get(j).setPosition(updateMorp.get(j).getPosition());
                    }
                }else{
                    if(j+1 != arrayNeSize) {
                        updateMorp.get(j).setLemma(updateMorp.get(j + 1).getLemma());
                        updateMorp.get(j).setType(updateMorp.get(j + 1).getType());
                        updateMorp.get(j).setWeight(updateMorp.get(j + 1).getWeight());
                        updateMorp.get(j).setId(updateMorp.get(j + 1).getId() - 1);
                        updateMorp.get(j).setPosition(updateMorp.get(j + 1).getPosition());
                    }
                }
            }
            if(removeCheck == true){
                updateMorp.remove(updateMorp.size()-1);
            }
            //배열 크기 재정의
            Document[] tempArray = new Document[updateMorp.size()];
            for(int k=0; k<updateMorp.size(); k++){
                tempArray[k] = arrayMorp[k];
            }
            arrayMorp = tempArray;

            for(int k=0; k<updateMorp.size(); k++){
                arrayMorp[k] = new Document("lemma", updateMorp.get(k).getLemma())
                              .append("type", updateMorp.get(k).getType())
                              .append("id", updateMorp.get(k).getId())
                              .append("weight", updateMorp.get(k).getWeight())
                              .append("position", updateMorp.get(k).getPosition());
            }
            AnotherUpdate(uSentence, i);

            arraySentence[i] = new Document("id", uSentence.get(i).getId())
                               .append("text", uSentence.get(i).getText())
                               .append("WSD", Arrays.asList(arrayWsd)).append("morp", Arrays.asList(arrayMorp))
                               .append("NE", Arrays.asList(arrayNe)).append("word", Arrays.asList(arrayWord))
                               .append("ZA", Arrays.asList(arrayZa)).append("SRL", Arrays.asList(arraySrl))
                               .append("relation", Arrays.asList(arrayRelation))
                               .append("dependency", Arrays.asList(arrayDependency))
                               .append("SA", Arrays.asList(arraySa)).append("morp_eval", Arrays.asList(arrayMorpEval))
                               .append("chunk", Arrays.asList(arrayChunk))
                               .append("phrase_dependency", Arrays.asList(arrayPhraseDependency));
        }

        Document documentSentence = new Document("sentence", Arrays.asList(arraySentence));

        BasicDBObject searchUpdateTitle = new BasicDBObject().append("title.text", getEtriFile.getTitle().getText());
        BasicDBObject update = new BasicDBObject("$set", documentSentence);

        etriDB.getetriCollection().updateOne(searchUpdateTitle, update);


        TimeUpdate();
        sentenceEvent(sentenceIndex);
    }


    public void editWordBegin(KeyEvent keyEvent) {
        if(keyEvent.getCode() == KeyCode.ENTER) {
            List<Sentence> updateSentence = getEtriFile.getSentenceList();
            List<Word> updateWord;
            Document[] arraySentence = new Document[updateSentence.size()];
            Document[] tempArrayWord;

            for (int i = 0; i < updateSentence.size(); i++) {
                List<Morp> tempMorp = updateSentence.get(i).getMorpList();
                Document[] tempMorp2 = new Document[tempMorp.size()];
                for (int j = 0; j < tempMorp.size(); j++) {
                    tempMorp2[j] = new Document("id", tempMorp.get(j).getId()).append("lemma", tempMorp.get(j).getLemma())
                                                                              .append("type", tempMorp.get(j).getType())
                                                                              .append("position", tempMorp.get(j).getPosition())
                                                                              .append("weight", tempMorp.get(j).getWeight());
                }

                updateWord = updateSentence.get(i).getWordList();
                tempArrayWord = new Document[updateWord.size()];

                for (int j = 0; j < updateWord.size(); j++) {
                    if (i == sentenceIndex && j == wordIndex) {
                        tempArrayWord[j] = new Document("id", updateWord.get(j).getId()).append("text", updateWord.get(j).getText())
                                                                                        .append("type", updateWord.get(j).getType())
                                                                                        .append("begin", Integer.parseInt(wordBeginText.getText()))
                                                                                        .append("end", updateWord.get(j).getEnd());
                        updateWord.get(j).setBegin(Integer.parseInt(wordBeginText.getText()));
                    } else {
                        tempArrayWord[j] = new Document("id", updateWord.get(j).getId()).append("text", updateWord.get(j).getText())
                                                                                        .append("type", updateWord.get(j).getType())
                                                                                        .append("begin", updateWord.get(j).getBegin())
                                                                                        .append("end", updateWord.get(j).getEnd());
                    }
                }
                AnotherUpdate(updateSentence, i);

                arraySentence[i] = new Document("id", updateSentence.get(i).getId()).append("text", updateSentence.get(i).getText())
                                                                                    .append("morp", Arrays.asList(tempMorp2))
                                                                                    .append("WSD", Arrays.asList(arrayWsd))
                                                                                    .append("NE", Arrays.asList(arrayNe))
                                                                                    .append("word", Arrays.asList(tempArrayWord))
                                                                                    .append("ZA", Arrays.asList(arrayZa))
                                                                                    .append("SRL", Arrays.asList(arraySrl))
                                                                                    .append("relation", Arrays.asList(arrayRelation))
                                                                                    .append("dependency", Arrays.asList(arrayDependency))
                                                                                    .append("SA", Arrays.asList(arraySa))
                                                                                    .append("morp_eval", Arrays.asList(arrayMorpEval))
                                                                                    .append("chunk", Arrays.asList(arrayChunk))
                                                                                    .append("phrase_dependency",
                                                                                            Arrays.asList(arrayPhraseDependency));
            }

            Document documentSentence = new Document("sentence", Arrays.asList(arraySentence));

            BasicDBObject searchUpdateTitle = new BasicDBObject().append("title.text", getEtriFile.getTitle().getText());
            BasicDBObject update = new BasicDBObject("$set", documentSentence);

            etriDB.getetriCollection().updateOne(searchUpdateTitle, update);

            wordEvent(wordIndex);
        }
    }

    public void editWordEnd(KeyEvent keyEvent) {
        if(keyEvent.getCode() == KeyCode.ENTER) {
            List<Sentence> updateSentence = getEtriFile.getSentenceList();
            List<Word> updateWord;
            Document[] arraySentence = new Document[updateSentence.size()];
            Document[] tempArrayWord;

            for (int i = 0; i < updateSentence.size(); i++) {
                List<Morp> tempMorp = updateSentence.get(i).getMorpList();
                Document[] tempMorp2 = new Document[tempMorp.size()];
                for (int j = 0; j < tempMorp.size(); j++) {
                    tempMorp2[j] = new Document("id", tempMorp.get(j).getId()).append("lemma", tempMorp.get(j).getLemma())
                                                                              .append("type", tempMorp.get(j).getType())
                                                                              .append("position", tempMorp.get(j).getPosition())
                                                                              .append("weight", tempMorp.get(j).getWeight());
                }

                updateWord = updateSentence.get(i).getWordList();
                tempArrayWord = new Document[updateWord.size()];

                for (int j = 0; j < updateWord.size(); j++) {
                    if (i == sentenceIndex && j == wordIndex) {
                        tempArrayWord[j] = new Document("id", updateWord.get(j).getId()).append("text", updateWord.get(j).getText())
                                                                                        .append("type", updateWord.get(j).getType())
                                                                                        .append("begin", updateWord.get(j).getBegin())
                                                                                        .append("end", Integer.parseInt(wordEndText.getText()));
                        updateWord.get(j).setEnd(Integer.parseInt(wordEndText.getText()));
                    } else {
                        tempArrayWord[j] = new Document("id", updateWord.get(j).getId()).append("text", updateWord.get(j).getText())
                                                                                        .append("type", updateWord.get(j).getType())
                                                                                        .append("begin", updateWord.get(j).getBegin())
                                                                                        .append("end", updateWord.get(j).getEnd());
                    }
                }
                AnotherUpdate(updateSentence, i);

                arraySentence[i] = new Document("id", updateSentence.get(i).getId()).append("text", updateSentence.get(i).getText())
                                                                                    .append("morp", Arrays.asList(tempMorp2))
                                                                                    .append("WSD", Arrays.asList(arrayWsd))
                                                                                    .append("NE", Arrays.asList(arrayNe))
                                                                                    .append("word", Arrays.asList(tempArrayWord))
                                                                                    .append("ZA", Arrays.asList(arrayZa))
                                                                                    .append("SRL", Arrays.asList(arraySrl))
                                                                                    .append("relation", Arrays.asList(arrayRelation))
                                                                                    .append("dependency", Arrays.asList(arrayDependency))
                                                                                    .append("SA", Arrays.asList(arraySa))
                                                                                    .append("morp_eval", Arrays.asList(arrayMorpEval))
                                                                                    .append("chunk", Arrays.asList(arrayChunk))
                                                                                    .append("phrase_dependency",
                                                                                            Arrays.asList(arrayPhraseDependency));
            }

            Document documentSentence = new Document("sentence", Arrays.asList(arraySentence));

            BasicDBObject searchUpdateTitle = new BasicDBObject().append("title.text", getEtriFile.getTitle().getText());
            BasicDBObject update = new BasicDBObject("$set", documentSentence);

            etriDB.getetriCollection().updateOne(searchUpdateTitle, update);

            wordEvent(wordIndex);
        }
    }
}
